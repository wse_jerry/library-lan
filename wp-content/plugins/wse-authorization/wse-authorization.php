<?php

/**
 * Plugin Name: Wse Authorization
 * Plugin URI:  https://wse.com.cn
 * Author:      Jerry Qi
 * Author URI:  https://jerryqi.cn
 * Description: Auth user,department or position to view post.
 * Version:     1.0
 */

/* Disallow direct access to the plugin file */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}

class jerry_W_A
{

    // Class Variables
    /**
     * used as localiztion domain name
     * @var string
     */
    public $localization_domain = "bauspc";

    public $options = false;
    /**
     * Class constructor
     */
    public function __construct()
    {
        //Language Setup
        $locale = get_locale();
        load_plugin_textdomain($this->localization_domain, false, dirname(plugin_basename(__FILE__)) . '/lang/');
        $this->hooks();
    }

    /**
     * hooks
     * function used to add action and filter hooks
     * Used with `admin_hooks`, `client_hooks`, `and common_hooks`
     * @return void
     */
    public function hooks()
    {
        if (is_admin()) {
            $this->admin_hooks();
        } else {
            $this->client_hooks();
        }
        $this->common_hooks();
    }

    /**
     * common_hooks
     * hooks for both admin and client sides
     * @return void
     */
    public function common_hooks()
    {
        /* add_filter hooks */
        add_action('init', array($this, 'W_A_init'));
        /* Save Meta Box */
        add_action('save_post', array($this, 'Wse_authorization_box_inner_save'));
        /* add shortcodes */
        add_shortcode('O_U', array($this, 'Wse_authorization_shortcode'));
    }

    /**
     * admin_hooks
     * Admin side hooks should go here
     * @return void
     */
    public function admin_hooks()
    {
        //add admin panel
        if (!class_exists('SimplePanel')) {
            require_once plugin_dir_path(__FILE__) . 'panel/Simple_Panel_class.php';
        }

        require_once plugin_dir_path(__FILE__) . 'panel/Wse_authorization_panel.php';

        /* Define the custom box */
        add_action('add_meta_boxes', array($this, 'Wse_authorization_box'));
    }

    /**
     * client_hooks
     * client side hooks should go here
     * @return void
     */
    public function client_hooks()
    {
    }

    //init
    public function W_A_init()
    {
        $options = $this->W_A_get_option();
        if ($options['run_on_the_content']) {
            /* hook the_content to filter users */
            //add_filter('the_content', array($this, 'Wse_authorization_filter'), 20);
        }
        if ($options['run_on_the_excerpt']) {
            /* hook the_excerpt to filter users */
            add_filter('the_excerpt', array($this, 'Wse_authorization_filter'), 20);
        }
        //add_filter('the_content', array($this, 'Wse_authorization_filter'), 20);
        //allow other filters
        do_action('Wse_authorization_filter_add', $this);
    }

    //options
    public function W_A_get_option()
    {
        if ($this->options) {
            return $this->options;
        }

        $temp = array(
            'b_massage' => '',
            'list_users' => true,
            'list_roles' => true,
            'run_on_the_content' => true,
            'run_on_the_excerpt' => false,
            'posttypes' => array('post' => true, 'page' => true),
            'capability' => 'manage_options',
            'user_role_list_type' => 'checkbox',
            'user_list_type' => 'checkbox',
        );

        $i = get_option('W_A');
        if (!empty($i)) {
            //checkboxes
            $checkboxes = array(
                'run_on_the_content',
                'run_on_the_excerpt',
                'list_users',
                'list_roles',
            );

            //all others
            foreach ($i as $c => $value) {
                if (in_array($c, $checkboxes)) {
                    if (isset($i[$c]) && $i[$c]) {
                        $temp[$c] = true;
                    } else {
                        $temp[$c] = false;
                    }
                } else {
                    $temp[$c] = $value;
                }
            }

        }

        update_option('W_A', $temp);
        $this->options = $temp;
        //delete_option('W_A');
        return $temp;
    }

    /* Adds a box to the main column on the custom post type edit screens */
    public function Wse_authorization_box($post_type)
    {
        $options = $this->W_A_get_option();
        if (!current_user_can($options['capability'])) {
            return;
        }

        $allowed_types = array();
        foreach ((array) $options['posttypes'] as $key => $value) {
            if ($value) {
                $allowed_types[] = $key;
            }

        }
        //added a filter to enable controling the allowed post types by filter hook
        $allowed_types = apply_filters('USC_allowed_post_types', $allowed_types);

        if (in_array($post_type, (array) $allowed_types)) {
            add_meta_box('Wse_authorization', __('Wse authorization box'), array($this, 'Wse_authorization_box_inner'), $post_type);
        }

        //allow custom types by action hook
        do_action('USC_add_meta_box', $this);
    }

    /* Prints the box content */
    public function Wse_authorization_box_inner()
    {
        global $post, $wp_roles;
        $savedviewusers = get_post_meta($post->ID, 'W_A_view_users', true);
        $savedviewgroups = get_post_meta($post->ID, 'W_A_view_groups', true);
        $saveddownusers = get_post_meta($post->ID, 'W_A_down_users', true);
        $saveddowngroups = get_post_meta($post->ID, 'W_A_down_groups', true);

        // Use nonce for verification
        wp_nonce_field(plugin_basename(__FILE__), 'Wse_authorization_box_inner');

        //Choose who can view this content
        echo __('Select users or departments to show this content to', 'bauspc');
        //by user
        echo '<h4>' . __('By User Name:', 'bauspc') . '</h4><p>';
        $site_id = 1;
        if (is_multisite()) {
            $site_id = get_current_blog_id();
        }

        $blogusers = get_users('blog_id=' . $site_id . '&orderby=nicename');
        if (empty($savedviewusers)) {
            $savedviewusers = array();
        }

        foreach ($blogusers as $user) {
            echo '<label><input type="checkbox" name="W_A_view_users[]" value="' . $user->ID . '"';
            if (in_array($user->ID, $savedviewusers)) {
                echo ' checked';
            }
            echo '>' . $user->display_name . '</label>    ';
        }
        echo '</p>';

        //By Departments
        echo '<h4>' . __('By User Department:', 'bauspc') . '</h4><p>';
        $args = array(
            'taxonomy' => 'department',
            'hide_empty' => false,
        );
        $groups = get_terms("department", $args);
        if (empty($savedviewgroups)) {
            $savedviewgroups = array();
        }

        foreach ($groups as $group) {
            echo '<label><input type="checkbox" name="W_A_view_groups[]" value="' . $group->term_id . '"';
            if (in_array($group->term_id, $savedviewgroups)) {
                echo ' checked';
            }
            echo '>' . $group->name . '</label>    ';
        }
        echo '</p>';

        //Choose who can download the attachements
        echo __('Select users or departments to download attachments', 'bauspc');
        echo '<h4>' . __('By User Name:', 'bauspc') . '</h4><p>';
        if (empty($saveddownusers)) {
            $saveddownusers = array();
        }

        foreach ($blogusers as $user) {
            echo '<label><input type="checkbox" name="W_A_down_users[]" value="' . $user->ID . '"';
            if (in_array($user->ID, $saveddownusers)) {
                echo ' checked';
            }
            echo '>' . $user->display_name . '</label>    ';
        }
        echo '</p>';

        //By Departments
        echo '<h4>' . __('By User Department:', 'bauspc') . '</h4><p>';
        if (empty($saveddowngroups)) {
            $saveddowngroups = array();
        }

        foreach ($groups as $group) {
            echo '<label><input type="checkbox" name="W_A_down_groups[]" value="' . $group->term_id . '"';
            if (in_array($group->term_id, $saveddowngroups)) {
                echo ' checked';
            }
            echo '>' . $group->name . '</label>    ';
        }
        echo '</p>';
        ?>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.clearselection_usc').click(function(e){
				e.preventDefault();
				$(this).prev().val([]);
			});
		});
		</script>
		<?php

        echo '<h4>' . __('Content Blocked message:', 'bauspc') . '</h4><p>';
        echo '<textarea rows="3" cols="70" name="W_A_message" id="W_A_message">' . get_post_meta($post->ID, 'W_A_message', true) . '</textarea>
		<br/><span class="description">' . __('This message will be shown to anyone who is not on the list above.') . '</span></p>';
    }

    public function Wse_authorization_box_inner_ext()
    {

    }

    /* When the post is saved, saves our custom data */
    public function Wse_authorization_box_inner_save($post_id)
    {
        global $post;
        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if (isset($_POST['Wse_authorization_box_inner'])) {
            if (!wp_verify_nonce($_POST['Wse_authorization_box_inner'], plugin_basename(__FILE__))) {
                return $post_id;
            }

        } else {
            return $post_id;
        }
        // verify if this is an auto save routine.
        // If it is our form has not been submitted, so we dont want to do anything
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // OK, we're authenticated: we need to find and save the data

        $savedviewusers = get_post_meta($post->ID, 'W_A_view_users', true);
        $savedviewgroups = get_post_meta($post->ID, 'W_A_view_groups', true);
        $saveddownusers = get_post_meta($post->ID, 'W_A_down_users', true);
        $saveddowngroups = get_post_meta($post->ID, 'W_A_down_groups', true);
//User view
        if (isset($_POST['W_A_view_users']) && !empty($_POST['W_A_view_users'])) {
            foreach ($_POST['W_A_view_users'] as $vu) {
                $new_view_users[] = $vu;
            }
            update_post_meta($post_id, 'W_A_view_users', $new_view_users);
        } else {
            if (count($savedviewusers) > 0) {
                delete_post_meta($post_id, 'W_A_view_users');
            }
        }

        //Group view
        if (isset($_POST['W_A_view_groups']) && !empty($_POST['W_A_view_groups'])) {
            foreach ($_POST['W_A_view_groups'] as $vg) {
                $new_view_groups[] = $vg;
            }
            update_post_meta($post_id, 'W_A_view_groups', $new_view_groups);
        } else {
            if (count($savedviewgroups) > 0) {
                delete_post_meta($post_id, 'W_A_view_groups');
            }
        }

        //User download
        if (isset($_POST['W_A_down_users']) && !empty($_POST['W_A_down_users'])) {
            foreach ($_POST['W_A_down_users'] as $du) {
                $new_down_users[] = $du;
            }
            update_post_meta($post_id, 'W_A_down_users', $new_down_users);
        } else {
            if (count($saveddownusers) > 0) {
                delete_post_meta($post_id, 'W_A_down_users');
            }
        }

        //Group download
        if (isset($_POST['W_A_down_groups']) && !empty($_POST['W_A_down_groups'])) {
            foreach ($_POST['W_A_down_groups'] as $dg) {
                $new_down_groups[] = $dg;
            }
            update_post_meta($post_id, 'W_A_down_groups', $new_down_groups);
        } else {
            if (count($saveddowngroups) > 0) {
                delete_post_meta($post_id, 'W_A_down_groups');
            }
        }

        if (isset($_POST['W_A_message']) && $_POST['W_A_message'] != '') {
            update_post_meta($post_id, 'W_A_message', $_POST['W_A_message']);
        }
    }

    public function Wse_authorization_box_inner_save_ext($post_id)
    {

    }

    public function Wse_authorization_filter($content)
    {
        global $post;
        $savedoptions = get_post_meta($post->ID, 'W_A_options', true);
        $m = get_post_meta($post->ID, 'W_A_message', true);

        if (isset($savedoptions) && !empty($savedoptions)) {
            // none logged only
            if (isset($savedoptions['non_logged']) && $savedoptions['non_logged'] == 1) {
                if (is_user_logged_in()) {
                    return $this->displayMessage($m);
                }
            }
            //logged in users only
            if (isset($savedoptions['logged']) && $savedoptions['logged'] == 1) {
                if (!is_user_logged_in()) {
                    return $this->displayMessage($m);
                }
            }
        }
        $run_check = 0;
        //get saved roles
        $savedroles = get_post_meta($post->ID, 'W_A_roles', true);
        //get saved users
        $savedusers = get_post_meta($post->ID, 'W_A_users', true);
        if (!count($savedusers) > 0 && !count($savedroles) > 0) {
            return $content;
        }
        //by role
        if (isset($savedroles) && !empty($savedroles)) {
            foreach ((array) $savedroles as $role) {
                if ($this->has_role(strtolower($role))) {
                    return $content;
                }
            }
            //failed role check
            $run_check = 1;
        }

        //by user
        if (isset($savedusers) && !empty($savedusers)) {
            $current_user = wp_get_current_user();
            if (in_array($current_user->ID, $savedusers)) {
                return $content;
            } else {
                $run_check = $run_check + 1;
            }
            //failed both checks
            return $this->displayMessage($m);
        }
        if ($run_check > 0) {
            return $this->displayMessage($m);
        }

        return $content;
    }

/************************
 * helpers
 ************************/
/**
 * @Deprecated 1.0.2
 */
    public function bausp_get_current_user_role()
    {
    }

/**
 * @since 1.0.2
 */
    public function has_role($role, $user_id = null)
    {
        if (is_numeric($user_id)) {
            $user = get_userdata($user_id);
        } else {
            $user = wp_get_current_user();
        }

        if (empty($user)) {
            return false;
        }

        if (is_array($role)) {
            foreach ($role as $r) {
                if (in_array($r, (array) $user->roles)) {
                    return true;
                }

            }
        } else {
            if (in_array($role, (array) $user->roles)) {
                return true;
            }

        }
        return false;
    }

    public function credits()
    {
        echo '<ul style="list-style: square inside none; width: 300px; font-weight: bolder; padding: 20px; border: 2px solid; background-color: #FFFFE0; border-color: #E6DB55; position: fixed;  right: 120px; top: 150px;">
					<li> Any feedback or suggestions are welcome at <a href="http://en.jerryqi.cn/2011/wse-authorization-plugin">plugin homepage</a></li>
					<li> <a href="http://wordpress.org/tags/wse-authorization?forum_id=10">Support forum</a> for help and bug submittion</li>
					<li> Also check out <a href="http://en.jerryqi.cn/category/plugins">my other plugins</a></li>
					<li> And if you like my work <span style="color: #FF0000;">make a donation</span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PPCPQV8KA3UQA"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"></a>or atleast <a href="http://wordpress.org/extend/plugins/wse-authorization/">rank the plugin</a></li>
				</ul>';
    } //end function

/************************
 *    shortcodes
 ************************/

    public function Wse_authorization_shortcode($atts, $content = null, $tag = '')
    {
        $atts = shortcode_atts(array(
            'user_id' => '',
            'user_name' => '',
            'user_role' => '',
            'logged_status' => '',
            'blocked_message' => false,
            'blocked_meassage' => null,
        ), $atts);

        extract($atts);

        global $post;
        if ($blocked_meassage !== null) {
            $blocked_message = $blocked_meassage;
        }

        $options = $this->W_A_get_option('W_A');
        $current_user = wp_get_current_user();

        if ((isset($user_id) && $user_id != '') || (isset($user_name) && $user_name != '') || (isset($user_role) && $user_role != '')) {
            //check logged in
            if (!is_user_logged_in()) {
                return $this->displayMessage($blocked_message);
            }

            //check user id
            if (isset($user_id) && $user_id != '') {
                $user_id = explode(",", $user_id);
                if (!in_array($current_user->ID, $user_id)) {
                    return $this->displayMessage($blocked_message);
                }
            }

            //check user name
            if (isset($user_name) && $user_name != '') {
                $user_name = explode(",", $user_name);
                if (!in_array($current_user->user_login, $user_name)) {
                    return $this->displayMessage($blocked_message);
                }
            }

            //check user role
            if (isset($user_role) && $user_role != '') {
                $user_role = explode(",", $user_role);
                if (!$this->has_role($user_role)) {
                    return $this->displayMessage($blocked_message);
                }
            }
        }

        //logged in
        if ($logged_status == 'in') {
            if (!is_user_logged_in()) {
                return $this->displayMessage($blocked_message);
            }
        }
        //logged out
        if ($logged_status == 'out') {
            if (is_user_logged_in()) {
                return $this->displayMessage($blocked_message);
            }
        }

        return apply_filters('user_spcefic_content_shortcode_filter', do_shortcode($content));
    } //end function

    public function displayMessage($m)
    {
        global $post;
        if (isset($m) && $m != '') {
            return apply_filters('user_specific_content_blocked', $m, $post);
        } else {
            $options = $this->W_A_get_option('W_A');
            return apply_filters('user_specific_content_blocked', $options['b_massage'], $post);
        }
    }
} //end class
add_action('init', 'init_uspc_plugin', 0);
function init_uspc_plugin()
{
    global $W_A_i;
    $W_A_i = new jerry_W_A();
}