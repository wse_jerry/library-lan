<?php 
namespace Bookmarks\Listeners;

use Bookmarks\Entities\Bookmark\Bookmark;
use Bookmarks\Entities\Bookmark\SyncAllBookmarks;
use Bookmarks\Entities\Post\SyncBookmarkCount;

class ClearBookmarks extends AJAXListenerBase
{
	/**
	* Bookmarks Sync
	*/
	private $bookmarks_sync;

	public function __construct()
	{
		parent::__construct();
		$this->bookmarks_sync = new SyncAllBookmarks;
		$this->setFormData();
		$this->clearBookmarks();
		$this->sendResponse();
	}

	/**
	* Set Form Data
	*/
	private function setFormData()
	{
		$this->data['siteid'] = intval(sanitize_text_field($_POST['siteid']));
		$this->data['old_bookmarks'] = $this->user_repo->formattedBookmarks();
		$this->data['logged_in'] = ( isset($_POST['logged_in']) && $_POST['logged_in'] !== '' ) ? true : false;
		$this->data['user_id'] = ( isset($_POST['user_id']) && $_POST['user_id'] !== '' ) ? intval($_POST['user_id']) : 0;
	}

	/**
	* Remove all user's bookmarks from the specified site
	*/
	private function clearBookmarks()
	{
		$user = ( $this->data['logged_in'] ) ? $this->data['user_id'] : null;
		do_action('bookmarks_before_clear', $this->data['siteid'], $user);
		$bookmarks = $this->user_repo->getAllBookmarks();
		foreach($bookmarks as $key => $site_bookmarks){
			if ( $site_bookmarks['site_id'] == $this->data['siteid'] ) {
				$this->updateBookmarkCounts($site_bookmarks);
				unset($bookmarks[$key]);
			}
		}
		$this->bookmarks_sync->sync($bookmarks);
		
		do_action('bookmarks_after_clear', $this->data['siteid'], $user);
	}

	/**
	* Update all the cleared post bookmark counts
	*/
	private function updateBookmarkCounts($site_bookmarks)
	{
		foreach($site_bookmarks['posts'] as $bookmark){
			$count_sync = new SyncBookmarkCount($bookmark, 'inactive', $this->data['siteid']);
			$count_sync->sync();
		}
	}

	/**
	* Set and send the response
	*/
	private function sendResponse()
	{
		$bookmarks = $this->user_repo->formattedBookmarks();
		$this->response(array(
			'status' => 'success',
			'old_bookmarks' => $this->data['old_bookmarks'],
			'bookmarks' => $bookmarks
		));
	}
}