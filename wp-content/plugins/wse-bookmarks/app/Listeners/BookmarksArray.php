<?php 
namespace Bookmarks\Listeners;

/**
* Return an array of user's bookmarkd posts
*/
class BookmarksArray extends AJAXListenerBase
{
	/**
	* User Bookmarks
	* @var array
	*/
	private $bookmarks;

	public function __construct()
	{
		parent::__construct(false);
		$this->setBookmarks();
		$this->response(array('status'=>'success', 'bookmarks' => $this->bookmarks));
	}

	/**
	* Get the Bookmarks
	*/
	private function setBookmarks()
	{
		$bookmarks = $this->user_repo->formattedBookmarks();
		$this->bookmarks = $bookmarks;
	}
}