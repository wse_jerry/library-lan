<?php 
namespace Bookmarks\Listeners;

use Bookmarks\Entities\Bookmark\Bookmark;

class BookmarkButton extends AJAXListenerBase
{
	public function __construct()
	{
		parent::__construct();
		$this->setFormData();
		$this->updateBookmark();
	}

	/**
	* Set Form Data
	*/
	private function setFormData()
	{
		$this->data['postid'] = intval(sanitize_text_field($_POST['postid']));
		$this->data['siteid'] = intval(sanitize_text_field($_POST['siteid']));
		$this->data['status'] = ( $_POST['status'] == 'active') ? 'active' : 'inactive';
		$this->data['groupid'] = ( isset($_POST['groupid']) && $_POST['groupid'] !== '' ) ? intval($_POST['groupid']) : 1;
		$this->data['logged_in'] = ( isset($_POST['logged_in']) && $_POST['logged_in'] !== '' ) ? true : null;
		$this->data['user_id'] = ( isset($_POST['user_id']) && $_POST['user_id'] !== '' ) ? intval($_POST['user_id']) : null;
	}

	/**
	* Update the Bookmark
	*/
	private function updateBookmark()
	{
		try {
			$this->beforeUpdateAction();
			$bookmark = new Bookmark;
			$bookmark->update($this->data['postid'], $this->data['status'], $this->data['siteid'], $this->data['groupid']);
			$this->afterUpdateAction();
			$this->response(array(
				'status' => 'success', 
				'bookmark_data' => array(
					'id' => $this->data['postid'], 
					'siteid' => $this->data['siteid'], 
					'status' => $this->data['status'],
					'groupid' => $this->data['groupid'],
					'save_type' => $bookmark->saveType(),
					'logged_in' => $this->data['logged_in'],
					'user_id' => $this->data['user_id']
				),
				'bookmarks' => $this->user_repo->formattedBookmarks($this->data['postid'], $this->data['siteid'], $this->data['status'])
			));
		} catch ( \Exception $e ){
			return $this->sendError($e->getMessage());
		}
	}

	/**
	* Before Update Action
	* Provides hook for performing actions before a bookmark
	*/
	private function beforeUpdateAction()
	{
		$user = ( is_user_logged_in() ) ? get_current_user_id() : null;
		do_action('bookmarks_before_bookmark', $this->data['postid'], $this->data['status'], $this->data['siteid'], $user);
	}

	/**
	* After Update Action
	* Provides hook for performing actions after a bookmark
	*/
	private function afterUpdateAction()
	{
		$user = ( is_user_logged_in() ) ? get_current_user_id() : null;
		do_action('bookmarks_after_bookmark', $this->data['postid'], $this->data['status'], $this->data['siteid'], $user);
	}
}