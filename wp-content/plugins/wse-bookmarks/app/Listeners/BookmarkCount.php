<?php
namespace Bookmarks\Listeners;

use Bookmarks\Entities\Post\BookmarkCount as BookmarkCounter;

/**
* Return the total number of bookmarks for a specified post
*/
class BookmarkCount extends AJAXListenerBase
{
	/**
	* Bookmark Counter
	*/
	private $bookmark_counter;

	public function __construct()
	{
		parent::__construct();
		$this->bookmark_counter = new BookmarkCounter;
		$this->setData();
		$this->sendCount();
	}

	private function setData()
	{
		$this->data['postid'] = ( isset($_POST['postid']) ) ? intval($_POST['postid']) : null;
		$this->data['siteid'] = ( isset($_POST['siteid']) ) ? intval($_POST['siteid']) : null;
	}

	private function sendCount()
	{
		$this->response(array(
			'status' => 'success',
			'count' => $this->bookmark_counter->getCount($this->data['postid'], $this->data['siteid'])
		));
	}
}