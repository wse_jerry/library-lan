<?php 
namespace Bookmarks;

/**
* Static Helper Methods
*/
class Helpers 
{
	/**
	* Plugin Root Directory
	*/
	public static function plugin_url()
	{
		return plugins_url() . '/' . dirname( plugin_basename( BOOKMARKS_PLUGIN_FILE ) );
	}

	/**
	* Views
	*/
	public static function view($file)
	{
		return dirname(__FILE__) . '/Views/' . $file . '.php';
	}

	/**
	* Plugin Version
	*/
	public static function version()
	{
		global $bookmarks_version;
		return $bookmarks_version;
	}

	/**
	* Get File Contents
	*/
	public static function getFileContents($file)
	{
		return file_get_contents( dirname( dirname(__FILE__) ) . '/' . $file);
	}

	/**
	* Multidemensional array key search
	* @since 1.1
	* @return boolean
	*/
	public static function keyExists($needle, $haystack)
	{
		if ( array_key_exists($needle, $haystack) || in_array($needle, $haystack) ){
			return true;
		} else {
			$return = false;
			foreach ( array_values($haystack) as $value ){
				if ( is_array($value) && !$return ) $return = self::keyExists($needle, $value);
			}
			return $return;
		}
	}

	/**
	* Site ID Exists
	* checks if site id is in bookmarks array yet
	* @since 1.1
	* @return boolean
	*/
	public static function siteExists($site_id, $meta)
	{
		foreach ( $meta as $key => $site ){
			if ( $site['site_id'] == $site_id ) return true;
		}
		return false;
	}

	/**
	* Groups Exists
	* checks if groups array is in bookmarks array yet
	* @since 2.2
	* @return boolean
	*/
	public static function groupsExist($site_bookmarks)
	{
		if ( isset($site_bookmarks['groups']) && !empty($site_bookmarks['groups']) ) return true;
		return false;
	}

	/**
	* Pluck the site bookmarks from saved meta array
	* @since 1.1
	* @param int $site_id
	* @param array $bookmarks (user meta)
	* @return array
	*/
	public static function pluckSiteBookmarks($site_id, $all_bookmarks)
	{
		foreach($all_bookmarks as $site_bookmarks){
			if ( $site_bookmarks['site_id'] == $site_id && isset($site_bookmarks['posts']) ) return $site_bookmarks['posts'];
		}
		return array();
	}

	/**
	* Pluck the site bookmarks from saved meta array
	* @since 1.1
	* @param int $site_id
	* @param array $bookmarks (user meta)
	* @return array
	*/
	public static function pluckGroupBookmarks($group_id, $site_id, $all_bookmarks)
	{
		foreach($all_bookmarks as $key => $site_bookmarks){
			if ( $site_bookmarks['site_id'] !== $site_id ) continue;
			foreach ( $all_bookmarks[$key]['groups'] as $group ){
				if ( $group['group_id'] == $group_id ){
					return $group['posts'];
				}
			}
		}
		return array();
	}
}