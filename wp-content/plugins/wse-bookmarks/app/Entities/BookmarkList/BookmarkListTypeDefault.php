<?php
namespace Bookmarks\Entities\BookmarkList;

use Bookmarks\Entities\Bookmark\BookmarkButton;

/**
* Create a bookmarks list using the default markup
*/
class BookmarkListTypeDefault extends BookmarkListTypeBase
{
	public function __construct($list_options)
	{
		parent::__construct($list_options);
	}

	private function setTemplate()
	{
		$template = '';
		if ( $this->list_options->include_thumbnails ) $template .= '[post_thumbnail_' . $this->list_options->thumbnail_size . ']';
		$template .= "\n\n";
		if ( $this->list_options->include_links )  $template .= '<a href="[post_permalink]">';
		$template .= '[post_title]';
		if ( $this->list_options->include_links ) $template .= '</a>';
		$template .= "\n\n";
		if ( $this->list_options->include_excerpt ) $template .= "[post_excerpt]\n\n";
		if ( $this->list_options->include_button )$template .= "[bookmarks_button]";
		return $template;
	}

	/**
	* Generate a single listing
	* @param int bookmark post id
	*/
	public function listing($bookmark)
	{
		return $this->listing_presenter->present($this->list_options, $this->setTemplate(), $bookmark);
	}
}