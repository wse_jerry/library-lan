<?php
namespace Bookmarks\Entities\BookmarkList;

use Bookmarks\Entities\User\UserBookmarks;
use Bookmarks\Config\SettingsRepository;
use Bookmarks\Entities\BookmarkList\BookmarkListingPresenter;
use Bookmarks\Entities\PostType\PostTypeRepository;

/**
* Base class for bookmark lists
*/
abstract class BookmarkListTypeBase
{
	/**
	* List options
	* @var object
	*/
	protected $list_options;

	/**
	* User bookmarks object
	*/
	protected $user_bookmarks;

	/**
	* Settings Repo
	*/
	protected $settings_repo;

	/**
	* Post Type Repo
	*/
	protected $post_type_repo;

	/**
	* User's Bookmarks
	*/
	protected $bookmarks;

	/**
	* Listing Presenter
	*/
	protected $listing_presenter;

	public function __construct($list_options)
	{
		$this->settings_repo = new SettingsRepository;
		$this->post_type_repo = new PostTypeRepository;
		$this->user_bookmarks = new UserBookmarks;
		$this->listing_presenter = new BookmarkListingPresenter;
		$this->list_options = $list_options;
		$this->setApiOptions();
		$this->setBookmarks();
		$this->setNoBookmarksText();
		$this->setPostTypes();
	}

	/**
	* Set the API options (defined in shortcode and api functions)
	*/
	protected function setApiOptions()
	{
		$this->list_options->site_id = ( is_null($this->list_options->site_id) || $this->list_options->site_id == '' ) 
			? get_current_blog_id() : $this->list_options->site_id;
		$this->list_options->user_id = ( is_null($this->list_options->user_id) || $this->list_options->user_id == '' ) 
			? null : $this->list_options->user_id;
		$this->list_options->filters = ( is_null($this->list_options->filters) || $this->list_options->filters == '' ) 
			? null : $this->list_options->filters;
	}

	/**
	* Set the bookmarks
	*/
	protected function setBookmarks()
	{
		$bookmarks = $this->user_bookmarks->getBookmarksArray($this->list_options->user_id, $this->list_options->site_id, $this->list_options->filters);
		$this->bookmarks = ( isset($bookmarks[0]['site_id']) ) ? $bookmarks[0]['posts'] : $bookmarks;
	}

	/**
	* Set the no bookmarks text
	*/
	protected function setNoBookmarksText()
	{
		if ( $this->list_options->no_bookmarks == '' ) 
			$this->list_options->no_bookmarks = $this->settings_repo->noBookmarksText();
	}

	/**
	* Set the post types
	*/
	protected function setPostTypes()
	{
		$this->list_options->post_types = implode(',', $this->post_type_repo->getAllPostTypes('names', true));
		if ( isset($this->list_options->filters['post_type']) )	
			$this->list_options->post_types = implode(',', $this->list_options->filters['post_type']);
	}

	/**
	* Generate the list opening
	*/
	protected function listOpening()
	{
		$css = apply_filters('bookmarks/list/wrapper/css', $this->list_options->wrapper_css, $this->list_options);
		$out = '<' . $this->list_options->wrapper_type;
		$out .= ' class="bookmarks-list ' . $css . '" data-userid="' . $this->list_options->user_id . '" data-siteid="' . $this->list_options->site_id . '" ';
		$out .= ( $this->list_options->include_button ) ? 'data-includebuttons="true"' : 'data-includebuttons="false"';
		$out .= ( $this->list_options->include_links ) ? ' data-includelinks="true"' : ' data-includelinks="false"';
		$out .= ( $this->list_options->include_thumbnails ) ? ' data-includethumbnails="true"' : ' data-includethumbnails="false"';
		$out .= ( $this->list_options->include_excerpt ) ? ' data-includeexcerpts="true"' : ' data-includeexcerpts="false"';
		$out .= ' data-thumbnailsize="' . $this->list_options->thumbnail_size . '"';
		$out .= ' data-nobookmarkstext="' . $this->list_options->no_bookmarks . '"';
		$out .= ' data-posttypes="' . $this->list_options->post_types . '"';
		$out .= '>';
		return $out;
	}

	/**
	* Generate the list closing
	*/
	protected function listClosing()
	{
		return '</' . $this->list_options->wrapper_type . '>';
	}

	/**
	* Generates the no bookmarks item
	*/
	protected function noBookmarks()
	{
		if ( !empty($this->bookmarks) ) return;
		$out = $this->listOpening();
		$out .= '<' . $this->list_options->wrapper_type;
		$out .= ' data-postid="0" data-nobookmarks class="no-bookmarks">' . $this->list_options->no_bookmarks;
		$out .= '</' . $this->list_options->wrapper_type . '>';
		$out .= $this->listClosing();
		return $out;
	}

	/**
	* Get the markup for a full list
	*/
	public function getListMarkup()
	{
		if ( is_multisite() ) switch_to_blog($this->list_options->site_id);
		if ( empty($this->bookmarks) ) return $this->noBookmarks();

		$out = $this->listOpening();	
		foreach ( $this->bookmarks as $key => $bookmark ){
			$out .= $this->listing($bookmark);
		}
		$out .= $this->listClosing();
		if ( is_multisite() ) restore_current_blog();
		return $out;
	}
}