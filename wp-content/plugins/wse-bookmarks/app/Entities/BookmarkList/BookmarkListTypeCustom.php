<?php
namespace Bookmarks\Entities\BookmarkList;

use Bookmarks\Entities\User\UserBookmarks;
use Bookmarks\Config\SettingsRepository;

/**
* Generate the list for custom markup list
*/
class BookmarkListTypeCustom extends BookmarkListTypeBase
{

	public function __construct($list_options)
	{
		parent::__construct($list_options);
	}

	public function listing($bookmark)
	{
		return $this->listing_presenter->present($this->list_options, $this->list_options->custom_markup_html, $bookmark);
	}
}