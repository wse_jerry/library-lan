<?php 
namespace Bookmarks\Entities\User;

use Bookmarks\Config\SettingsRepository;
use Bookmarks\Helpers;
use Bookmarks\Entities\Bookmark\BookmarksArrayFormatter;

class UserRepository 
{
	/**
	* Settings Repository
	*/
	private $settings_repo;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
	}

	/**
	* Display button for current user
	* @return boolean
	*/
	public function getsButton()
	{
		if ( is_user_logged_in() ) return true;
		if ( $this->settings_repo->anonymous('display') ) return true;
		if ( $this->settings_repo->requireLogin() ) return true;
		if ( $this->settings_repo->redirectAnonymous() ) return true;
		return false;
	}

	/**
	* Get All of current user's bookmarks (includes all sites)
	* @return array (multidimensional)
	*/
	public function getAllBookmarks()
	{
		if ( isset($_POST['logged_in']) && intval($_POST['logged_in']) == 1 ) {
			$all_bookmarks = $this->getLoggedInBookmarks();
		} else {
			$saveType = $this->settings_repo->saveType();
			$bookmarks = ( $saveType == 'cookie' ) ? $this->getCookieBookmarks() : $this->getSessionBookmarks();
			$all_bookmarks = $this->bookmarksWithSiteID($bookmarks);			
		}
		
		/**
		 * Filter All of current user's bookmarks.
		 * 
		 * @since	1.3.0
		 * @param	array	The original current user's bookmarks.
		 */
		$all_bookmarks = apply_filters('bookmarks/user/bookmarks/all', $all_bookmarks);

		return $all_bookmarks;
	}

	/**
	* Get User's Bookmarks by Site ID (includes a single site)
	* @return array (flat)
	*/
	public function getBookmarks($user_id = null, $site_id = null, $group_id = null)
	{
		$logged_in = ( isset($_POST['logged_in']) && intval($_POST['logged_in']) == 1 && isset($_POST['user_id']) ) ? true : false;
		if ( $logged_in || is_user_logged_in() || $user_id ) {
			$bookmarks = $this->getLoggedInBookmarks($user_id, $site_id, $group_id);
		} else {
			$saveType = $this->settings_repo->saveType();
			$bookmarks = ( $saveType == 'cookie' ) 
				? $this->getCookieBookmarks($site_id, $group_id) 
				: $this->getSessionBookmarks($site_id, $group_id);
		}
		
		/**
		 * Filter a User's Bookmarks.
		 * 
		 * @since	1.3.0
		 * @param	array	The original User's Bookmarks.
		 */
		$bookmarks = apply_filters('bookmarks/user/bookmarks', $bookmarks);

		return $bookmarks;
	}

	/**
	* Check for Site ID in user's bookmarks
	* Multisite Compatibility for >1.1
	* 1.2 compatibility with new naming structure
	* @since 1.1
	*/
	private function bookmarksWithSiteID($bookmarks)
	{
		if ( Helpers::keyExists('site_bookmarks', $bookmarks) ){
			foreach($bookmarks as $key => $site_bookmarks){
				if ( !isset($bookmarks[$key]['site_bookmarks']) ) continue;
				$bookmarks[$key]['posts'] = $bookmarks[$key]['site_bookmarks'];
				unset($bookmarks[$key]['site_bookmarks']);
				if ( isset($bookmarks[$key]['total']) ) unset($bookmarks[$key]['total']);
			}
		}
		if ( Helpers::keyExists('site_id', $bookmarks) ) return $bookmarks;
		$new_bookmarks = array(
			array(
				'site_id' => 1,
				'posts' => $bookmarks
			)
		);
		return $new_bookmarks;
	}

	/**
	* Check for Groups array in user's bookmarks
	* Add all bookmarks to the default group if it doesn't exist
	* Compatibility for < 2.2
	* @since 2.2
	*/
	private function bookmarksWithGroups($bookmarks)
	{
		if ( Helpers::groupsExist($bookmarks[0]) ) return $bookmarks;
		$data = [
			'group_id' => 1,
			'site_id' => $bookmarks[0]['site_id'],
			'group_name' => __('Default List', 'bookmarks'),
			'posts' => $bookmarks[0]['posts']
		];
		$bookmarks[0]['groups'] = array(
			$data
		);
		return $bookmarks;
	}

	/**
	* Get Logged In User Bookmarks
	*/
	private function getLoggedInBookmarks($user_id = null, $site_id = null, $group_id = null)
	{
		$user_id_post = ( isset($_POST['user_id']) ) ? intval($_POST['user_id']) : get_current_user_id();
		$user_id = ( !is_null($user_id) ) ? $user_id : $user_id_post;
		$bookmarks = get_user_meta($user_id, 'simplebookmarks');
		if ( empty($bookmarks) ) return array(array('site_id'=> 1, 'posts' => array(), 'groups' => array() ));
		
		$bookmarks = $this->bookmarksWithSiteID($bookmarks[0]);
		$bookmarks = $this->bookmarksWithGroups($bookmarks);

		if ( !is_null($site_id) && is_null($group_id) ) $bookmarks = Helpers::pluckSiteBookmarks($site_id, $bookmarks);
		if ( !is_null($group_id) ) $bookmarks = Helpers::pluckGroupBookmarks($group_id, $site_id, $bookmarks);

		return $bookmarks;
	}

	/**
	* Get Session Bookmarks
	*/
	private function getSessionBookmarks($site_id = null, $group_id = null)
	{
		if ( !isset($_SESSION['simplebookmarks']) ) $_SESSION['simplebookmarks'] = array();
		$bookmarks = $_SESSION['simplebookmarks'];
		$bookmarks = $this->bookmarksWithSiteID($bookmarks);
		$bookmarks = $this->bookmarksWithGroups($bookmarks);
		if ( !is_null($site_id) && is_null($group_id) ) $bookmarks = Helpers::pluckSiteBookmarks($site_id, $bookmarks);
		if ( !is_null($group_id) ) $bookmarks = Helpers::pluckGroupBookmarks($group_id, $site_id, $bookmarks);
		return $bookmarks;
	}

	/**
	* Get Cookie Bookmarks
	*/
	private function getCookieBookmarks($site_id = null, $group_id = null)
	{
		if ( !isset($_COOKIE['simplebookmarks']) ) $_COOKIE['simplebookmarks'] = json_encode(array());
		$bookmarks = json_decode(stripslashes($_COOKIE['simplebookmarks']), true);
		$bookmarks = $this->bookmarksWithSiteID($bookmarks);
		$bookmarks = $this->bookmarksWithGroups($bookmarks);
		if ( isset($_POST['user_consent_accepted']) && $_POST['user_consent_accepted'] == 'true' ) $bookmarks[0]['consent_provided'] = time();
		if ( !is_null($site_id) && is_null($group_id) ) $bookmarks = Helpers::pluckSiteBookmarks($site_id, $bookmarks);
		if ( !is_null($group_id) ) $bookmarks = Helpers::pluckGroupBookmarks($group_id, $site_id, $bookmarks);
		return $bookmarks;
	}

	/**
	* Has the user bookmarkd a specified post?
	* @param int $post_id
	* @param int $site_id
	* @param int $user_id
	* @param int $group_id
	*/
	public function isBookmark($post_id, $site_id = 1, $user_id = null, $group_id = null)
	{
		$bookmarks = $this->getBookmarks($user_id, $site_id, $group_id);
		if ( in_array($post_id, $bookmarks) ) return true;
		return false;
	}

	/**
	* Does the user count in total bookmarks?
	* @return boolean
	*/
	public function countsInTotal()
	{
		if ( is_user_logged_in() ) return true;
		return $this->settings_repo->anonymous('save');
	}

	/**
	* Format an array of bookmarks
	* @param $post_id - int, post to add to array (for session/cookie bookmarks)
	* @param $site_id - int, site id for post_id
	*/
	public function formattedBookmarks($post_id = null, $site_id = null, $status = null)
	{
		$bookmarks = $this->getAllBookmarks();
		$formatter = new BookmarksArrayFormatter;
		return $formatter->format($bookmarks, $post_id, $site_id, $status);
	}

	/**
	* Has the user consented to cookies (if applicable)
	*/
	public function consentedToCookies()
	{
		if ( $this->settings_repo->saveType() !== 'cookie' ) return true;
		if ( isset($_POST['user_consent_accepted']) && $_POST['user_consent_accepted'] == 'true' ) return true;
		if ( !$this->settings_repo->consent('require') ) return true;
		if ( isset($_COOKIE['simplebookmarks']) ){
			$cookie = json_decode(stripslashes($_COOKIE['simplebookmarks']), true);
			if ( isset($cookie[0]['consent_provided']) ) return true;
			if ( isset($cookie[0]['consent_denied']) ) return false;
		}
		return false;
	}

	/**
	* Has the user denied consent to cookies explicitly
	*/
	public function deniedCookies()
	{
		if ( $this->settings_repo->saveType() !== 'cookie' ) return false;
		if ( !$this->settings_repo->consent('require') ) return false;
		if ( isset($_COOKIE['simplebookmarks']) ){
			$cookie = json_decode(stripslashes($_COOKIE['simplebookmarks']), true);
			if ( isset($cookie[0]['consent_denied']) ) return true;
		}
		return false;
	}
}