<?php 
namespace Bookmarks\Entities\Bookmark;

/**
* Filters an array of bookmarks using provided array of filters
*/
class BookmarkFilter 
{
	/**
	* Bookmarks
	* @var array of post IDs
	*/
	private $bookmarks;

	/**
	* Filters
	* @var array
	*
	* Example: 
	*
	* array(
	* 	'post_type' => array('post', 'posttypetwo'),
	*	'terms' => array(
	*		'category' => array(
	*			'termone', 'termtwo', 'termthree'
	*		),
	*		'other-taxonomy' => array(
	*			'termone', 'termtwo', 'termthree'
	*		)
	*	)
	* );
	*
	*/
	private $filters;

	public function __construct($bookmarks, $filters)
	{
		$this->bookmarks = $bookmarks;
		$this->filters = $filters;
	}

	public function filter()
	{
		if ( isset($this->filters['post_type']) && is_array($this->filters['post_type']) ) $this->filterByPostType();
		if ( isset($this->filters['terms']) && is_array($this->filters['terms']) ) $this->filterByTerm();
		if ( isset($this->filters['status']) && is_array($this->filters['status']) ) $this->filterByStatus();
		return $this->bookmarks;
	}

	/**
	* Filter bookmarks by post type
	* @since 1.1.1
	* @param array $bookmarks
	*/
	private function filterByPostType()
	{
		foreach($this->bookmarks as $key => $bookmark){
			$post_type = get_post_type($bookmark);
			if ( !in_array($post_type, $this->filters['post_type']) ) unset($this->bookmarks[$key]);
		}
	}

	/**
	* Filter bookmarks by status
	* @since 2.1.4
	*/
	private function filterByStatus()
	{
		foreach($this->bookmarks as $key => $bookmark){
			$status = get_post_status($bookmark);
			if ( !in_array($status, $this->filters['status']) ) unset($this->bookmarks[$key]);
		}
	}

	/**
	* Filter bookmarks by terms
	* @since 1.1.1
	* @param array $bookmarks
	*/
	private function filterByTerm()
	{
		$taxonomies = $this->filters['terms'];
		$bookmarks = $this->bookmarks;
		
		foreach ( $bookmarks as $key => $bookmark ) :

			$all_terms = array();
			$all_filters = array();

			foreach ( $taxonomies as $taxonomy => $terms ){
				if ( !isset($terms) || !is_array($terms) ) continue;
				$post_terms = wp_get_post_terms($bookmark, $taxonomy, array("fields" => "slugs"));
				if ( !empty($post_terms) ) $all_terms = array_merge($all_terms, $post_terms);
				$all_filters = array_merge($all_filters, $terms);
			}

			$dif = array_diff($all_filters, $all_terms);
			if ( !empty($dif) ) unset($bookmarks[$key]);		

		endforeach;

		$this->bookmarks = $bookmarks;
	}
}