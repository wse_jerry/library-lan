<?php
namespace Bookmarks\Entities\Bookmark;

use Bookmarks\Entities\Post\BookmarkCount;
use Bookmarks\Entities\Bookmark\BookmarkButton;
use Bookmarks\Entities\BookmarkList\BookmarkList;

/**
* Format the user's bookmark array to include additional post data
*/
class BookmarksArrayFormatter
{
	/**
	* Formatted bookmarks array
	*/
	private $formatted_bookmarks;

	/**
	* Total Bookmarks Counter
	*/
	private $counter;

	/**
	* Post ID to add to return array
	* For adding/removing session/cookie bookmarks for current request
	* @var int
	*/
	private $post_id;

	/**
	* Site ID for post to add to array
	* For adding/removing session/cookie bookmarks for current request
	* @var int
	*/
	private $site_id;

	/**
	* Site ID for post to add to array
	* For adding/removing session/cookie bookmarks for current request
	* @var string
	*/
	private $status;

	public function __construct()
	{
		$this->counter = new BookmarkCount;
	}

	public function format($bookmarks, $post_id = null, $site_id = null, $status = null)
	{
		$this->formatted_bookmarks = $bookmarks;
		$this->post_id = $post_id;
		$this->site_id = $site_id;
		$this->status = $status;
		$this->resetIndexes();
		$this->addPostData();
		return $this->formatted_bookmarks;
	}

	/**
	* Reset the bookmark indexes
	*/
	private function resetIndexes()
	{
		foreach ( $this->formatted_bookmarks as $site => $site_bookmarks ){
			// Make older posts compatible with new name
			if ( !isset($site_bookmarks['posts']) ) {
				$site_bookmarks['posts'] = $site_bookmarks['site_bookmarks'];
				unset($this->formatted_bookmarks[$site]['site_bookmarks']);
			}
			foreach ( $site_bookmarks['posts'] as $key => $bookmark ){
				unset($this->formatted_bookmarks[$site]['posts'][$key]);
				$this->formatted_bookmarks[$site]['posts'][$bookmark]['post_id'] = $bookmark;
			}
		}
	}

	/**
	* Add the post type to each bookmark
	*/
	private function addPostData()
	{
		$this->checkCurrentPost();
		foreach ( $this->formatted_bookmarks as $site => $site_bookmarks ){
			foreach ( $site_bookmarks['posts'] as $key => $bookmark ){
				$site_id = $this->formatted_bookmarks[$site]['site_id'];
				$this->formatted_bookmarks[$site]['posts'][$key]['post_type'] = get_post_type($key);
				$this->formatted_bookmarks[$site]['posts'][$key]['title'] = get_the_title($key);
				$this->formatted_bookmarks[$site]['posts'][$key]['permalink'] = get_the_permalink($key);
				$this->formatted_bookmarks[$site]['posts'][$key]['total'] = $this->counter->getCount($key, $site_id);
				$this->formatted_bookmarks[$site]['posts'][$key]['thumbnails'] = $this->getThumbnails($key);
				$this->formatted_bookmarks[$site]['posts'][$key]['excerpt'] = apply_filters('the_excerpt', get_post_field('post_excerpt', $key));
				$button = new BookmarkButton($key, $site_id);
				$this->formatted_bookmarks[$site]['posts'][$key]['button'] = $button->display(false);
			}
			$this->formatted_bookmarks[$site] = array_reverse($this->formatted_bookmarks[$site]);
		}
	}

	/**
	* Make sure the current post is updated in the array
	* (for cookie/session bookmarks, so AJAX response returns array with correct posts without page refresh)
	*/
	private function checkCurrentPost()
	{
		if ( !isset($this->post_id) || !isset($this->site_id) ) return;
		if ( isset($_POST['logged_in']) && $_POST['logged_in'] == '1' ) return;
		foreach ( $this->formatted_bookmarks as $site => $site_bookmarks ){
			if ( $site_bookmarks['site_id'] == $this->site_id ) {
				if ( isset($site_bookmarks['posts'][$this->post_id]) && $this->status == 'inactive' ){
					unset($this->formatted_bookmarks[$site]['posts'][$this->post_id]);
				} else {
					$this->formatted_bookmarks[$site]['posts'][$this->post_id] = array('post_id' => $this->post_id);
				}
			}
		}
	}

	/**
	* Add thumbnail urls to the array
	*/
	private function getThumbnails($post_id)
	{
		if ( !has_post_thumbnail($post_id) ) return false;
		$sizes = get_intermediate_image_sizes();
		$thumbnails = array();
		foreach ( $sizes as $size ){
			$url = get_the_post_thumbnail_url($post_id, $size);
			$img = '<img src="' . $url . '" alt="' . get_the_title($post_id) . '" class="bookmarks-list-thumbnail" />';
			$thumbnails[$size] = apply_filters('bookmarks/list/thumbnail', $img, $post_id, $size);
		}
		return $thumbnails;
	}
}