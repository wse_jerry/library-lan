<?php 
namespace Bookmarks\Entities\Bookmark;

use Bookmarks\Entities\User\UserRepository;
use Bookmarks\Helpers;

/**
* Sync a single bookmark to a given save type
*/
class SyncSingleBookmark 
{
	/**
	* The Post ID
	*/
	private $post_id;

	/**
	* The Site ID
	*/
	private $site_id;

	/**
	* The Group ID
	*/
	private $group_id;

	/**
	* User Repository
	*/
	private $user;

	public function __construct($post_id, $site_id, $group_id = 1)
	{
		$this->user = new UserRepository;
		$this->post_id = $post_id;
		$this->site_id = $site_id;
		$this->group_id = $group_id;
	}

	/**
	* Sync a Session Bookmark
	*/
	public function session()
	{
		if ( $this->user->isBookmark($this->post_id, $this->site_id) ) return $_SESSION['simplebookmarks'] = $this->removeBookmark();
		return $_SESSION['simplebookmarks'] = $this->addBookmark();
	}

	/**
	* Sync a Cookie Bookmark
	*/
	public function cookie()
	{
		if ( $this->user->isBookmark($this->post_id, $this->site_id) ){
			$bookmarks = $this->removeBookmark();
			setcookie( 'simplebookmarks', json_encode( $bookmarks ), time() + apply_filters( 'simplebookmarks_cookie_expiration_interval', 31556926 ), '/' );
			return;
		}
		$bookmarks = $this->addBookmark();
		setcookie( 'simplebookmarks', json_encode( $bookmarks ), time() + apply_filters( 'simplebookmarks_cookie_expiration_interval', 31556926 ), '/' );
		return;
	}

	/**
	* Update User Meta (logged in only)
	*/
	public function updateUserMeta($bookmarks)
	{
		if ( !isset($_POST['logged_in']) || intval($_POST['logged_in']) !== 1 ) return false;
		if ( !isset($_POST['user_id']) ) return false;
		update_user_meta( intval($_POST['user_id']), 'simplebookmarks', $bookmarks );
	}

	/**
	* Remove a Bookmark
	*/
	private function removeBookmark()
	{
		$bookmarks = $this->user->getAllBookmarks($this->site_id);

		foreach($bookmarks as $key => $site_bookmarks){
			if ( $site_bookmarks['site_id'] !== $this->site_id ) continue;
			foreach($site_bookmarks['posts'] as $k => $fav){
				if ( $fav == $this->post_id ) unset($bookmarks[$key]['posts'][$k]);
			}
			if ( !Helpers::groupsExist($site_bookmarks) ) return;
			foreach( $site_bookmarks['groups'] as $group_key => $group){
				if ( $group['group_id'] !== $this->group_id ) continue;
				foreach ( $group['posts'] as $k => $g_post_id ){
					if ( $g_post_id == $this->post_id ) unset($bookmarks[$key]['groups'][$group_key]['posts'][$k]);
				}
			}
		}
		$this->updateUserMeta($bookmarks);
		return $bookmarks;
	}

	/**
	* Add a Bookmark
	*/
	private function addBookmark()
	{
		$bookmarks = $this->user->getAllBookmarks($this->site_id);
		if ( !Helpers::siteExists($this->site_id, $bookmarks) ){
			$bookmarks[] = array(
				'site_id' => $this->site_id,
				'posts' => array()
			);
		}
		// Loop through each site's bookmarks, continue if not the correct site id
		foreach($bookmarks as $key => $site_bookmarks){
			if ( $site_bookmarks['site_id'] !== $this->site_id ) continue;
			$bookmarks[$key]['posts'][] = $this->post_id;

			// Add the default group if it doesn't exist yet
			if ( !Helpers::groupsExist($site_bookmarks) ){
				$bookmarks[$key]['groups'] = array(
					array(
						'group_id' => 1,
						'site_id' => $this->site_id,
						'group_name' => __('Default List', 'bookmarks'),
						'posts' => array()
					)
				);
			}
			foreach( $bookmarks[$key]['groups'] as $group_key => $group){
				if ( $group['group_id'] == $this->group_id ) 
					$bookmarks[$key]['groups'][$group_key]['posts'][] = $this->post_id;
			}
		}
		$this->updateUserMeta($bookmarks);
		return $bookmarks;
	}
}