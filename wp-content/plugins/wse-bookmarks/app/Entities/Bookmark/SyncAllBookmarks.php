<?php
namespace Bookmarks\Entities\Bookmark;

use Bookmarks\Config\SettingsRepository;

/**
* Sync all bookmarks for a specific site
*/
class SyncAllBookmarks
{
	/**
	* Bookmarks to Save
	* @var array
	*/
	private $bookmarks;

	/**
	* Settings Repository
	*/
	private $settings_repo;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
	}

	/**
	* Sync the bookmarks
	*/
	public function sync($bookmarks)
	{
		$this->bookmarks = $bookmarks;
		$saveType = $this->settings_repo->saveType();
		$this->$saveType();
		$this->updateUserMeta();
	}

	/**
	* Sync Session Bookmarks
	*/
	private function session()
	{
		return $_SESSION['simplebookmarks'] = $this->bookmarks;
	}

	/**
	* Sync a Cookie Bookmark
	*/
	public function cookie()
	{
		setcookie( 'simplebookmarks', json_encode( $this->bookmarks ), time() + apply_filters( 'simplebookmarks_cookie_expiration_interval', 31556926 ), '/' );
		return;
	}

	/**
	* Update User Meta (logged in only)
	*/
	private function updateUserMeta()
	{
		if ( !isset($_POST['user_id']) ) return false;
		if ( !isset($_POST['logged_in']) && intval($_POST['logged_in']) !== 1 ) return false;
		return update_user_meta( intval($_POST['user_id']), 'simplebookmarks', $this->bookmarks );
	}
}