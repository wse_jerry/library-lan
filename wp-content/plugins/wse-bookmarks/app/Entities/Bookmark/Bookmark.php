<?php 
namespace Bookmarks\Entities\Bookmark;

use Bookmarks\Config\SettingsRepository;
use Bookmarks\Entities\Bookmark\SyncSingleBookmark;
use Bookmarks\Entities\Post\SyncBookmarkCount;

class Bookmark 
{
	/**
	* Settings Repository
	*/
	private $settings_repo;

	/**
	* Save Type
	*/
	private $save_type;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
	}

	/**
	* Save the Bookmark
	*/
	public function update($post_id, $status, $site_id, $group_id = 1)
	{
		$this->save_type = $this->settings_repo->saveType();
		$usersync = new SyncSingleBookmark($post_id, $site_id, $group_id);
		$saveType = $this->save_type;
		$usersync->$saveType();
		
		$postsync = new SyncBookmarkCount($post_id, $status, $site_id);
		$postsync->sync();
	}

	/**
	* Get the Save Type
	*/
	public function saveType()
	{
		return $this->save_type;
	}
}