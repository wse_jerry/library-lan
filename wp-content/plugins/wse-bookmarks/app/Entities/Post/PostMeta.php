<?php 
namespace Bookmarks\Entities\Post;

use Bookmarks\Config\SettingsRepository;
use Bookmarks\Entities\Post\BookmarkCount;

class PostMeta 
{
	/**
	* Settings Repository
	*/
	private $settings_repo;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
		add_action( 'add_meta_boxes', array($this, 'bookmarkCountBox') );
	}

	/**
	* Add the Bookmark Count Meta Box for enabled Types
	*/
	public function bookmarkCountBox()
	{
		foreach ( $this->settings_repo->metaEnabled() as $type ){
			add_meta_box(
				'bookmarks',
				__( 'Bookmarks', 'bookmarks' ),
				array($this, 'bookmarkCount'),
				$type,
				'side',
				'low'
			);
		}
	}

	/**
	* The bookmark count
	*/
	public function bookmarkCount()
	{
		global $post;
		$count = new BookmarkCount;
		echo '<strong>' . __('Total Bookmarks', 'bookmarks') . ':</strong> ';
		echo $count->getCount($post->ID);
		echo '<input type="hidden" name="simplebookmarks_count" value="' . $count->getCount($post->ID) . '">';
	}
}