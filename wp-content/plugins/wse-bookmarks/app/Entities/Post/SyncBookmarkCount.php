<?php 
namespace Bookmarks\Entities\Post;

use Bookmarks\Entities\Post\BookmarkCount;
use Bookmarks\Entities\User\UserRepository;

/**
* Updates the bookmark count for a given post
*/
class SyncBookmarkCount 
{
	/**
	* Post ID
	* @var int
	*/
	private $post_id;

	/**
	* Site ID
	* @var int
	*/
	private $site_id;

	/**
	* Status
	* @var string
	*/
	private $status;

	/**
	* Bookmark Count
	* @var object
	*/
	private $bookmark_count;

	/**
	* User Repository
	*/
	private $user;

	public function __construct($post_id, $status, $site_id)
	{
		$this->post_id = $post_id;
		$this->status = $status;
		$this->site_id = $site_id;
		$this->bookmark_count = new BookmarkCount;
		$this->user = new UserRepository;
	}

	/**
	* Sync the Post Total Bookmarks
	*/
	public function sync()
	{
		if ( !$this->user->countsInTotal() ) return false;
		$count = $this->bookmark_count->getCount($this->post_id, $this->site_id);
		$count = ( $this->status == 'active' ) ? $count + 1 : max(0, $count - 1);
		return update_post_meta($this->post_id, 'simplebookmarks_count', $count);
	}
}