<?php 
namespace Bookmarks\Config;

use Bookmarks\Config\SettingsRepository;
use Bookmarks\Entities\PostType\PostTypeRepository;
use Bookmarks\Helpers;

/**
* Plugin Settings
*/
class Settings 
{
	/**
	* Plugin Name
	*/
	private $plugin_name;

	/**
	* Settings Repository
	*/
	private $settings_repo;

	/**
	* Post Type Repository
	*/
	private $post_type_repo;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
		$this->post_type_repo = new PostTypeRepository;
		$this->setName();
		add_action( 'admin_init', array( $this, 'registerSettings' ) );
		add_action( 'admin_menu', array( $this, 'registerSettingsPage' ) );
	}

	/**
	* Set the plugin name
	*/
	private function setName()
	{
		global $bookmarks_name;
		$this->plugin_name = $bookmarks_name;
	}

	/**
	* Register the settings page
	*/
	public function registerSettingsPage()
	{
		add_options_page( 
			$this->plugin_name . ' ' . __('Settings', 'bookmarks'),
			$this->plugin_name,
			'manage_options',
			'simple-bookmarks', 
			array( $this, 'settingsPage' ) 
		);
	}

	/**
	* Display the Settings Page
	*/
	public function settingsPage()
	{
		$tab = ( isset($_GET['tab']) ) ? $_GET['tab'] : 'general';
		include( Helpers::view('settings/settings') );
	}

	/**
	* Register the settings
	*/
	public function registerSettings()
	{
		register_setting( 'simple-bookmarks-general', 'simplebookmarks_dependencies' );
		register_setting( 'simple-bookmarks-general', 'simplebookmarks_cache_enabled' );
		register_setting( 'simple-bookmarks-general', 'simplebookmarks_dev_mode');
		register_setting( 'simple-bookmarks-users', 'simplebookmarks_users' );
		register_setting( 'simple-bookmarks-display', 'simplebookmarks_display' );
	}
}