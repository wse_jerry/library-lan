<?php 
namespace Bookmarks;

use Bookmarks\Config\SettingsRepository;

/**
* Plugin Bootstrap
*/
class Bootstrap 
{
	/**
	* Settings Repository
	* @var object
	*/
	private $settings_repo;

	public function __construct()
	{
		$this->settings_repo = new SettingsRepository;
		add_action( 'init', array($this, 'init') );
		add_action( 'admin_init', array($this, 'adminInit'));
		add_filter( 'plugin_action_links_' . 'bookmarks/bookmarks.php', array($this, 'settingsLink' ) );
		add_action( 'plugins_loaded', array($this, 'addLocalization') );
	}

	/**
	* Initialize
	*/
	public function init()
	{
		new Config\Settings;
		new Activation\Activate;
		new Activation\Dependencies;
		new Entities\Post\PostHooks;
		new Events\RegisterPublicEvents;
		new Entities\Post\PostMeta;
		new API\Shortcodes\ButtonShortcode;
		new API\Shortcodes\BookmarkCountShortcode;
		new API\Shortcodes\UserBookmarksShortcode;
		new API\Shortcodes\UserBookmarkCount;
		new API\Shortcodes\PostBookmarksShortcode;
		new API\Shortcodes\ClearBookmarksShortcode;
		$this->startSession();
	}

	/**
	* Admin Init
	*/
	public function adminInit()
	{
		new Entities\Post\AdminColumns;
	}

	/**
	* Add a link to the settings on the plugin page
	*/
	public function settingsLink($links)
	{ 
		$settings_link = '<a href="options-general.php?page=simple-bookmarks">' . __('Settings', 'bookmarks') . '</a>'; 
		$help_link = '<a href="http://bookmarkposts.com">' . __('FAQ', 'bookmarks') . '</a>'; 
		array_unshift($links, $help_link); 
		array_unshift($links, $settings_link);
		return $links; 
	}

	/**
	* Localization Domain
	*/
	public function addLocalization()
	{
		load_plugin_textdomain(
			'bookmarks', 
			false, 
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages' );
	}

	/**
	* Initialize a Session
	*/
	public function startSession()
	{
		if ( $this->settings_repo->saveType() !== 'session' ) return;
		if ( !session_id() ) session_start();
	}
}