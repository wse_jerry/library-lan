<?php 
/**
* Static Wrapper for Bootstrap Class
* Prevents T_STRING error when checking for 5.3.2
*/
class Bookmarks 
{
	public static function init()
	{
		// dev/live
		global $bookmarks_env;
		$bookmarks_env = 'live';

		global $bookmarks_version;
		$bookmarks_version = '2.2.0';

		global $bookmarks_name;
		$bookmarks_name = __('Bookmarks', 'bookmarks');

		$app = new Bookmarks\Bootstrap;
	}
}