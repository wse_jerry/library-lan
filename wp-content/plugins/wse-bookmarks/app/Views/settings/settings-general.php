<?php settings_fields( 'simple-bookmarks-general' ); ?>

<h3><?php _e('Page Cache', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-post-types">
	<div class="post-type-row">
		<div class="post-type-checkbox">
			<input type="checkbox" name="simplebookmarks_cache_enabled" value="true" <?php if ( $this->settings_repo->cacheEnabled() ) echo 'checked'; ?> />
		</div>
		<div class="post-type-name">
			<?php _e('Cache Enabled on Site (Bookmarks content is injected on page load with AJAX request)', 'bookmarks'); ?>
		</div>
	</div><!-- .post-type-row -->
</div><!-- .simple-bookmarks-post-types -->

<h3><?php _e('Development Mode', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-post-types">
	<div class="post-type-row">
		<div class="post-type-checkbox">
			<input type="checkbox" name="simplebookmarks_dev_mode" value="true" <?php if ( $this->settings_repo->devMode() ) echo 'checked'; ?> />
		</div>
		<div class="post-type-name">
			<?php _e('Enable Development Mode (logs JS responses in the console for debugging)'); ?>
		</div>
	</div><!-- .post-type-row -->
</div><!-- .simple-bookmarks-post-types -->

<h3><?php _e('Dependencies', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-display-settings">
	<div class="row">
		<div class="description">
			<h5><?php _e('Enqueue Plugin CSS', 'bookmarks'); ?></h5>
		</div>
		<div class="field">
			<label class="block"><input type="checkbox" name="simplebookmarks_dependencies[css]" value="true" data-bookmarks-dependency-checkbox <?php if ( $this->settings_repo->outputDependency('css') ) echo 'checked'; ?> /><?php _e('Output Plugin CSS', 'bookmarks'); ?>
			</label>
			<div class="simplebookmarks-dependency-content" data-bookmarks-dependency-content>
				<p><em><?php _e('If you are compiling your own minified CSS, include the CSS below:', 'bookmarks'); ?></em></p>
				<textarea><?php echo Bookmarks\Helpers::getFileContents('assets/css/styles-uncompressed.css'); ?></textarea>
			</div>
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('Enqueue Plugin Javascript', 'bookmarks'); ?></h5>
			<p><?php _e('Important: The plugin JavaScript is required for core functions. If this is disabled, the plugin JS <strong>must</strong> be included with the theme along with the global JS variables.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block">
				<input type="checkbox" name="simplebookmarks_dependencies[js]" value="true" data-bookmarks-dependency-checkbox <?php if ( $this->settings_repo->outputDependency('js') ) echo 'checked'; ?> /><?php _e('Output Plugin JavaScript', 'bookmarks'); ?>
			</label>
			<div class="simplebookmarks-dependency-content" data-bookmarks-dependency-content>
				<p><em><?php _e('If you are compiling your own minified Javascript, include the below (required for plugin functionality):', 'bookmarks'); ?></em></p>
				<textarea><?php echo Bookmarks\Helpers::getFileContents('assets/js/bookmarks.js'); ?></textarea>
			</div>
		</div>
	</div><!-- .row -->
</div><!-- .bookmarks-display-settings -->

<div class="bookmarks-alert">
	<p><strong><?php _e('Bookmarks Version', 'bookmarks'); ?>:</strong> <?php echo Bookmarks\Helpers::version(); ?></p>
</div>