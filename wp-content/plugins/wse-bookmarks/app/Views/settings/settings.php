<div class="wrap">
	<h1><?php echo $this->plugin_name . ' '; _e('Settings', 'bookmarks'); ?></h1>

	<h2 class="nav-tab-wrapper">
		<a class="nav-tab <?php if ( $tab == 'general' ) echo 'nav-tab-active'; ?>" href="options-general.php?page=simple-bookmarks">
			<?php _e('General', 'bookmarks'); ?>
		</a>
		<a class="nav-tab <?php if ( $tab == 'users' ) echo 'nav-tab-active'; ?>" href="options-general.php?page=simple-bookmarks&tab=users">
			<?php _e('Users', 'bookmarks'); ?>
		</a>
		<a class="nav-tab <?php if ( $tab == 'display' ) echo 'nav-tab-active'; ?>" href="options-general.php?page=simple-bookmarks&tab=display">
			<?php _e('Display & Post Types', 'bookmarks'); ?>
		</a>
	</h2>

	<form method="post" enctype="multipart/form-data" action="options.php">
		<?php include(Bookmarks\Helpers::view('settings/settings-' . $tab)); ?>
		<?php submit_button(); ?>
	</form>
</div><!-- .wrap -->