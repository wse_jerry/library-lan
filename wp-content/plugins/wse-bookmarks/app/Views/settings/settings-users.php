<?php settings_fields( 'simple-bookmarks-users' ); ?>

<h3><?php _e('User Settings', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-display-settings">
	<div class="row">
		<div class="description">
			<h5><?php _e('Anonymous Users', 'bookmarks'); ?></h5>
			<p><?php _e('Enable favoriting functionality for unauthenticated users.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><input type="checkbox" name="simplebookmarks_users[anonymous][display]" value="true" <?php if ( $this->settings_repo->anonymous('display') ) echo ' checked'; ?> data-bookmarks-anonymous-checkbox /><?php _e('Enable Anonymous Users', 'bookmarks'); ?>
			</label>
			<label class="block" data-bookmarks-anonymous-count><input type="checkbox" name="simplebookmarks_users[anonymous][save]" value="true" <?php if ( $this->settings_repo->anonymous('save') ) echo ' checked'; ?> /><?php _e('Include in Post Bookmark Count', 'bookmarks'); ?>
			</label>
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('User Cookie Consent', 'bookmarks'); ?></h5>
			<p><?php _e('Require user consent for saving cookies before allowing bookmarks to be saved.', 'bookmarks'); ?></p><p><strong><?php _e('Important:', 'bookmarks'); ?></strong> <?php _e('If using this option for GDPR compliance, please consult an attorney for appropriate legal terms to display in the modal consent.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><input type="checkbox" name="simplebookmarks_users[consent][require]" value="true" <?php if ( $this->settings_repo->consent('require') ) echo ' checked'; ?> data-bookmarks-require-consent-checkbox /><?php _e('Require User Consent', 'bookmarks'); ?>
			</label>
			<div class="require-consent-modal-content" data-bookmarks-require-consent-modal-content>
				<h3><?php _e('Content to Display in Modal Agreement', 'bookmarks'); ?></h3>
				<?php
					wp_editor($this->settings_repo->consent('modal'), 'simplebookmarks_users_authentication_modal', 
					array(
						'textarea_name' => 'simplebookmarks_users[consent][modal]',
						'tabindex' => 1,
						'wpautop' => true
						)
					); 
				?>
				<p>
					<label class="block"><?php _e('Consent Button Text', 'bookmarks'); ?></label>
					<input type="text" name="simplebookmarks_users[consent][consent_button_text]" value="<?php echo $this->settings_repo->consent('consent_button_text'); ?>" />
				</p>
				<p>
					<label class="block"><?php _e('Deny Button Text', 'bookmarks'); ?></label>
					<input type="text" name="simplebookmarks_users[consent][deny_button_text]" value="<?php echo $this->settings_repo->consent('deny_button_text'); ?>" />
				</p>
			</div>
		</div>
	</div><!-- .row -->
	<div class="row" data-bookmarks-require-login>
		<div class="description">
			<h5><?php _e('Anonymous Favoriting Behavior', 'bookmarks'); ?></h5>
			<p><?php _e('By default, bookmark buttons are hidden from unauthenticated users if anonymous users are disabled.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><input type="checkbox" name="simplebookmarks_users[require_login]" value="true" <?php if ( $this->settings_repo->requireLogin() ) echo ' checked'; ?> data-bookmarks-require-login-checkbox data-bookmarks-anonymous-settings="modal" /><?php _e('Show Buttons and Display Modal for Anonymous Users', 'bookmarks'); ?>
			</label>
			<label class="block"><input type="checkbox" name="simplebookmarks_users[redirect_anonymous]" value="true" <?php if ( $this->settings_repo->redirectAnonymous() ) echo ' checked'; ?> data-bookmarks-redirect-anonymous-checkbox data-bookmarks-anonymous-settings="redirect" /><?php _e('Redirect Anonymous Users to a Page', 'bookmarks'); ?>
			</label>
			<div class="authentication-modal-content" data-bookmarks-authentication-modal-content>
				<h3><?php _e('Edit the Modal Content Below', 'bookmarks'); ?></h3>
				<p><strong><?php _e('Important: ', 'bookmarks'); ?></strong> <?php _e('If plugin css or javascript has been disabled, the modal window will not display correctly.', 'bookmarks'); ?></p>
				<p><?php _e('To add "close" button or link, give it a data attribute of "data-bookmarks-modal-close".', 'bookmarks'); ?></p>
				<?php
					wp_editor($this->settings_repo->authenticationModalContent(true), 'simplebookmarks_users_authentication_modal', 
					array(
						'textarea_name' => 'simplebookmarks_users[authentication_modal]',
						'tabindex' => 1,
						'wpautop' => true
						)
					); 
				?>
			</div>
			<div class="anonymous-redirect-content" data-bookmarks-anonymous-redirect-content>
				<label><?php _e('Enter the Page/Post ID to redirect to (defaults to the site url)', 'sscblog'); ?></label>
				<input type="text" name="simplebookmarks_users[anonymous_redirect_id]" value="<?php echo $this->settings_repo->redirectAnonymousId(); ?>" />
			</div>
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('Save Unauthenticated Bookmarks as', 'bookmarks'); ?></h5>
			<p><?php _e('Unauthenticated users\' bookmarks may be saved in either cookies or session. Authenticated users\' bookmarks are saved as user meta.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><input type="radio" name="simplebookmarks_users[anonymous][saveas]" value="cookie" <?php if ( $this->settings_repo->saveType() == 'cookie' ) echo 'checked'; ?>/><?php _e('Cookie', 'bookmarks'); ?>
			</label>
			<label>
				<input type="radio" name="simplebookmarks_users[anonymous][saveas]" value="session" <?php if ( $this->settings_repo->saveType() == 'session' ) echo 'checked'; ?>/><?php _e('Session', 'bookmarks'); ?>
			</label>
		</div>
	</div><!-- .row -->
</div><!-- .bookmarks-display-settings -->