<?php 
settings_fields( 'simple-bookmarks-display' ); 
$preset_buttons = $this->settings_repo->presetButton();
$button_type_selected = $this->settings_repo->getButtonType();
?>

<h3><?php _e('Enabled Bookmarks for:', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-post-types">
	<?php 
		foreach ( $this->post_type_repo->getAllPostTypes() as $posttype ) : 
		$post_type_object = get_post_type_object($posttype);
		$display = $this->settings_repo->displayInPostType($posttype);
	?>
	<div class="post-type-row">
		<div class="post-type-checkbox">
			<input type="checkbox" name="simplebookmarks_display[posttypes][<?php echo $posttype; ?>][display]" value="true" <?php if ( $display ) echo ' checked'; ?> data-bookmarks-posttype-checkbox />
		</div>
		<div class="post-type-name">
			<?php echo $post_type_object->labels->name; ?>
			<button class="button" data-bookmarks-toggle-post-type-settings <?php if ( !$display ) echo 'style="display:none;"';?>><?php _e('Settings', 'bookmarks'); ?></button>
		</div>
		<div class="post-type-settings">
			<div class="row">
				<div class="description">
					<h5><?php _e('Insert Bookmark button before content', 'bookmarks') ?></h5>
					<p><?php _e('Bookmark buttons are automatically inserted before the content using the_content filter.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<input type="checkbox" name="simplebookmarks_display[posttypes][<?php echo $posttype; ?>][before_content]" value="true" <?php if ( isset($display['before_content']) ) echo ' checked'; ?>/> <?php _e('Include before content', 'bookmarks'); ?>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Insert Bookmark button after content', 'bookmarks') ?></h5>
					<p><?php _e('Bookmark buttons are automatically inserted after the content using the_content filter.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<input type="checkbox" name="simplebookmarks_display[posttypes][<?php echo $posttype; ?>][after_content]" value="true" <?php if ( isset($display['after_content']) ) echo ' checked'; ?>/> <?php _e('Include after content', 'bookmarks'); ?>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Show bookmark count on post edit screen', 'bookmarks') ?></h5>
					<p><?php _e('Adds a meta box with the total number of bookmarks the post has received.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<input type="checkbox" name="simplebookmarks_display[posttypes][<?php echo $posttype; ?>][postmeta]" value="true" <?php if ( isset($display['postmeta']) ) echo ' checked'; ?>/> <?php _e('Add meta box', 'bookmarks'); ?>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Show bookmark count in admin columns', 'bookmarks') ?></h5>
					<p><?php _e('Adds a column to the admin listing with the total bookmark count.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<input type="checkbox" name="simplebookmarks_display[posttypes][<?php echo $posttype; ?>][admincolumns]" value="true" <?php if ( isset($display['admincolumns']) ) echo ' checked'; ?>/> <?php _e('Add admin column', 'bookmarks'); ?>
				</div>
			</div><!-- .row -->
		</div><!-- .post-type-settings -->
	</div><!-- .post-type-row -->
	<?php endforeach; ?>
</div><!-- .simple-bookmarks-post-types -->

<h3><?php _e('Bookmark Button Content & Appearance', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-display-settings">
	<div class="row">
		<div class="description">
			<h5><?php _e('Button HTML Element', 'bookmarks'); ?></h5>
			<p><?php _e('By default, the button is displayed in an HTML button element.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('Button HTML Element', 'bookmarks'); ?></label>
			<select name="simplebookmarks_display[button_element_type]">
				<?php $button_type = $this->settings_repo->getButtonHtmlType(); ?>
				<option value="button" <?php if ( $button_type == 'button' ) echo 'selected';?>><?php _e('Button', 'bookmarks'); ?></option>
				<option value="a" <?php if ( $button_type == 'a' ) echo 'selected';?>><?php _e('a (link)', 'bookmarks'); ?></option>
				<option value="div" <?php if ( $button_type == 'div' ) echo 'selected';?>><?php _e('Div', 'bookmarks'); ?></option>
				<option value="span" <?php if ( $button_type == 'span' ) echo 'selected';?>><?php _e('Span', 'bookmarks'); ?></option>
			</select>
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('Button Type', 'bookmarks'); ?></h5>
			<p><?php _e('Use a predefined button or add your own markup.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('Button Type', 'bookmarks'); ?></label>
			<select name="simplebookmarks_display[buttontype]" data-bookmarks-preset-button-select>
				<option value="custom"><?php _e('Custom Markup', 'bookmarks'); ?></option>
				<?php 
				foreach ( $preset_buttons as $button_name => $attrs ){
					$out = '<option value="' . $button_name . '"';
					if ( $button_name == $button_type_selected ) $out .= ' selected';
					$out .= '>' . $attrs['label'] . '</option>';
					echo $out;
				}
				?>
			</select>
			<div class="bookmark-button-previews" data-bookmarks-preset-button-previews>
				<h4><?php _e('Preview', 'bookmarks'); ?></h4>
				<?php
				foreach ( $preset_buttons as $button_name => $attrs ){
					$out = '<button class="simplebookmark-button preset '  . $button_name . '" data-bookmarks-button-preview="' . $button_name . '" data-bookmarks-button-active-content="' . $attrs['state_active'] . '" data-bookmarks-button-default-content="' . $attrs['state_default'] . '" data-bookmarks-button-icon="' . htmlentities($attrs['icon']) . '">' . $attrs['icon'] . ' ' . $attrs['state_default'] . ' <span class="simplebookmark-button-count" >2</span></button>';
					echo $out;
				}
				?>
			</div><!-- .bookmark-button-previews -->
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('Color Options', 'bookmarks'); ?></h5>
			<p><?php _e('If colors are not specified, theme colors will apply. Note: theme styles will effect the appearance of the bookmarks button. The button is displayed in a button element, with a css class of "simplebookmarks-button".', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><input type="checkbox" name="simplebookmarks_display[button_colors][custom]" value="true" data-bookmarks-custom-colors-checkbox <?php if ( $this->settings_repo->buttonColors('custom') ) echo 'checked'; ?> /><?php _e('Specify custom colors', 'bookmarks'); ?></label>
			<div class="color-options" data-bookmarks-custom-colors-options>
				<div class="option-group">
					<h4><?php _e('Default Button State (Unbookmarkd)', 'bookmarks'); ?></h4>
					<?php
					$default_options = $this->settings_repo->colorOptions('default');
					foreach ( $default_options as $option => $label ){
						$out = '<div class="option" data-bookmarks-color-option="' . $option . '">';
						$out .= '<label>' . $label . '</label>';
						$out .= '<input type="text" data-bookmarks-color-picker="' . $option . '" name="simplebookmarks_display[button_colors][' . $option . ']"';
						$out .= ' value="';
						if ( $this->settings_repo->buttonColors($option) ) $out .= $this->settings_repo->buttonColors($option);
						$out .= '" />';
						$out .= '</div><!-- .option -->';
						echo $out;
					}
					?>
				</div><!-- .option-group -->
				<div class="option-group">
					<h4><?php _e('Active Button State (Bookmarkd)', 'bookmarks'); ?></h4>
					<?php
					$default_options = $this->settings_repo->colorOptions('active');
					foreach ( $default_options as $option => $label ){
						$out = '<div class="option" data-bookmarks-color-option="' . $option . '">';
						$out .= '<label>' . $label . '</label>';
						$out .= '<input type="text" data-bookmarks-color-picker="' . $option . '" name="simplebookmarks_display[button_colors][' . $option . ']"';
						if ( $this->settings_repo->buttonColors($option) ) $out .= ' value="' . $this->settings_repo->buttonColors($option) . '"';
						$out .= '" />';
						$out .= '</div><!-- .option -->';
						echo $out;
					}
					?>
				</div><!-- .option-group -->
				<div class="option-group">
					<div class="option box-shadow">
						<label><input type="checkbox" name="simplebookmarks_display[button_colors][box_shadow]" value="true" <?php if ( $this->settings_repo->buttonColors('box_shadow') ) echo 'checked'; ?> data-bookmarks-button-shadow /><?php _e('Include button shadow', 'bookmarks'); ?>
					</div>
				</div>
			</div><!-- .color-options -->
		</div>
	</div><!-- .row -->
	<div class="row" data-bookmarks-custom-button-option>
		<div class="description">
			<h5><?php _e('Button Markup: Unbookmarkd', 'bookmarks'); ?></h5>
			<p><?php _e('The button inner html, in an unbookmarkd state.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('Text/HTML', 'bookmarks'); ?></label>
			<input type="text" name="simplebookmarks_display[buttontext]" value="<?php echo $this->settings_repo->buttonText(); ?>" />
		</div>
	</div><!-- .row -->
	<div class="row" data-bookmarks-custom-button-option>
		<div class="description">
			<h5><?php _e('Button Markup: Bookmarkd', 'bookmarks'); ?></h5>
			<p><?php _e('The button inner html, in a bookmarkd state.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('Text/HTML', 'bookmarks'); ?></label>
			<input type="text" name="simplebookmarks_display[buttontextbookmarkd]" value="<?php echo $this->settings_repo->buttonTextBookmarkd(); ?>" />
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('Include Total Bookmark Count', 'bookmarks'); ?></h5>
			<p><?php _e('Adds the total number of times the post has been bookmarkd to the button.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label><input type="checkbox" name="simplebookmarks_display[buttoncount]" value="true" <?php if ( $this->settings_repo->includeCountInButton() ) echo 'checked'; ?> data-bookmarks-include-count-checkbox /> <?php _e('Include count in button', 'bookmarks'); ?></label>
		</div>
	</div><!-- .row -->
</div><!-- .bookmarks-display-settings -->

<h3><?php _e('Bookmark Button Loading Indication', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-post-types">
	<div class="post-type-row">
		<div class="post-type-checkbox">
			<input type="checkbox" name="simplebookmarks_display[loadingindicator][include]" value="true" <?php if ( $this->settings_repo->includeLoadingIndicator() ) echo 'checked'; ?> data-bookmarks-posttype-checkbox />
		</div>
		<div class="post-type-name">
			<?php _e('Display loading indicator for buttons', 'bookmarks'); ?> <em>(<?php _e('Helpful for slow sites with cache enabled', 'bookmarks'); ?>)</em>
			<button class="button" data-bookmarks-toggle-post-type-settings <?php if ( !$display ) echo 'style="display:none;"';?>><?php _e('Settings', 'bookmarks'); ?></button>
		</div>
		<div class="post-type-settings">
			<div class="row">
				<div class="description">
					<h5><?php _e('Loading Text', 'bookmarks') ?></h5>
					<p><?php _e('Replaces the unbookmarkd/bookmarkd button text during the loading state.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<label class="block"><?php _e('Loading Text/HTML', 'bookmarks'); ?></label>
					<input type="text" name="simplebookmarks_display[loadingindicator][text]" value="<?php echo $this->settings_repo->loadingText(); ?>" />
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Loading Spinner', 'bookmarks') ?></h5>
					<p><?php _e('Adds a spinner to the button during loading state. See plugin documentation for filters available for theme customization.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<label class="block"><input type="checkbox" name="simplebookmarks_display[loadingindicator][include_html]" value="true" <?php if ( $this->settings_repo->loadingIndicatorType('include_html') ) echo 'checked'; ?> data-bookmarks-spinner-type="html">					<?php _e('Use CSS/HTML Spinner', 'bookmarks'); ?>
					</label>
					<label><input type="checkbox" name="simplebookmarks_display[loadingindicator][include_image]" value="true" <?php if ( $this->settings_repo->loadingIndicatorType('include_image') ) echo 'checked'; ?> data-bookmarks-spinner-type="image">					<?php _e('Use Image/GIF Spinner', 'bookmarks'); ?>
					</label>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Page Load', 'bookmarks') ?></h5>
					<p><?php _e('Adds the loading state to bookmarks buttons during page load. Helpful on sites with page cache enabled.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<label><input type="checkbox" name="simplebookmarks_display[loadingindicator][include_preload]" value="true" <?php if ( $this->settings_repo->includeLoadingIndicatorPreload() ) echo 'checked'; ?>><?php _e('Add loading state on page load', 'bookmarks'); ?>
				</label>
				</div>
			</div><!-- .row -->
		</div><!-- .post-type-settings -->
	</div><!-- .post-type-row -->
</div><!-- .simple-bookmarks-post-types -->

<h3><?php _e('Listing Display', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-post-types">
	<div class="post-type-row">
		<div class="post-type-checkbox">
			<input type="checkbox" name="simplebookmarks_display[listing][customize]" value="true" <?php if ( $this->settings_repo->listCustomization() ) echo 'checked'; ?> data-bookmarks-posttype-checkbox />
		</div>
		<div class="post-type-name">
			<?php _e('Customize the bookmarks list HTML', 'bookmarks'); ?>
			<button class="button" data-bookmarks-toggle-post-type-settings <?php if ( !$this->settings_repo->listCustomization() ) echo 'style="display:none;"';?>><?php _e('Settings', 'bookmarks'); ?></button>
		</div>
		<div class="post-type-settings">
			<div class="row">
				<div class="description">
					<h5><?php _e('List Wrapper Element', 'bookmarks') ?></h5>
					<p><?php _e('The list wrapper html element. Defaults to an html ul list.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<label class="block"><?php _e('List Wrapper Type', 'bookmarks'); ?></label>
					<select name="simplebookmarks_display[listing][wrapper_type]">
						<option value="ul" <?php if ( $this->settings_repo->listCustomization('wrapper_type') == 'ul' ) echo 'selected';?>><?php _e('Unordered List (ul)', 'bookmarks'); ?></option>
						<option value="ol" <?php if ( $this->settings_repo->listCustomization('wrapper_type') == 'ol' ) echo 'selected';?>><?php _e('Ordered List (ol)', 'bookmarks'); ?></option>
						<option value="div" <?php if ( $this->settings_repo->listCustomization('wrapper_type') == 'div' ) echo 'selected';?>><?php _e('Div', 'bookmarks'); ?></option>
					</select>
					<p>
						<label class="block"><?php _e('Wrapper CSS Classes', 'bookmarks'); ?></label>
						<input type="text" name="simplebookmarks_display[listing][wrapper_css]" value="<?php echo $this->settings_repo->listCustomization('wrapper_css'); ?>" />
					</p>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Single List Element', 'bookmarks') ?></h5>
					<p><?php _e('The individual listing html element. Defaults to an html li item.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<label class="block"><?php _e('List Element Type', 'bookmarks'); ?></label>
					<select name="simplebookmarks_display[listing][listing_type]">
						<option value="li" <?php if ( $this->settings_repo->listCustomization('listing_type') == 'ul' ) echo 'selected';?>><?php _e('List Item (li)', 'bookmarks'); ?></option>
						<option value="p" <?php if ( $this->settings_repo->listCustomization('listing_type') == 'ol' ) echo 'selected';?>><?php _e('Paragraph (p)', 'bookmarks'); ?></option>
						<option value="div" <?php if ( $this->settings_repo->listCustomization('listing_type') == 'div' ) echo 'selected';?>><?php _e('Div', 'bookmarks'); ?></option>
					</select>
					<p>
						<label class="block"><?php _e('Listing CSS Classes', 'bookmarks'); ?></label>
						<input type="text" name="simplebookmarks_display[listing][listing_css]" value="<?php echo $this->settings_repo->listCustomization('listing_css'); ?>" />
					</p>
				</div>
			</div><!-- .row -->
			<div class="row">
				<div class="description">
					<h5><?php _e('Single Listing Content Markup', 'bookmarks') ?></h5>
					<p><?php _e('Optionally customize the single listing markup.', 'bookmarks'); ?></p>
				</div>
				<div class="field">
					<p>
						<label class="block"><input type="checkbox" name="simplebookmarks_display[listing][customize_markup]" value="true" data-bookmarks-listing-customizer-checkbox <?php if ( $this->settings_repo->listCustomization('customize_markup') ) echo 'checked'; ?>/><?php _e('Customize Content', 'bookmarks'); ?></label>
					</p>
					<div class="simple-bookmarks-listing-customizer" data-bookmarks-listing-customizer>
						<div class="bookmarks-alert">
							<p><strong><?php _e('Important:', 'bookmarks'); ?></strong> <?php _e('By customizing the listing content, some shortcode options will not apply. These options include: include_links, include_buttons, include_thumbnails, and thumbnail_size', 'bookmarks'); ?></p>
						</div>
						<div class="variable-tools">
							<h4><?php _e('Add Dynamic Fields', 'bookmarks'); ?></h4>
							<p><?php _e('To add a custom meta field, use the format <code>[custom_field:custom_field_name]</code>', 'bookmarks'); ?></p>
							<hr>
							<div class="variables">
								<label><?php _e('Post Fields', 'bookmarks'); ?></label>
								<select>
									<option value="[post_title]"><?php _e('Post Title', 'bookmarks'); ?></option>
									<option value="[post_excerpt]"><?php _e('Excerpt', 'bookmarks'); ?></option>
									<option value="[post_permalink]"><?php _e('Permalink (raw link)', 'bookmarks'); ?></option>
									<option value="[permalink][/permalink]"><?php _e('Permalink (as link)', 'bookmarks'); ?></option>
									<?php
									$thumbnail_sizes = get_intermediate_image_sizes();
									foreach ( $thumbnail_sizes as $size ){
										echo '<option value="[post_thumbnail_' . $size . ']">' . __('Thumbnail: ', 'bookmarks') . $size . '</option>';
									}
									?>
								</select>
								<button data-bookmarks-listing-customizer-variable-button class="button"><?php _e('Add', 'bookmarks'); ?></button>
							</div><!-- .variables -->
							<div class="variables right">
								<label><?php _e('Bookmarks Fields', 'bookmarks'); ?></label>
								<select>
									<option value="[bookmarks_button]"><?php _e('Bookmark Button', 'bookmarks'); ?></option>
									<option value="[bookmarks_count]"><?php _e('Bookmark Count', 'bookmarks'); ?></option>
								</select>
								<button data-bookmarks-listing-customizer-variable-button class="button"><?php _e('Add', 'bookmarks'); ?></button>
							</div><!-- .variables -->
						</div><!-- .variable-tools -->
						<?php
							wp_editor(
								$this->settings_repo->listCustomization('custom_markup_html'), 
								'simplebookmarks_display_listing_custom_markup', 
								array(
									'textarea_name' => 'simplebookmarks_display[listing][custom_markup_html]',
									'tabindex' => 1,
									'wpautop' => true
								)
							);
						?>
					</div>
				</div><!-- .field -->
			</div><!-- .row -->
		</div><!-- .post-type-settings -->
	</div><!-- .post-type-row -->
</div><!-- .simple-bookmarks-post-types -->


<h3><?php _e('Additional Display Settings', 'bookmarks'); ?></h3>
<div class="simple-bookmarks-display-settings">
	<div class="row">
		<div class="description">
			<h5><?php _e('Clear Bookmarks Button Text', 'bookmarks'); ?></h5>
			<p><?php _e('The text that appears in the "Clear Bookmarks" button by default. Note, the text may be overridden in the shortcode or function call.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('Clear Bookmarks Button Text/HTML', 'bookmarks'); ?></label>
			<input type="text" name="simplebookmarks_display[clearbookmarks]" value="<?php echo $this->settings_repo->clearBookmarksText(); ?>" />
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="description">
			<h5><?php _e('No Bookmarks Text', 'bookmarks'); ?></h5>
			<p><?php _e('Appears in user bookmark lists when they have not bookmarkd any posts.', 'bookmarks'); ?></p>
		</div>
		<div class="field">
			<label class="block"><?php _e('No Bookmarks Text/HTML', 'bookmarks'); ?></label>
			<input type="text" name="simplebookmarks_display[nobookmarks]" value="<?php echo $this->settings_repo->noBookmarksText(); ?>" />
		</div>
	</div><!-- .row -->
</div><!-- .bookmarks-display-settings -->