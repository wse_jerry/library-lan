<?php 
namespace Bookmarks\Events;

use Bookmarks\Listeners\NonceHandler;
use Bookmarks\Listeners\BookmarkButton;
use Bookmarks\Listeners\BookmarksArray;
use Bookmarks\Listeners\ClearBookmarks;
use Bookmarks\Listeners\BookmarkCount;
use Bookmarks\Listeners\BookmarkList;
use Bookmarks\Listeners\CookieConsent;

class RegisterPublicEvents 
{
	public function __construct()
	{
		// Generate a Nonce
		add_action( 'wp_ajax_nopriv_bookmarks_nonce', array($this, 'nonce' ));
		add_action( 'wp_ajax_bookmarks_nonce', array($this, 'nonce' ));

		// Front End Bookmark Button
		add_action( 'wp_ajax_nopriv_bookmarks_bookmark', array($this, 'bookmarkButton' ));
		add_action( 'wp_ajax_bookmarks_bookmark', array($this, 'bookmarkButton' ));

		// User's Bookmarkd Posts (array of IDs)
		add_action( 'wp_ajax_nopriv_bookmarks_array', array($this, 'bookmarksArray' ));
		add_action( 'wp_ajax_bookmarks_array', array($this, 'bookmarksArray' ));

		// Clear Bookmarks
		add_action( 'wp_ajax_nopriv_bookmarks_clear', array($this, 'clearBookmarks' ));
		add_action( 'wp_ajax_bookmarks_clear', array($this, 'clearBookmarks' ));

		// Total Bookmark Count
		add_action( 'wp_ajax_nopriv_bookmarks_totalcount', array($this, 'bookmarkCount' ));
		add_action( 'wp_ajax_bookmarks_totalcount', array($this, 'bookmarkCount' ));

		// Single Bookmark List
		add_action( 'wp_ajax_nopriv_bookmarks_list', array($this, 'bookmarkList' ));
		add_action( 'wp_ajax_bookmarks_list', array($this, 'bookmarkList' ));

		// Accept/Deny Cookies
		add_action( 'wp_ajax_nopriv_bookmarks_cookie_consent', array($this, 'cookiesConsented' ));
		add_action( 'wp_ajax_bookmarks_cookie_consent', array($this, 'cookiesConsented' ));

	}

	/**
	* Bookmark Button
	*/
	public function bookmarkButton()
	{
		new BookmarkButton;
	}

	/**
	* Generate a Nonce
	*/
	public function nonce()
	{
		new NonceHandler;
	}

	/**
	* Get an array of current user's bookmarks
	*/
	public function bookmarksArray()
	{
		new BookmarksArray;
	}

	/**
	* Clear all Bookmarks
	*/
	public function clearBookmarks()
	{
		new ClearBookmarks;
	}

	/**
	* Bookmark Count for a single post
	*/
	public function bookmarkCount()
	{
		new BookmarkCount;
	}

	/**
	* Single Bookmark List for a Specific User
	*/
	public function bookmarkList()
	{
		new BookmarkList;
	}

	/**
	* Cookies were either accepted or denied
	*/
	public function cookiesConsented()
	{
		new CookieConsent;
	}
}