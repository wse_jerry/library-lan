<?php 
namespace Bookmarks\Activation;

/**
* Plugin Activation
*/
class Activate 
{
	public function __construct()
	{
		$this->setOptions();
	}

	/**
	* Default Plugin Options
	*/
	private function setOptions()
	{
		if ( !get_option('simplebookmarks_dependencies') 
			&& get_option('simplebookmarks_dependencies') !== "" ){
			update_option('simplebookmarks_dependencies', array(
				'css' => 'true',
				'js' => 'true'
			));
		}
		if ( !get_option('simplebookmarks_users')
			&& get_option('simplebookmarks_users') !== "" ){
			update_option('simplebookmarks_users', array(
				'anonymous' => array(
					'display' => 'true',
					'save' => 'true'
				),
				'saveas' => 'cookie'
			));
		}
		if ( !get_option('simplebookmarks_display')
			&& get_option('simplebookmarks_display') !== "" ){
			update_option('simplebookmarks_display', array(
				'buttontext' => __('Bookmark <i class="sf-icon-star-empty"></i>', 'bookmarks'),
				'buttontextbookmarkd' => __('Bookmarkd <i class="sf-icon-star-full"></i>', 'bookmarks'),
				'posttypes' => array(
					'post' => array(
						'display' => true,
						'after_content' => true,
						'postmeta' => true
					)
				)
			));
		}
		if ( !get_option('simplebookmarks_cache_enabled')
			&& get_option('simplebookmarks_cache_enabled') !== "" ){
			update_option('simplebookmarks_cache_enabled', 'true');
		}
	}
}