/**
 * Utility Methods
 */
var Bookmarks = Bookmarks || {};

Bookmarks.Utilities = function() {
        var plugin = this;
        var $ = jQuery;

        /*
         * Check if an item is bookmarkd
         * @param int post_id
         * @param object bookmarks for a specific site
         */
        plugin.isBookmark = function(post_id, site_bookmarks) {
            var status = false;
            $.each(site_bookmarks, function(i, v) {
                if (v.post_id === parseInt(post_id)) status = true;
                if (parseInt(v.post_id) === post_id) status = true;
            });
            return status;
        }

        /**
         * Get the length of an
         */
        plugin.objectLength = function(object) {
            var size = 0,
                key;
            for (key in object) {
                if (object.hasOwnProperty(key)) size++;
            }
            return size;
        }

        /*
         * Get Site index from All Bookmarks
         */
        plugin.siteIndex = function(siteid) {
            for (var i = 0; i < Bookmarks.userBookmarks.length; i++) {
                if (Bookmarks.userBookmarks[i].site_id !== parseInt(siteid)) continue;
                return i;
            }
        }

        /*
         * Get a specific thumbnail size
         */
        plugin.getThumbnail = function(bookmark, size) {
            var thumbnails = bookmark.thumbnails;
            if (typeof thumbnails === 'undefined' || thumbnails.length == 0) return false;
            var thumbnail_url = thumbnails[size];
            if (typeof thumbnail_url === 'undefined') return false;
            if (!thumbnail_url) return false;
            return thumbnail_url;
        }
    }
    /**
     * Formatting functionality
     */
var Bookmarks = Bookmarks || {};

Bookmarks.Formatter = function() {
        var plugin = this;
        var $ = jQuery;

        /*
         *  Add Bookmark Count to a button
         */
        plugin.addBookmarkCount = function(html, count) {
            if (!Bookmarks.jsData.button_options.include_count) return html;
            if (count <= 0) count = 0;
            html += ' <span class="simplebookmark-button-count">' + count + '</span>';
            return html;
        }

        /**
         * Decrement all counts by one
         */
        plugin.decrementAllCounts = function() {
            var buttons = $('.simplebookmark-button.active.has-count');
            for (var i = 0; i < buttons.length; i++) {
                var button = $(buttons)[i];
                var count_display = $(button).find('.simplebookmark-button-count');
                var new_count = $(count_display).text() - 1;
                $(button).attr('data-bookmarkcount', new_count);
            }
        }
    }
    /**
     * Builds the bookmark button html
     */
var Bookmarks = Bookmarks || {};

Bookmarks.ButtonOptionsFormatter = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.options = Bookmarks.jsData.button_options;
        plugin.formatter = new Bookmarks.Formatter;

        /**
         * Format the button according to plugin options
         */
        plugin.format = function(button, isBookmark) {
            if (plugin.options.custom_colors) plugin.colors(button, isBookmark);
            plugin.html(button, isBookmark);
        }

        /**
         * Set the HTML content for the button
         */
        plugin.html = function(button, isBookmark) {
            var count = $(button).attr('data-bookmarkcount');
            var options = plugin.options.button_type;
            var html = '';
            if (plugin.options.button_type === 'custom') {
                if (isBookmark) $(button).html(plugin.formatter.addBookmarkCount(Bookmarks.jsData.bookmarkd, count));
                if (!isBookmark) $(button).html(plugin.formatter.addBookmarkCount(Bookmarks.jsData.bookmark, count));
                plugin.applyIconColor(button, isBookmark);
                plugin.applyCountColor(button, isBookmark);
                return;
            }
            if (isBookmark) {
                html += '<i class="' + options.icon_class + '"></i> ';
                html += options.state_active;
                $(button).html(plugin.formatter.addBookmarkCount(html, count));
                return;
            }
            html += '<i class="' + options.icon_class + '"></i> ';
            html += options.state_default;
            $(button).html(plugin.formatter.addBookmarkCount(html, count));
            plugin.applyIconColor(button, isBookmark);
            plugin.applyCountColor(button, isBookmark);
        }

        /**
         * Apply custom colors to the button if the option is selected
         */
        plugin.colors = function(button, isBookmark) {
            if (!plugin.options.custom_colors) return;
            if (isBookmark) {
                var options = plugin.options.active;
                if (options.background_active) $(button).css('background-color', options.background_active);
                if (options.border_active) $(button).css('border-color', options.border_active);
                if (options.text_active) $(button).css('color', options.text_active);
                return;
            }
            var options = plugin.options.default;
            if (options.background_default) $(button).css('background-color', options.background_default);
            if (options.border_default) $(button).css('border-color', options.border_default);
            if (options.text_default) $(button).css('color', options.text_default);
            plugin.boxShadow(button);
        }

        /**
         * Remove the box shadow from the button if the option is selected
         */
        plugin.boxShadow = function(button) {
            if (plugin.options.box_shadow) return;
            $(button).css('box-shadow', 'none');
            $(button).css('-webkit-box-shadow', 'none');
            $(button).css('-moz-box-shadow', 'none');
        }

        /**
         * Apply custom colors to the icon if the option is selected
         */
        plugin.applyIconColor = function(button, isBookmark) {
            if (!plugin.options.custom_colors) return;
            if (isBookmark && plugin.options.active.icon_active) {
                $(button).find('i').css('color', plugin.options.active.icon_active);
            }
            if (!isBookmark && plugin.options.default.icon_default) {
                $(button).find('i').css('color', plugin.options.default.icon_default);
            }
        }

        /**
         * Apply custom colors to the bookmark count if the option is selected
         */
        plugin.applyCountColor = function(button, isBookmark) {
            if (!plugin.options.custom_colors) return;
            if (isBookmark && plugin.options.active.count_active) {
                $(button).find(Bookmarks.selectors.count).css('color', plugin.options.active.count_active);
                return;
            }
            if (!isBookmark && plugin.options.default.count_default) {
                $(button).find(Bookmarks.selectors.count).css('color', plugin.options.default.count_default);
            }
        }
    }
    /**
     * Generates a new nonce on page load via AJAX
     * Solves problem of cached pages and expired nonces
     *
     * Events:
     * bookmarks-nonce-generated: The nonce has been generated
     */
var Bookmarks = Bookmarks || {};

Bookmarks.NonceGenerator = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.bindEvents = function() {
            $(document).ready(function() {
                if (Bookmarks.jsData.dev_mode) {
                    console.log('Bookmarks Localized Data');
                    console.log(Bookmarks.jsData);
                }
                plugin.getNonce();
            });
        }

        /**
         * Make the AJAX call to get the nonce
         */
        plugin.getNonce = function() {
            if (Bookmarks.jsData.cache_enabled === '') {
                Bookmarks.jsData.nonce = bookmarks_data.nonce;
                return;
            }
            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'POST',
                datatype: 'json',
                data: {
                    action: Bookmarks.formActions.nonce,
                    logged_in: Bookmarks.jsData.logged_in,
                    user_id: Bookmarks.jsData.user_id
                },
                success: function(data) {
                    Bookmarks.jsData.nonce = data.nonce;
                    if (Bookmarks.jsData.dev_mode) {
                        console.log('Nonce successfully generated: ' + data.nonce);
                    }
                    $(document).trigger('bookmarks-nonce-generated', [data.nonce]);
                }
            });
        }

        return plugin.bindEvents();
    }
    /**
     * Gets the user bookmarks
     */
var Bookmarks = Bookmarks || {};

Bookmarks.UserBookmarks = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.initialLoad = false;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-nonce-generated', function() {
                plugin.initialLoad = true;
                plugin.getBookmarks();
            });
        }

        /**
         * Get the user bookmarks
         */
        plugin.getBookmarks = function() {
            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'POST',
                datatype: 'json',
                data: {
                    action: Bookmarks.formActions.bookmarksarray,
                    logged_in: Bookmarks.jsData.logged_in,
                    user_id: Bookmarks.jsData.user_id
                },
                success: function(data) {
                    if (Bookmarks.jsData.dev_mode) {
                        console.log('The current user bookmarks were successfully loaded.');
                        console.log(data);
                    }
                    Bookmarks.userBookmarks = data.bookmarks;
                    $(document).trigger('bookmarks-user-bookmarks-loaded', [plugin.initialLoad]);
                    $(document).trigger('bookmarks-update-all-buttons');

                    // Deprecated Callback
                    if (plugin.initialLoad) bookmarks_after_initial_load(Bookmarks.userBookmarks);
                },
                error: function(data) {
                    if (!Bookmarks.jsData.dev_mode) return;
                    console.log('The was an error loading the user bookmarks.');
                    console.log(data);
                }
            });
        }

        return plugin.bindEvents();
    }
    /**
     * Clears all bookmarks for the user
     *
     * Events:
     * bookmarks-cleared: The user's bookmarks have been cleared. Params: clear button
     */
var Bookmarks = Bookmarks || {};

Bookmarks.Clear = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.activeButton; // The active "clear bookmarks" button
        plugin.utilities = new Bookmarks.Utilities;
        plugin.formatter = new Bookmarks.Formatter;

        plugin.bindEvents = function() {
            $(document).on('click', Bookmarks.selectors.clear_button, function(e) {
                e.preventDefault();
                plugin.activeButton = $(this);
                plugin.clearBookmarks();
            });
            $(document).on('bookmarks-updated-single', function() {
                plugin.updateClearButtons();
            });
            $(document).on('bookmarks-user-bookmarks-loaded', function() {
                plugin.updateClearButtons();
            });
        }

        /*
         * Submit an AJAX request to clear all of the user's bookmarks
         */
        plugin.clearBookmarks = function() {
            plugin.loading(true);
            var site_id = $(plugin.activeButton).attr('data-siteid');
            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'post',
                datatype: 'json',
                data: {
                    action: Bookmarks.formActions.clearall,
                    nonce: Bookmarks.jsData.nonce,
                    siteid: site_id,
                    logged_in: Bookmarks.jsData.logged_in,
                    user_id: Bookmarks.jsData.user_id
                },
                success: function(data) {
                    if (Bookmarks.jsData.dev_mode) {
                        console.log('Bookmarks list successfully cleared.');
                        console.log(data);
                    }
                    Bookmarks.userBookmarks = data.bookmarks;
                    plugin.formatter.decrementAllCounts();
                    plugin.loading(false);
                    plugin.clearSiteBookmarks(site_id);
                    $(document).trigger('bookmarks-cleared', [plugin.activeButton, data.old_bookmarks]);
                    $(document).trigger('bookmarks-update-all-buttons');
                },
                error: function(data) {
                    if (!Bookmarks.jsData.dev_mode) return;
                    console.log('There was an error clearing the bookmarks list.');
                    console.log(data);
                }
            });
        }

        /**
         * Toggle the button loading state
         */
        plugin.loading = function(loading) {
            if (loading) {
                $(plugin.activeButton).addClass(Bookmarks.cssClasses.loading);
                $(plugin.activeButton).attr('disabled', 'disabled');
                return;
            }
            $(plugin.activeButton).removeClass(Bookmarks.cssClasses.loading);
        }

        /*
         * Update disabled status for clear buttons
         */
        plugin.updateClearButtons = function() {
            var button;
            var siteid;
            for (var i = 0; i < $(Bookmarks.selectors.clear_button).length; i++) {
                button = $(Bookmarks.selectors.clear_button)[i];
                siteid = $(button).attr('data-siteid');
                for (var c = 0; c < Bookmarks.userBookmarks.length; c++) {
                    if (Bookmarks.userBookmarks[c].site_id !== parseInt(siteid)) continue;
                    if (plugin.utilities.objectLength(Bookmarks.userBookmarks[c].posts) > 0) {
                        $(button).attr('disabled', false);
                        continue;
                    }
                    $(button).attr('disabled', 'disabled');
                }
            }
        }

        /**
         * Clear out bookmarks for this site id (fix for cookie-enabled sites)
         */
        plugin.clearSiteBookmarks = function(site_id) {
            $.each(Bookmarks.userBookmarks, function(i, v) {
                if (this.site_id !== parseInt(site_id)) return;
                Bookmarks.userBookmarks[i].posts = {};
            });
        }

        return plugin.bindEvents();
    }
    /**
     * Bookmarks List functionality
     */
var Bookmarks = Bookmarks || {};

Bookmarks.Lists = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.utilities = new Bookmarks.Utilities;
        plugin.buttonFormatter = new Bookmarks.ButtonOptionsFormatter;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-update-all-lists', function() {
                plugin.updateAllLists();
            });
            $(document).on('bookmarks-updated-single', function() {
                plugin.updateAllLists();
            });
            $(document).on('bookmarks-cleared', function() {
                plugin.updateAllLists();
            });
            $(document).on('bookmarks-user-bookmarks-loaded', function() {
                plugin.updateAllLists();
            });
        }

        /**
         * Loop through all the bookmarks lists
         */
        plugin.updateAllLists = function() {
            if (typeof Bookmarks.userBookmarks === 'undefined') return;
            for (var i = 0; i < Bookmarks.userBookmarks.length; i++) {
                var lists = $(Bookmarks.selectors.list + '[data-siteid="' + Bookmarks.userBookmarks[i].site_id + '"]');
                for (var c = 0; c < $(lists).length; c++) {
                    var list = $(lists)[c];
                    plugin.updateSingleList(list)
                }
            }
        }

        /**
         * Update a specific user list
         */
        plugin.updateSingleList = function(list) {
            var user_id = $(list).attr('data-userid');
            var site_id = $(list).attr('data-siteid');
            var include_links = $(list).attr('data-includelinks');
            var include_buttons = $(list).attr('data-includebuttons');
            var include_thumbnails = $(list).attr('data-includethumbnails');
            var thumbnail_size = $(list).attr('data-thumbnailsize');
            var include_excerpt = $(list).attr('data-includeexcerpts');
            var post_types = $(list).attr('data-posttypes');
            var no_bookmarks = $(list).attr('data-nobookmarkstext');

            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: Bookmarks.formActions.bookmarklist,
                    nonce: Bookmarks.jsData.nonce,
                    userid: user_id,
                    siteid: site_id,
                    include_links: include_links,
                    include_buttons: include_buttons,
                    include_thumbnails: include_thumbnails,
                    thumbnail_size: thumbnail_size,
                    include_excerpt: include_excerpt,
                    no_bookmarks: no_bookmarks,
                    post_types: post_types,
                    user_id_current: Bookmarks.jsData.user_id,
                    logged_in: Bookmarks.jsData.logged_in
                },
                success: function(data) {
                    if (Bookmarks.jsData.dev_mode) {
                        console.log('Bookmarks list successfully retrieved.');
                        console.log($(list));
                        console.log(data);
                    }
                    var newlist = $(data.list);
                    $(list).replaceWith(newlist);
                    plugin.removeButtonLoading(newlist);
                    $(document).trigger('bookmarks-list-updated', [newlist]);
                    changeStatus();
                },
                error: function(data) {
                    if (!Bookmarks.jsData.dev_mode) return;
                    console.log('There was an error updating the list.');
                    console.log(list);
                    console.log(data);
                }
            });
        }

        /**
         * Remove loading state from buttons in the list
         */
        plugin.removeButtonLoading = function(list) {
            var buttons = $(list).find(Bookmarks.selectors.button);
            $.each(buttons, function() {
                plugin.buttonFormatter.format($(this), false);
                $(this).removeClass(Bookmarks.cssClasses.active);
                $(this).removeClass(Bookmarks.cssClasses.loading);
            });
        }

        /**
         * Remove unbookmarkd items from the list
         */
        plugin.removeInvalidListItems = function(list, bookmarks) {
            var listitems = $(list).find('li[data-postid]');
            $.each(listitems, function(i, v) {
                var postid = $(this).attr('data-postid');
                if (!plugin.utilities.isBookmark(postid, bookmarks)) $(this).remove();
            });
        }

        return plugin.bindEvents();
    }
    /**
     * Bookmark Buttons
     * Bookmarks/Unbookmarks a specific post
     *
     * Events:
     * bookmarks-updated-single: A user's bookmark has been updated. Params: bookmarks, post_id, site_id, status
     */
var Bookmarks = Bookmarks || {};

Bookmarks.Button = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.activeButton; // The clicked button
        plugin.allButtons; // All bookmark buttons for the current post
        plugin.authenticated = true;

        plugin.formatter = new Bookmarks.Formatter;
        plugin.data = {};

        plugin.bindEvents = function() {
            $(document).on('click', Bookmarks.selectors.button, function(e) {
                e.preventDefault();
                plugin.activeButton = $(this);
                plugin.setAllButtons();
                plugin.submitBookmark();
            });
        }

        /**
         * Set all buttons
         */
        plugin.setAllButtons = function() {
            var post_id = $(plugin.activeButton).attr('data-postid');
            plugin.allButtons = $('button[data-postid="' + post_id + '"]');
        }

        /**
         * Set the Post Data
         */
        plugin.setData = function() {
            plugin.data.post_id = $(plugin.activeButton).attr('data-postid');
            plugin.data.site_id = $(plugin.activeButton).attr('data-siteid');
            plugin.data.status = ($(plugin.activeButton).hasClass('active')) ? 'inactive' : 'active';
            var consentProvided = $(plugin.activeButton).attr('data-user-consent-accepted');
            plugin.data.user_consent_accepted = (typeof consentProvided !== 'undefined' && consentProvided !== '') ? true : false;
        }

        /**
         * Submit the button
         */
        plugin.submitBookmark = function() {
            plugin.loading(true);
            plugin.setData();
            var formData = {
                action: Bookmarks.formActions.bookmark,
                nonce: Bookmarks.jsData.nonce,
                postid: plugin.data.post_id,
                siteid: plugin.data.site_id,
                status: plugin.data.status,
                logged_in: Bookmarks.jsData.logged_in,
                user_id: Bookmarks.jsData.user_id,
                user_consent_accepted: plugin.data.user_consent_accepted
            }
            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: formData,
                success: function(data) {
                    if (Bookmarks.jsData.dev_mode) {
                        console.log('The bookmark was successfully saved.');
                        console.log(data);
                    }
                    if (data.status === 'unauthenticated') {
                        Bookmarks.authenticated = false;
                        plugin.loading(false);
                        plugin.data.status = 'inactive';
                        $(document).trigger('bookmarks-update-all-buttons');
                        $(document).trigger('bookmarks-require-authentication', [plugin.data]);
                        return;
                    }
                    if (data.status === 'consent_required') {
                        plugin.loading(false);
                        $(document).trigger('bookmarks-require-consent', [data, plugin.data, plugin.activeButton]);
                        return;
                    }
                    Bookmarks.userBookmarks = data.bookmarks;
                    plugin.loading(false);
                    plugin.resetButtons();
                    $(document).trigger('bookmarks-updated-single', [data.bookmarks, plugin.data.post_id, plugin.data.site_id, plugin.data.status]);
                    $(document).trigger('bookmarks-update-all-buttons');
                    //$(".jebookmark").trigger("je-update-bookmarks");
                    //console.log('.jebookmarktttttt');
                    if ($('.simplebookmark-button').hasClass('active')) {
                        $('.jebookmark').addClass('active');
                    } else {
                        $('.jebookmark').removeClass('active');
                    }
                    // Deprecated callback
                    bookmarks_after_button_submit(data.bookmarks, plugin.data.post_id, plugin.data.site_id, plugin.data.status);
                },
                error: function(data) {
                    if (!Bookmarks.jsData.dev_mode) return;
                    console.log('There was an error saving the bookmark.');
                    console.log(data);
                }
            });
        }

        /*
         * Set the output html
         */
        plugin.resetButtons = function() {
            var bookmark_count = parseInt($(plugin.activeButton).attr('data-bookmarkcount'));

            $.each(plugin.allButtons, function() {
                if (plugin.data.status === 'inactive') {
                    if (bookmark_count <= 0) bookmark_count = 1;
                    $(this).removeClass(Bookmarks.cssClasses.active);
                    $(this).attr('data-bookmarkcount', bookmark_count - 1);
                    $(this).find(Bookmarks.selectors.count).text(bookmark_count - 1);
                    return;
                }
                $(this).addClass(Bookmarks.cssClasses.active);
                $(this).attr('data-bookmarkcount', bookmark_count + 1);
                $(this).find(Bookmarks.selectors.count).text(bookmark_count + 1);
            });
        }

        /*
         * Toggle loading on the button
         */
        plugin.loading = function(loading) {
            if (loading) {
                $.each(plugin.allButtons, function() {
                    $(this).attr('disabled', 'disabled');
                    $(this).addClass(Bookmarks.cssClasses.loading);
                    $(this).html(plugin.addLoadingIndication());
                });
                return;
            }
            $.each(plugin.allButtons, function() {
                $(this).attr('disabled', false);
                $(this).removeClass(Bookmarks.cssClasses.loading);
            });
        }

        /*
         * Add loading indication to button
         */
        plugin.addLoadingIndication = function(html) {
            if (Bookmarks.jsData.indicate_loading !== '1') return html;
            if (plugin.data.status === 'active') return Bookmarks.jsData.loading_text + Bookmarks.jsData.loading_image_active;
            return Bookmarks.jsData.loading_text + Bookmarks.jsData.loading_image;
        }

        return plugin.bindEvents();
    }
    /**
     * Updates Bookmark Buttons as Needed
     */
var Bookmarks = Bookmarks || {};

Bookmarks.ButtonUpdater = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.utilities = new Bookmarks.Utilities;
        plugin.formatter = new Bookmarks.Formatter;
        plugin.buttonFormatter = new Bookmarks.ButtonOptionsFormatter;

        plugin.activeButton;
        plugin.data = {};

        plugin.bindEvents = function() {
            $(document).on('bookmarks-update-all-buttons', function() {
                plugin.updateAllButtons();
            });
            $(document).on('bookmarks-list-updated', function(event, list) {
                plugin.updateAllButtons(list);
            });
        }

        /*
         * Update all bookmarks buttons to match the user bookmarks
         * @param list object (optionally updates button in list)
         */
        plugin.updateAllButtons = function(list) {
            if (typeof Bookmarks.userBookmarks === 'undefined') return;
            var buttons = (typeof list === undefined && list !== '') ?
                $(list).find(Bookmarks.selectors.button) :
                $(Bookmarks.selectors.button);

            for (var i = 0; i < $(buttons).length; i++) {
                plugin.activeButton = $(buttons)[i];
                if (Bookmarks.authenticated) plugin.setButtonData();

                if (Bookmarks.authenticated && plugin.utilities.isBookmark(plugin.data.postid, plugin.data.site_bookmarks)) {
                    plugin.buttonFormatter.format($(plugin.activeButton), true);
                    $(plugin.activeButton).addClass(Bookmarks.cssClasses.active);
                    $(plugin.activeButton).removeClass(Bookmarks.cssClasses.loading);
                    $(plugin.activeButton).find(Bookmarks.selectors.count).text(plugin.data.bookmark_count);
                    continue;
                }

                plugin.buttonFormatter.format($(plugin.activeButton), false);
                $(plugin.activeButton).removeClass(Bookmarks.cssClasses.active);
                $(plugin.activeButton).removeClass(Bookmarks.cssClasses.loading);
                $(plugin.activeButton).find(Bookmarks.selectors.count).text(plugin.data.bookmark_count);
            }
        }


        /**
         * Set the button data
         */
        plugin.setButtonData = function() {
            plugin.data.postid = $(plugin.activeButton).attr('data-postid');
            plugin.data.siteid = $(plugin.activeButton).attr('data-siteid');
            plugin.data.bookmark_count = $(plugin.activeButton).attr('data-bookmarkcount');
            plugin.data.site_index = plugin.utilities.siteIndex(plugin.data.siteid);
            plugin.data.site_bookmarks = Bookmarks.userBookmarks[plugin.data.site_index].posts;
            if (plugin.data.bookmark_count <= 0) plugin.data.bookmark_count = 0;
        }

        return plugin.bindEvents();
    }
    /**
     * Total User Bookmarks Count Updates
     */
var Bookmarks = Bookmarks || {};

Bookmarks.TotalCount = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-updated-single', function() {
                plugin.updateTotal();
            });
            $(document).on('bookmarks-cleared', function() {
                plugin.updateTotal();
            });
            $(document).on('bookmarks-user-bookmarks-loaded', function() {
                plugin.updateTotal();
            });
        }

        /*
         * Update Total Number of Bookmarks
         */
        plugin.updateTotal = function() {
            // Loop through all the total bookmark elements
            for (var i = 0; i < $(Bookmarks.selectors.total_bookmarks).length; i++) {
                var item = $(Bookmarks.selectors.total_bookmarks)[i];
                var siteid = parseInt($(item).attr('data-siteid'));
                var posttypes = $(item).attr('data-posttypes');
                var posttypes_array = posttypes.split(','); // Multiple Post Type Support
                var count = 0;

                // Loop through all sites in bookmarks
                for (var c = 0; c < Bookmarks.userBookmarks.length; c++) {
                    var site_bookmarks = Bookmarks.userBookmarks[c];
                    if (site_bookmarks.site_id !== siteid) continue;
                    $.each(site_bookmarks.posts, function() {
                        if ($(item).attr('data-posttypes') === 'all') {
                            count++;
                            return;
                        }
                        if ($.inArray(this.post_type, posttypes_array) !== -1) count++;
                    });
                }
                $(item).text(count);
            }
        }

        return plugin.bindEvents();
    }
    /**
     * Updates the count of bookmarks for a post
     */
var Bookmarks = Bookmarks || {};

Bookmarks.PostBookmarkCount = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-updated-single', function(event, bookmarks, post_id, site_id, status) {
                if (status === 'active') return plugin.updateCounts();
                plugin.decrementSingle(post_id, site_id);
            });
            $(document).on('bookmarks-cleared', function(event, button, old_bookmarks) {
                plugin.updateCounts(old_bookmarks, true);
            });
        }

        /*
         * Update Total Number of Bookmarks
         */
        plugin.updateCounts = function(bookmarks, decrement) {
            if (typeof bookmarks === 'undefined' || bookmarks === '') bookmarks = Bookmarks.userBookmarks;
            if (typeof decrement === 'undefined' || decrement === '') decrement = false;

            // Loop through all the total bookmark elements
            for (var i = 0; i < $('[' + Bookmarks.selectors.post_bookmark_count + ']').length; i++) {

                var item = $('[' + Bookmarks.selectors.post_bookmark_count + ']')[i];
                var postid = parseInt($(item).attr(Bookmarks.selectors.post_bookmark_count));
                var siteid = $(item).attr('data-siteid');
                if (siteid === '') siteid = '1';

                // Loop through all sites in bookmarks
                for (var c = 0; c < bookmarks.length; c++) {
                    var site_bookmarks = bookmarks[c];
                    if (site_bookmarks.site_id !== parseInt(siteid)) continue;
                    $.each(site_bookmarks.posts, function() {

                        if (this.post_id === postid) {
                            if (decrement) {
                                var count = parseInt(this.total) - 1;
                                $(item).text(count);
                                return;
                            }
                            $(item).text(this.total);
                        }
                    });
                }
            }
        }

        /**
         * Decrement a single post total
         */
        plugin.decrementSingle = function(post_id, site_id) {
            for (var i = 0; i < $('[' + Bookmarks.selectors.post_bookmark_count + ']').length; i++) {
                var item = $('[' + Bookmarks.selectors.post_bookmark_count + ']')[i];
                var item_post_id = $(item).attr(Bookmarks.selectors.post_bookmark_count);
                var item_site_id = $(item).attr('data-siteid');
                if (item_site_id === '') item_site_id = '1';
                if (item_site_id !== site_id) continue;
                if (item_post_id !== post_id) continue;
                var count = parseInt($(item).text()) - 1;
                $(item).text(count);
            }
        }

        return plugin.bindEvents();
    }
    /**
     * Bookmarks Require Authentication
     */
var Bookmarks = Bookmarks || {};

Bookmarks.RequireAuthentication = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-require-authentication', function() {
                if (Bookmarks.jsData.dev_mode) {
                    console.log('Unauthenticated user was prevented from favoriting.');
                }
                if (Bookmarks.jsData.authentication_redirect) {
                    plugin.redirect();
                    return;
                }
                plugin.openModal();
            });
            $(document).on('click', '.simplebookmarks-modal-backdrop', function(e) {
                plugin.closeModal();
            });
            $(document).on('click', '[' + Bookmarks.selectors.close_modals + ']', function(e) {
                e.preventDefault();
                plugin.closeModal();
            });
        }

        /**
         * Redirect to a page
         */
        plugin.redirect = function() {
            window.location = Bookmarks.jsData.authentication_redirect_url;
        }

        /**
         * Open the Modal
         */
        plugin.openModal = function() {
            plugin.buildModal();
            setTimeout(function() {
                $('[' + Bookmarks.selectors.modals + ']').addClass('active');
            }, 10);
        }

        /**
         * Build the Modal
         */
        plugin.buildModal = function() {
            var modal = $('[' + Bookmarks.selectors.modals + ']');
            if (modal.length > 0) return;
            var html = '<div class="simplebookmarks-modal-backdrop" ' + Bookmarks.selectors.modals + '></div>';
            html += '<div class="simplebookmarks-modal-content" ' + Bookmarks.selectors.modals + '>';
            html += '<div class="simplebookmarks-modal-content-body">';
            html += Bookmarks.jsData.authentication_modal_content;
            html += '</div><!-- .simplebookmarks-modal-content-body -->';
            html += '</div><!-- .simplebookmarks-modal-content -->';
            $('body').prepend(html);
        }

        /**
         * Close the Moda
         */
        plugin.closeModal = function() {
            $('[' + Bookmarks.selectors.modals + ']').removeClass('active');
            $(document).trigger('bookmarks-modal-closed');
        }

        return plugin.bindEvents();
    }
    /**
     * Bookmarks Require Consent Modal Agreement
     */
var Bookmarks = Bookmarks || {};

Bookmarks.RequireConsent = function() {
        var plugin = this;
        var $ = jQuery;

        plugin.consentData;
        plugin.postData;
        plugin.activeButton;

        plugin.bindEvents = function() {
            $(document).on('bookmarks-require-consent', function(event, consent_data, post_data, active_button) {
                plugin.consentData = consent_data;
                plugin.postData = post_data;
                plugin.activeButton = active_button;
                plugin.openModal();
            });
            $(document).on('bookmarks-user-consent-approved', function(e, button) {
                if (typeof button !== 'undefined') {
                    $(plugin.activeButton).attr('data-user-consent-accepted', 'true');
                    $(plugin.activeButton).click();
                    plugin.closeModal();
                    return;
                }
                plugin.setConsent(true);
            });
            $(document).on('bookmarks-user-consent-denied', function() {
                plugin.setConsent(false);
            });
            $(document).on('click', '.simplebookmarks-modal-backdrop', function(e) {
                plugin.closeModal();
            });
            $(document).on('click', '[data-bookmarks-consent-deny]', function(e) {
                e.preventDefault();
                plugin.closeModal();
                $(document).trigger('bookmarks-user-consent-denied');
            });
            $(document).on('click', '[data-bookmarks-consent-accept]', function(e) {
                e.preventDefault();
                $(document).trigger('bookmarks-user-consent-approved', [$(this)]);
            });
        }

        /**
         * Open the Modal
         */
        plugin.openModal = function() {
            plugin.buildModal();
            setTimeout(function() {
                $('[' + Bookmarks.selectors.consentModal + ']').addClass('active');
            }, 10);
        }

        /**
         * Build the Modal
         */
        plugin.buildModal = function() {
            var modal = $('[' + Bookmarks.selectors.consentModal + ']');
            if (modal.length > 0) return;
            var html = '<div class="simplebookmarks-modal-backdrop" ' + Bookmarks.selectors.consentModal + '></div>';
            html += '<div class="simplebookmarks-modal-content" ' + Bookmarks.selectors.consentModal + '>';
            html += '<div class="simplebookmarks-modal-content-body no-padding">';
            html += '<div class="simplebookmarks-modal-content-interior">';
            html += plugin.consentData.message;
            html += '</div>';
            html += '<div class="simplebookmarks-modal-content-footer">'
            html += '<button class="simplebookmarks-button-consent-deny" data-bookmarks-consent-deny>' + plugin.consentData.deny_text + '</button>';
            html += '<button class="simplebookmarks-button-consent-accept" data-bookmarks-consent-accept>' + plugin.consentData.accept_text + '</button>';
            html += '</div><!-- .simplebookmarks-modal-footer -->';
            html += '</div><!-- .simplebookmarks-modal-content-body -->';
            html += '</div><!-- .simplebookmarks-modal-content -->';
            $('body').prepend(html);
        }

        /**
         * Close the Modal
         */
        plugin.closeModal = function() {
            $('[' + Bookmarks.selectors.consentModal + ']').removeClass('active');
        }

        /**
         * Submit a manual deny/consent
         */
        plugin.setConsent = function(consent) {
            $.ajax({
                url: Bookmarks.jsData.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: Bookmarks.formActions.cookieConsent,
                    consent: consent
                }
            });
        }

        return plugin.bindEvents();
    }
    /**
     * Primary Bookmarks Initialization
     * @package Bookmarks
     * @author Kyle Phillips - https://github.com/kylephillips/bookmarks
     *
     * Events:
     * bookmarks-nonce-generated: The nonce has been generated
     * bookmarks-updated-single: A user's bookmark has been updated. Params: bookmarks, post_id, site_id, status
     * bookmarks-cleared: The user's bookmarks have been cleared. Params: clear button
     * bookmarks-user-bookmarks-loaded: The user's bookmarks have been loaded. Params: intialLoad (bool)
     * bookmarks-require-authentication: An unauthenticated user has attempted to bookmark a post (The Require Login & Show Modal setting is checked)
     */

/**
 * Callback Functions for use in themes (deprecated in v2 in favor of events)
 */
function bookmarks_after_button_submit(bookmarks, post_id, site_id, status) {}

function bookmarks_after_initial_load(bookmarks) {}

jQuery(document).ready(function() {
    new Bookmarks.Factory;
});

var Bookmarks = Bookmarks || {};

/**
 * DOM Selectors Used by the Plugin
 */
Bookmarks.selectors = {
    button: '.simplebookmark-button', // Bookmark Buttons
    list: '.bookmarks-list', // Bookmark Lists
    clear_button: '.simplebookmarks-clear', // Clear Button
    total_bookmarks: '.simplebookmarks-user-count', // Total Bookmarks (from the_user_bookmarks_count)
    modals: 'data-bookmarks-modal', // Modals
    consentModal: 'data-bookmarks-consent-modal', // Consent Modal
    close_modals: 'data-bookmarks-modal-close', // Link/Button to close the modals
    count: '.simplebookmark-button-count', // The count inside the bookmarks button 
    post_bookmark_count: 'data-bookmarks-post-count-id' // The total number of times a post has been bookmarkd
}

/**
 * CSS Classes Used by the Plugin
 */
Bookmarks.cssClasses = {
    loading: 'loading', // Loading State
    active: 'active', // Active State
}

/**
 * Localized JS Data Used by the Plugin
 */
Bookmarks.jsData = {
    ajaxurl: bookmarks_data.ajaxurl, // The WP AJAX URL
    nonce: null, // The Dynamically-Generated Nonce
    bookmark: bookmarks_data.bookmark, // Active Button Text/HTML
    bookmarkd: bookmarks_data.bookmarkd, // Inactive Button Text
    include_count: bookmarks_data.includecount, // Whether to include the count in buttons
    indicate_loading: bookmarks_data.indicate_loading, // Whether to include loading indication in buttons
    loading_text: bookmarks_data.loading_text, // Loading indication text
    loading_image_active: bookmarks_data.loading_image_active, // Loading spinner url in active button
    loading_image: bookmarks_data.loading_image, // Loading spinner url in inactive button
    cache_enabled: bookmarks_data.cache_enabled, // Is cache enabled on the site
    authentication_modal_content: bookmarks_data.authentication_modal_content, // Content to display in authentication gate modal
    authentication_redirect: bookmarks_data.authentication_redirect, // Whether to redirect unauthenticated users to a page
    authentication_redirect_url: bookmarks_data.authentication_redirect_url, // URL to redirect to
    button_options: bookmarks_data.button_options, // Custom button options
    dev_mode: bookmarks_data.dev_mode, // Is Dev mode enabled
    logged_in: bookmarks_data.logged_in, // Is the user logged in
    user_id: bookmarks_data.user_id // The current user ID (0 if logged out)
}

/**
 * The user's bookmarks
 * @var object
 */
Bookmarks.userBookmarks = null;

/**
 * Is the user authenticated
 * @var object
 */
Bookmarks.authenticated = true;

/**
 * WP Form Actions Used by the Plugin
 */
Bookmarks.formActions = {
    nonce: 'bookmarks_nonce',
    bookmarksarray: 'bookmarks_array',
    bookmark: 'bookmarks_bookmark',
    clearall: 'bookmarks_clear',
    bookmarklist: 'bookmarks_list',
    cookieConsent: 'bookmarks_cookie_consent'
}

/**
 * Primary factory class
 */
Bookmarks.Factory = function() {
    var plugin = this;
    var $ = jQuery;

    plugin.build = function() {
        new Bookmarks.NonceGenerator;
        new Bookmarks.UserBookmarks;
        new Bookmarks.Lists;
        new Bookmarks.Clear;
        new Bookmarks.Button;
        new Bookmarks.ButtonUpdater;
        new Bookmarks.TotalCount;
        new Bookmarks.PostBookmarkCount;
        new Bookmarks.RequireAuthentication;
        new Bookmarks.RequireConsent;
    }

    return plugin.build();
}