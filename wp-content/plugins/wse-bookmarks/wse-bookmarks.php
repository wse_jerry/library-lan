<?php
   /**
 * Plugin Name: Wse Bookmark
 * Plugin URI:  https://wse.com.cn
 * Author:      Jerry Qi
 * Author URI:  https://jerryqi.cn
 * Description: Manage user's bookmarks.
 * Version:     1.0
 * Text Domain: bookmarks
 * Domain Path: /languages/
 */

/**
* Check Wordpress and PHP versions before instantiating plugin
*/
register_activation_hook( __FILE__, 'bookmarks_check_versions' );

define( 'BOOKMARKS_PLUGIN_FILE', __FILE__ );

function bookmarks_check_versions( $wp = '3.9', $php = '5.3.2' ) {
    global $wp_version;
    if ( version_compare( PHP_VERSION, $php, '<' ) ) $flag = 'PHP';
    elseif ( version_compare( $wp_version, $wp, '<' ) ) $flag = 'WordPress';
    else return;
    $version = 'PHP' == $flag ? $php : $wp;
    
    if (function_exists('deactivate_plugins')){
        deactivate_plugins( basename( __FILE__ ) );
    }
    
    wp_die('<p>The <strong>Bookmarks</strong> plugin requires'.$flag.'  version '.$version.' or greater.</p>','Plugin Activation Error',  array( 'response'=>200, 'back_link'=>TRUE ) );
}

if( !class_exists('Bootstrap') ) :
    bookmarks_check_versions();
    require_once(__DIR__ . '/vendor/autoload.php');
    require_once(__DIR__ . '/app/Bookmarks.php');
    require_once(__DIR__ . '/app/API/functions.php');
    Bookmarks::init();
endif;