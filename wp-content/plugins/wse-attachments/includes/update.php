<?php
// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Wse_Attachments_Metabox class.
 * 
 * @class Wse_Attachments_Metabox
 */
class Wse_Attachments_Update {

	public function __construct() {
		// actions
		add_action( 'init', array( $this, 'check_update' ) );
	}

	public function check_update() {
		if ( ! current_user_can( 'manage_options' ) )
			return;

		// get current database version
		$current_db_version = get_option( 'wse_attachments_version', '1.0.0' );

		// new version?
		if ( version_compare( $current_db_version, Wse_Attachments()->defaults['version'], '<' ) ) {
			// update plugin version
			update_option( 'wse_attachments_version', Wse_Attachments()->defaults['version'], false );
		}
	}

}

new Wse_Attachments_Update();
