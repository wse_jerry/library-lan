<?php
// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Wse_Attachments_Metabox class.
 * 
 * @class Wse_Attachments_Metabox
 */
class Wse_Attachments_Settings {

	private $capabilities;
	private $attachment_links;
	private $download_box_displays;
	private $contents;
	private $download_methods;
	private $libraries;
	private $choices;
	private $tabs;
	public $post_types;

	/**
	 * Constructor class.
	 */
	public function __construct() {
		//actions
		add_action( 'admin_menu', array( $this, 'settings_page' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'after_setup_theme', array( $this, 'load_defaults' ) );
		add_action( 'wp_loaded', array( $this, 'load_post_types' ) );
	}

	/**
	 * Load defaults
	 */
	public function load_defaults() {
		$this->tabs = array(
			'general' => array(
				'name'	 => __( 'General', 'wse-attachments' ),
				'key'	 => 'wse_attachments_general',
				'submit' => 'save_da_general',
				'reset'	 => 'reset_da_general'
			),
			'display' => array(
				'name'	 => __( 'Display', 'wse-attachments' ),
				'key'	 => 'wse_attachments_display',
				'submit' => 'save_da_display',
				'reset'	 => 'reset_da_display'
			),
			'admin' => array(
				'name'	 => __( 'Admin', 'wse-attachments' ),
				'key'	 => 'wse_attachments_admin',
				'submit' => 'save_da_admin',
				'reset'	 => 'reset_da_admin'
			)
		);
		
		$this->choices = array(
			'yes'	 => __( 'Enable', 'wse-attachments' ),
			'no'	 => __( 'Disable', 'wse-attachments' )
		);

		$this->libraries = array(
			'all'	 => __( 'All files', 'wse-attachments' ),
			'post'	 => __( 'Attached to a post only', 'wse-attachments' )
		);

		$this->capabilities = array(
			'manage_wse_attachments' => __( 'Manage download attachments', 'wse-attachments' )
		);

		$this->attachment_links = array(
			'media_library'	 => __( 'Media Library', 'wse-attachments' ),
			'modal'			 => __( 'Modal', 'wse-attachments' )
		);

		$this->download_box_displays = array(
			'before_content' => __( 'before the content', 'wse-attachments' ),
			'after_content'	 => __( 'after the content', 'wse-attachments' ),
			'manually'		 => __( 'manually', 'wse-attachments' )
		);

		$this->download_methods = array(
			'force'		 => __( 'Force download', 'wse-attachments' ),
			'redirect'	 => __( 'Redirect to file', 'wse-attachments' )
		);

		$this->contents = array(
			'caption'		 => __( 'caption', 'wse-attachments' ),
			'description'	 => __( 'description', 'wse-attachments' )
		);
	}

	/**
	 * Load post types.
	 */
	public function load_post_types() {
		$this->post_types = apply_filters( 'da_post_types', array_merge( array( 'post', 'page' ), get_post_types( array( '_builtin' => false, 'public' => true ), 'names' ) ) );
		sort( $this->post_types, SORT_STRING );
	}

	/**
	 * Add options page menu.
	 */
	public function settings_page() {
		add_options_page(
		__( 'Attachments', 'wse-attachments' ), __( 'Attachments', 'wse-attachments' ), 'manage_options', 'wse-attachments', array( $this, 'options_page' )
		);
	}

	/**
	 * Options pgae output callback.
	 * 
	 * @return mixed
	 */
	public function options_page() {
		$tab_key = ( isset( $_GET['tab'] ) ? esc_attr( $_GET['tab'] ) : 'general' );
		$options_page = 'options-general.php';
		$form_page = 'options.php';
		
		echo '
		<div class="wrap">' . screen_icon() . '
			<h1>' . __( 'Wse Attachments', 'wse-attachments' ) . '</h1>';

			echo '
			<h2 class="nav-tab-wrapper">';

		foreach ( $this->tabs as $key => $name ) {
			echo '
			<a class="nav-tab ' . ( $tab_key == $key ? 'nav-tab-active' : '' ) . '" href="' . esc_url( admin_url( $options_page . '?page=wse-attachments&tab=' . $key ) ) . '">' . $name['name'] . '</a>';
		}

		echo '
			</h2>
			<div class="wse-attachments-settings">';
		
		echo '
				<div class="df-sidebar">
					<div class="df-credits">
						<h3 class="hndle">' . __( 'Wse Attachments', 'wse-attachments' ) . ' 1.0.0</h3>
						<div class="inside">
							<h4 class="inner">' . __( 'Need support?', 'wse-attachments' ) . '</h4>
							<p class="inner">' . sprintf( __( 'If you are having problems with this plugin, please browse it\'s <a href="%s" target="_blank">Documentation</a> or talk about them in the <a href="%s" target="_blank">Support forum</a>', 'wse-attachments' ), 'https://www.dfactory.eu/docs/wse-attachments/?utm_source=wse-attachments-settings&utm_medium=link&utm_campaign=docs', 'https://www.dfactory.eu/support/?utm_source=wse-attachments-settings&utm_medium=link&utm_campaign=support' ) . '</p>
							<hr/>
							<h4 class="inner">' . __( 'Secondary Development', 'wse-attachments' ) . '</h4>
							<p class="inner">来自网上的开源插件，由华尔街英语进行了二次开发
							</p>
							<hr/>
							<p class="df-link inner">' . __( 'Created by', 'wse-attachments' ) . ' <a href="http://www.dfactory.eu/?utm_source=wse-attachments-settings&utm_medium=link&utm_campaign=created-by" target="_blank" title="dFactory - Quality plugins for WordPress"><img src="' . WSE_ATTACHMENTS_URL . '/images/logo-dfactory.png' . '" title="dFactory - Quality plugins for WordPress" alt="dFactory - Quality plugins for WordPress"/></a></p>
						</div>
					</div>
				</div>';
		
		echo '
				<form action="' . $form_page . '" method="post" >';
		
		wp_nonce_field( 'update-options' );

		settings_fields( $this->tabs[$tab_key]['key'] );
		do_settings_sections( $this->tabs[$tab_key]['key'] );

		echo '
					<p class="submit">';
		submit_button( '', 'primary ' . $this->tabs[$tab_key]['submit'], $this->tabs[$tab_key]['submit'], false );
		echo ' ';
		submit_button( __( 'Reset to defaults', 'wse-attachments' ), 'secondary ' . $this->tabs[$tab_key]['reset'], $this->tabs[$tab_key]['reset'], false );
		
		echo '
					</p>';

		echo '
				</form>
			</div>
			<div class="clear"></div>
		</div>';
	}

	/**
	 * Register settings.
	 */
	public function register_settings() {
		global $pagenow;
		
		// general section
		register_setting( 'wse_attachments_general', 'wse_attachments_general', array( $this, 'validate_general' ) );
		add_settings_section( 'wse_attachments_general', __( 'General settings', 'wse-attachments' ), '', 'wse_attachments_general' );
		add_settings_field( 'da_general_label', __( 'Label', 'wse-attachments' ), array( $this, 'da_general_label' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_user_roles', __( 'User roles', 'wse-attachments' ), array( $this, 'da_general_user_roles' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_post_types', __( 'Supported post types', 'wse-attachments' ), array( $this, 'da_general_post_types' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_download_method', __( 'Download method', 'wse-attachments' ), array( $this, 'da_general_download_method' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_pretty_urls', __( 'Pretty URLs', 'wse-attachments' ), array( $this, 'da_general_pretty_urls' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_encrypt_urls', __( 'Encrypt URLs', 'wse-attachments' ), array( $this, 'da_general_encrypt_urls' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_reset_downloads', __( 'Reset count', 'wse-attachments' ), array( $this, 'da_general_reset_downloads' ), 'wse_attachments_general', 'wse_attachments_general' );
		add_settings_field( 'da_general_deactivation_delete', __( 'Deactivation', 'wse-attachments' ), array( $this, 'da_general_deactivation_delete' ), 'wse_attachments_general', 'wse_attachments_general' );

		// frontend section
		register_setting( 'wse_attachments_display', 'wse_attachments_general', array( $this, 'validate_general' ) );
		add_settings_section( 'wse_attachments_display', __( 'Display settings', 'wse-attachments' ), '', 'wse_attachments_display' );
		add_settings_field( 'da_general_frontend_display', __( 'Fields display', 'wse-attachments' ), array( $this, 'da_general_frontend_display' ), 'wse_attachments_display', 'wse_attachments_display' );
		add_settings_field( 'da_general_display_style', __( 'Display style', 'wse-attachments' ), array( $this, 'da_general_display_style' ), 'wse_attachments_display', 'wse_attachments_display' );
		add_settings_field( 'da_general_frontend_content', __( 'Downloads description', 'wse-attachments' ), array( $this, 'da_general_frontend_content' ), 'wse_attachments_display', 'wse_attachments_display' );
		add_settings_field( 'da_general_css_style', __( 'Use CSS style', 'wse-attachments' ), array( $this, 'da_general_css_style' ), 'wse_attachments_display', 'wse_attachments_display' );
		add_settings_field( 'da_general_download_box_display', __( 'Display position', 'wse-attachments' ), array( $this, 'da_general_download_box_display' ), 'wse_attachments_display', 'wse_attachments_display' );
		
		// admin section
		register_setting( 'wse_attachments_admin', 'wse_attachments_general', array( $this, 'validate_general' ) );
		add_settings_section( 'wse_attachments_admin', __( 'Admin settings', 'wse-attachments' ), '', 'wse_attachments_admin' );
		add_settings_field( 'da_general_backend_display', __( 'Fields display', 'wse-attachments' ), array( $this, 'da_general_backend_display' ), 'wse_attachments_admin', 'wse_attachments_admin' );
		add_settings_field( 'da_general_backend_content', __( 'Downloads description', 'wse-attachments' ), array( $this, 'da_general_backend_content' ), 'wse_attachments_admin', 'wse_attachments_admin' );
		add_settings_field( 'da_restrict_edit_downloads', __( 'Restrict Edit', 'wse-attachments' ), array( $this, 'da_restrict_edit_downloads' ), 'wse_attachments_admin', 'wse_attachments_admin' );
		add_settings_field( 'da_general_attachment_link', __( 'Edit attachment link', 'wse-attachments' ), array( $this, 'da_general_attachment_link' ), 'wse_attachments_admin', 'wse_attachments_admin' );
		add_settings_field( 'da_general_libraries', __( 'Media Library', 'wse-attachments' ), array( $this, 'da_general_libraries' ), 'wse_attachments_admin', 'wse_attachments_admin' );
		add_settings_field( 'da_general_downloads_in_media_library', __( 'Downloads count', 'wse-attachments' ), array( $this, 'da_general_downloads_in_media_library' ), 'wse_attachments_admin', 'wse_attachments_admin' );
	}

	public function da_general_label() {
		echo '
		<div id="da_general_label">
			<input type="text" class="regular-text" name="wse_attachments_general[label]" value="' . esc_attr( Wse_Attachments()->options['label'] ) . '"/>
			<br/>
			<p class="description">' . esc_html__( 'Enter download attachments list label.', 'wse-attachments' ) . '</p>
		</div>';
	}

	public function da_general_post_types() {
		echo '
		<div id="da_general_post_types">
			<fieldset>';

			foreach ( $this->post_types as $val ) {
				echo '
			<input id="da-general-post-types-' . $val . '" type="checkbox" name="wse_attachments_general[post_types][]" value="' . esc_attr( $val ) . '" ' . checked( true, (isset( Wse_Attachments()->options['post_types'][$val] ) ? Wse_Attachments()->options['post_types'][$val] : false ), false ) . '/><label for="da-general-post-types-' . $val . '">' . $val . '</label>';
			}

			echo '
				<p class="description">' . __( 'Select which post types would you like to enable for your downloads.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_download_method() {
		echo '
		<div id="da_general_download_method">
			<fieldset>';

			foreach ( $this->download_methods as $val => $trans ) {
				echo '
			<input id="da-general-download-method-' . $val . '" type="radio" name="wse_attachments_general[download_method]" value="' . esc_attr( $val ) . '" ' . checked( $val, Wse_Attachments()->options['download_method'], false ) . '/><label for="da-general-download-method-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select download method.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_backend_display() {
		echo '
		<div id="da_general_backend_display">
			<fieldset>';

			foreach ( Wse_Attachments()->columns as $val => $trans ) {
				if ( ! in_array( $val, array( 'title', 'index', 'icon', 'exclude' ), true ) )
					echo '
			<input id="da-general-backend-display-' . $val . '" type="checkbox" name="wse_attachments_general[backend_columns][]" value="' . esc_attr( $val ) . '" ' . checked( true, Wse_Attachments()->options['backend_columns'][$val], false ) . '/><label for="da-general-backend-display-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select which columns would you like to enable on backend for your downloads.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_frontend_display() {
		echo '
		<div id="da_general_frontend_display">
			<fieldset>';

			foreach ( Wse_Attachments()->columns as $val => $trans ) {
				if ( ! in_array( $val, array( 'id', 'type', 'title', 'exclude' ), true ) )
					echo '
			<input id="da-general-frontend-display-' . $val . '" type="checkbox" name="wse_attachments_general[frontend_columns][]" value="' . esc_attr( $val ) . '" ' . checked( true, Wse_Attachments()->options['frontend_columns'][$val], false ) . '/><label for="da-general-frontend-display-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select which columns would you like to enable on frontend for your downloads.', 'wse-attachments' ) . '</p>
			<fieldset>
		</div>';
	}

	public function da_general_css_style() {
		echo '
		<div id="da_general_css_style">
			<fieldset>';

			foreach ( $this->choices as $val => $trans ) {
				echo '
			<input id="da-general-css-style-' . $val . '" type="radio" name="wse_attachments_general[use_css_style]" value="' . esc_attr( $val ) . '" ' . checked( ($val === 'yes' ? true : false ), Wse_Attachments()->options['use_css_style'], false ) . '/><label for="da-general-css-style-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select if you\'d like to use bultin CSS style.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_pretty_urls() {
		echo '
		<div id="da_general_pretty_urls">
			<fieldset>';

			foreach ( $this->choices as $val => $trans ) {
				echo '
			<input id="da-general-pretty-urls-' . $val . '" type="radio" name="wse_attachments_general[pretty_urls]" value="' . esc_attr( $val ) . '" ' . checked( ($val === 'yes' ? true : false ), Wse_Attachments()->options['pretty_urls'], false ) . '/><label for="da-general-pretty-urls-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Enable if you want to use pretty URLs.', 'wse-attachments' ) . '</p>
			<div id="da_general_download_link"' . ( ! Wse_Attachments()->options['pretty_urls'] ? ' style="display: none;"' : '') . '>
				<label for="da_general_download_link_label">' . __( 'Slug', 'wse-attachments' ) . '</label>: <input id="da_general_download_link_label" type="text" name="wse_attachments_general[download_link]" class="regular-text" value="' . esc_attr( Wse_Attachments()->options['download_link'] ) . '"/>
				<p class="description"><code>' . site_url() . '/<strong>' . Wse_Attachments()->options['download_link'] . '</strong>/123/</code></p>
				<p class="description">' . __( 'Download link slug.', 'wse-attachments' ) . '</p>	
			</div>
			</fieldset>
		</div>';
	}
	
	public function da_general_encrypt_urls() {
		echo '
		<div id="da_general_pretty_urls">
			<fieldset>';

			foreach ( $this->choices as $val => $trans ) {
				echo '
			<input id="da-general-encrypt-urls-' . $val . '" type="radio" name="wse_attachments_general[encrypt_urls]" value="' . esc_attr( $val ) . '" ' . checked( ($val === 'yes' ? true : false ), isset( Wse_Attachments()->options['encrypt_urls'] ) ? Wse_Attachments()->options['encrypt_urls'] : Wse_Attachments()->defaults['general']['encrypt_urls'] , false ) . '/><label for="da-general-encrypt-urls-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Enable if you want to encrypt the attachment ids in generated URL\'s', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_download_box_display() {
		echo '
		<div id="da_general_download_box_display">
			<fieldset>';

			foreach ( $this->download_box_displays as $val => $trans ) {
				echo '
			<input id="da-general-download-box-display-' . $val . '" type="radio" name="wse_attachments_general[download_box_display]" value="' . esc_attr( $val ) . '" ' . checked( $val, Wse_Attachments()->options['download_box_display'], false ) . '/><label for="da-general-download-box-display-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select where you would like your download attachments to be displayed.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_display_style() {
		echo '
		<div id="da_general_display_style">
			<fieldset>';

			foreach ( Wse_Attachments()->display_styles as $val => $trans ) {
				echo '
			<input id="da-general-display-style-' . $val . '" type="radio" name="wse_attachments_general[display_style]" value="' . esc_attr( $val ) . '" ' . checked( $val, isset( Wse_Attachments()->options['display_style'] ) ? esc_attr( Wse_Attachments()->options['display_style'] ) : Wse_Attachments()->defaults['general']['display_style'], false ) . '/><label for="da-general-display-style-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select display style for file attachments.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_backend_content() {
		echo '
		<div id="da_general_backend_content">
			<fieldset>';

			foreach ( $this->contents as $val => $trans ) {
				echo '
			<input id="da-general-backend-content-' . $val . '" type="checkbox" name="wse_attachments_general[backend_content][]" value="' . esc_attr( $val ) . '" ' . checked( true, (isset( Wse_Attachments()->options['backend_content'][$val] ) ? Wse_Attachments()->options['backend_content'][$val] : false ), false ) . '/><label for="da-general-backend-content-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select what fields to use on backend for download attachments description.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	/**
	 * Limit views edit to admins.
	 */
	public function da_restrict_edit_downloads() {
		echo '
		<div id="da_restrict_edit_downloads">
			<fieldset>
				<label><input type="checkbox" name="wse_attachments_general[restrict_edit_downloads]" value="1" ' . checked( true, Wse_Attachments()->options['restrict_edit_downloads'], false ) . ' />' . __( 'Enable to restrict downloads count editing to admins only.', 'wse-attachments' ) . '</label>
			</fieldset>
		</div>';
	}

	public function da_general_frontend_content() {
		echo '
		<div id="da_general_frontend_content">
			<fieldset>';

			foreach ( $this->contents as $val => $trans ) {
				echo '
			<input id="da-general-frontend-content-' . $val . '" type="checkbox" name="wse_attachments_general[frontend_content][]" value="' . esc_attr( $val ) . '" ' . checked( true, (isset( Wse_Attachments()->options['frontend_content'][$val] ) ? Wse_Attachments()->options['frontend_content'][$val] : false ), false ) . '/><label for="da-general-frontend-content-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select what fields to use on frontend for download attachments description.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_attachment_link() {
		echo '
		<div id="da_general_attachment_link">
			<fieldset>';

			foreach ( $this->attachment_links as $val => $trans ) {
				echo '
			<input id="da-general-attachment-link-' . $val . '" type="radio" name="wse_attachments_general[attachment_link]" value="' . esc_attr( $val ) . '" ' . checked( $val, Wse_Attachments()->options['attachment_link'], false ) . '/><label for="da-general-attachment-link-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select where you would like to edit download attachments.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_libraries() {
		echo '
		<div id="da_general_libraries">
			<fieldset>';

			foreach ( $this->libraries as $val => $trans ) {
				echo '
			<input id="da-general-libraries-' . $val . '" type="radio" name="wse_attachments_general[library]" value="' . esc_attr( $val ) . '" ' . checked( $val, Wse_Attachments()->options['library'], false ) . '/><label for="da-general-libraries-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select which attachments should be visible in Media Library window.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_downloads_in_media_library() {
		echo '
		<div id="da_general_downloads_in_media_library">
			<fieldset>';

			foreach ( $this->choices as $val => $trans ) {
				echo '
			<input id="da-general-downloads-in-media-library-' . $val . '" type="radio" name="wse_attachments_general[downloads_in_media_library]" value="' . esc_attr( $val ) . '" ' . checked( ($val === 'yes' ? true : false ), Wse_Attachments()->options['downloads_in_media_library'], false ) . '/><label for="da-general-downloads-in-media-library-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Enable if you want to display downloads count in your Media Library columns.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_user_roles() {
		global $wp_roles;

		$editable_roles = get_editable_roles();
		
		echo '
		<div id="da_general_user_roles">
			<fieldset>';

			foreach ( $editable_roles as $val => $trans ) {
				$role = $wp_roles->get_role( $val );

				// admins have access by default
				if ( $role->has_cap( 'manage_options' ) )
					continue;
				
				echo '
				<input id="da-general-user-roles-' . $val . '" type="checkbox" name="wse_attachments_general[user_roles][]" value="' . $val . '" ' . checked( true, in_array( $val, ( isset( Wse_Attachments()->options['user_roles'] ) ? Wse_Attachments()->options['user_roles'] : Wse_Attachments()->defaults['general']['user_roles'] ) ), false ) . '/><label for="da-general-user-roles-' . $val . '">' . translate_user_role( $wp_roles->role_names[$val] ) . '</label>';
			}

			echo '
			<p class="description">' . __( 'Select user roles allowed to add, remove and manage attachments.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	public function da_general_deactivation_delete() {
		echo '
		<div id="da_general_deactivation_delete">
			<fieldset>';

			foreach ( $this->choices as $val => $trans ) {
				echo '
			<input id="da-general-deactivation-delete-' . $val . '" type="radio" name="wse_attachments_general[deactivation_delete]" value="' . esc_attr( $val ) . '" ' . checked( ($val === 'yes' ? true : false ), Wse_Attachments()->options['deactivation_delete'], false ) . '/><label for="da-general-deactivation-delete-' . $val . '">' . $trans . '</label>';
			}

			echo '
			<p class="description">' . __( 'Enable if you want all plugin data to be deleted on deactivation.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}
	
	public function da_general_reset_downloads() {
		echo '
		<div id="da_general_deactivation_delete">
			<fieldset>';
		
		submit_button( __( 'Reset downloads', 'wse-attachments' ), 'secondary', 'reset_da_downloads', false );
		
		echo '
				<p class="description">' . __( 'Click to reset the downloads count for all the attachments.', 'wse-attachments' ) . '</p>
			</fieldset>
		</div>';
	}

	/**
	 * Validate general settings, reset general settings, reset download counts.
	 * 
	 * @global object $wp_roles
	 * @global object $wpdb
	 * @param array $input
	 * @return array
	 */
	public function validate_general( $input ) {
		// get old input for saving tabs
		$old_input = Wse_Attachments()->options;

		// save general
		if ( isset( $_POST['save_da_general'] ) ) {
			$new_input = $old_input;

			global $wp_roles;

			// label
			$new_input['label'] = sanitize_text_field( $input['label'] );

			// capabilities
			$user_roles = array();

			foreach ( $wp_roles->roles as $role_name => $role_text ) {
				$role = $wp_roles->get_role( $role_name );

				if ( ! $role->has_cap( 'manage_options' ) ) {
					if ( ! empty( $input['user_roles'] ) && in_array( $role_name, array_map( 'esc_attr', $input['user_roles'] ) ) ) {
						$role->add_cap( Wse_Attachments()->capability );
						$user_roles[] = $role_name;
					} else
						$role->remove_cap( Wse_Attachments()->capability );
				}
			}

			$new_input['user_roles'] = $user_roles;

			// post types
			$post_types = array();
			$new_input['post_types'] = (isset( $input['post_types'] ) ? $input['post_types'] : array());

			foreach ( $this->post_types as $post_type ) {
				$post_types[$post_type] = (in_array( $post_type, $input['post_types'], true ) ? true : false);
			}

			$new_input['post_types'] = $post_types;

			// download method
			$new_input['download_method'] = isset( $input['download_method'], $this->download_methods[$input['download_method']] ) ? $input['download_method'] : Wse_Attachments()->defaults['general']['download_method'];

			// pretty urls
			$new_input['pretty_urls'] = isset( $input['pretty_urls'], $this->choices[$input['pretty_urls']] ) ? ( $input['pretty_urls'] === 'yes' ? true : false ) : Wse_Attachments()->defaults['general']['pretty_urls'];

			// download link
			if ( $input['pretty_urls'] ) {
				$new_input['download_link'] = sanitize_title( $input['download_link'] );

				if ( $new_input['download_link'] === '' )
					$new_input['download_link'] = Wse_Attachments()->defaults['general']['download_link'];
			} else
				$new_input['download_link'] = Wse_Attachments()->defaults['general']['download_link'];

			// encrypt urls
			$new_input['encrypt_urls'] = isset( $input['encrypt_urls'], $this->choices[$input['encrypt_urls']] ) ? ( $input['encrypt_urls'] === 'yes' ? true : false ) : Wse_Attachments()->defaults['general']['encrypt_urls'];

			// deactivation delete
			$new_input['deactivation_delete'] = (isset( $input['deactivation_delete'] ) && in_array( $input['deactivation_delete'], array_keys( $this->choices ), true ) ? ($input['deactivation_delete'] === 'yes' ? true : false) : Wse_Attachments()->defaults['general']['deactivation_delete']);

			$input = $new_input;
		// save display
		} elseif ( isset( $_POST['save_da_display'] ) ) {
			$new_input = $old_input;

			// frontend columns
			$columns = array();
			$input['frontend_columns'] = (isset( $input['frontend_columns'] ) ? $input['frontend_columns'] : array());

			foreach ( Wse_Attachments()->columns as $column => $text ) {
				if ( in_array( $column, array( 'id', 'type', 'exclude' ), true ) )
					continue;
				elseif ( $column === 'title' )
					$columns[$column] = true;
				else
					$columns[$column] = (in_array( $column, $input['frontend_columns'], true ) ? true : false);
			}

			$new_input['frontend_columns'] = $columns;

			// frontend content
			$contents = array();
			$input['frontend_content'] = (isset( $input['frontend_content'] ) ? $input['frontend_content'] : array());

			foreach ( $this->contents as $content => $trans ) {
				$contents[$content] = (in_array( $content, $input['frontend_content'], true ) ? true : false);
			}

			$new_input['frontend_content'] = $contents;
			
			// display style
			$new_input['display_style'] = isset( $input['display_style'], Wse_Attachments()->display_styles[$input['display_style']] ) ? $input['display_style'] : Wse_Attachments()->defaults['general']['display_style'];

			// use css style
			$new_input['use_css_style'] = (isset( $input['use_css_style'] ) && in_array( $input['use_css_style'], array_keys( $this->choices ), true ) ? ($input['use_css_style'] === 'yes' ? true : false) : Wse_Attachments()->defaults['general']['use_css_style']);
			
			// download box display
			$new_input['download_box_display'] = (isset( $input['download_box_display'] ) && in_array( $input['download_box_display'], array_keys( $this->download_box_displays ), true ) ? $input['download_box_display'] : Wse_Attachments()->defaults['general']['download_box_display']);
			
			$input = $new_input;
		// save admin
		} elseif ( isset( $_POST['save_da_admin'] ) ) {
			$new_input = $old_input;

			// backend columns
			$columns = array();
			$input['backend_columns'] = (isset( $input['backend_columns'] ) ? $input['backend_columns'] : array());

			foreach ( Wse_Attachments()->columns as $column => $text ) {
				if ( in_array( $column, array( 'index', 'icon', 'exclude' ), true ) )
					continue;
				if ( $column === 'title' )
					$columns[$column] = true;
				else
					$columns[$column] = (in_array( $column, $input['backend_columns'], true ) ? true : false);
			}

			$new_input['backend_columns'] = $columns;

			$new_input['restrict_edit_downloads'] = array_key_exists( 'restrict_edit_downloads', $input );

			// backend content
			$contents = array();
			$input['backend_content'] = (isset( $input['backend_content'] ) ? $input['backend_content'] : array());

			foreach ( $this->contents as $content => $trans ) {
				$contents[$content] = (in_array( $content, $input['backend_content'], true ) ? true : false);
			}

			$new_input['backend_content'] = $contents;
			
			// attachment link
			$new_input['attachment_link'] = (isset( $input['attachment_link'] ) && in_array( $input['attachment_link'], array_keys( $this->attachment_links ), true ) ? $input['attachment_link'] : Wse_Attachments()->defaults['general']['attachment_link']);

			// library
			$new_input['library'] = (isset( $input['library'] ) && in_array( $input['library'], array_keys( $this->libraries ), true ) ? $input['library'] : Wse_Attachments()->defaults['general']['library']);
			
			// downloads in media library
			$new_input['downloads_in_media_library'] = (isset( $input['downloads_in_media_library'] ) && in_array( $input['downloads_in_media_library'], array_keys( $this->choices ), true ) ? ($input['downloads_in_media_library'] === 'yes' ? true : false) : Wse_Attachments()->defaults['general']['downloads_in_media_library']);
			
			$input = $new_input;
			
		// reset general
		} elseif ( isset( $_POST['reset_da_general'] ) ) {
			$new_input = $old_input;
			
			global $wp_roles;

			// capabilities
			$new_input['user_roles'] = array();
			
			foreach ( $wp_roles->roles as $role_name => $display_name ) {
				$role = $wp_roles->get_role( $role_name );

					if ( $role->has_cap( 'upload_files' ) ) {
						$role->add_cap( $capability );
						
						$new_input['user_roles'][] = $role_name;
					} else {
						$role->remove_cap( $capability );
					}
			}
			
			$keys = array(
				'label',
				'download_method',
				'post_types',
				'pretty_urls',
				'download_link',
				'encrypt_urls',
				'deactivation_delete'
			);

			foreach( $keys as $key ) {
				if ( array_key_exists( $key, Wse_Attachments()->defaults['general'] ) ) {
					$new_input[$key] = Wse_Attachments()->defaults['general'][$key];
				}
			}
			
			$input = $new_input;

			add_settings_error( 'reset_general_settings', 'reset_general_settings', __( 'General settings restored to defaults.', 'wse-attachments' ), 'updated' );
			
		// reset display
		} elseif ( isset( $_POST['reset_da_display'] ) ) {
			$new_input = $old_input;

			$keys = array(
				'frontend_columns',
				'display_style',
				'frontend_content',
				'use_css_style',
				'download_box_display'
			);

			foreach( $keys as $key ) {
				if ( array_key_exists( $key, Wse_Attachments()->defaults['general'] ) ) {
					$new_input[$key] = Wse_Attachments()->defaults['general'][$key];
				}
			}
			
			$input = $new_input;

			add_settings_error( 'reset_display_settings', 'reset_display_settings', __( 'Display settings restored to defaults.', 'wse-attachments' ), 'updated' );
			
		// reset admin
		} elseif ( isset( $_POST['reset_da_admin'] ) ) {
			$new_input = $old_input;

			$keys = array(
				'backend_columns',
				'backend_content',
				'attachment_link',
				'library',
				'downloads_in_media_library'
			);

			foreach( $keys as $key ) {
				if ( array_key_exists( $key, Wse_Attachments()->defaults['general'] ) ) {
					$new_input[$key] = Wse_Attachments()->defaults['general'][$key];
				}
			}
			
			$input = $new_input;

			add_settings_error( 'reset_admin_settings', 'reset_admin_settings', __( 'Admin settings restored to defaults.', 'wse-attachments' ), 'updated' );
			
		// reset downloads
		} elseif ( isset( $_POST['reset_da_downloads'] ) ) {
			global $wpdb;

			// lets use wpdb to reset downloads a lot faster then normal update_post_meta for each post_id
			$result = $wpdb->update(
			$wpdb->postmeta, array( 'meta_value' => 0 ), array( 'meta_key' => '_da_downloads' ), '%d', '%s'
			);

			$input = Wse_Attachments()->options;

			if ( $result === false )
				add_settings_error( 'reset_downloads', 'reset_downloads_error', __( 'Error occurred while resetting the downloads count.', 'wse-attachments' ), 'error' );
			else
				add_settings_error( 'reset_downloads', 'reset_downloads_updated', __( 'Attachments downloads count has been reset.', 'wse-attachments' ), 'updated' );
		}

		return $input;
	}

}

new Wse_Attachments_Settings();
