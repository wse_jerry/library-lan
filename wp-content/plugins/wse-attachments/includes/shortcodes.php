<?php
// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Wse_Attachments_Metabox class.
 * 
 * @class Wse_Attachments_Metabox
 */
class Wse_Attachments_Shortcodes {

	/**
	 * Constructor class.
	 */
	public function __construct() {
		// actions
		add_action( 'init', array( $this, 'register_download_shortcodes' ) );
	}

	/**
	 * Register download attachments shortcodes.
	 */
	public function register_download_shortcodes() {
		add_shortcode( 'wse-attachments', array( $this, 'wse_attachments_shortcode' ) );
		add_shortcode( 'wse-attachment', array( $this, 'wse_attachment_shortcode' ) );
	}

	/**
	 * Handle wse-attachments shortcode.
	 * 
	 * @param array $args
	 * @return mixed
	 */
	public function wse_attachments_shortcode( $args ) {
		$defaults = array(
			'post_id'				 => 0,
			'container'				 => 'div',
			'container_class'		 => 'wse-attachments attach',
			'container_id'			 => '',
			'style'					 => isset( Wse_Attachments()->options['display_style'] ) ? esc_attr( Wse_Attachments()->options['display_style'] ) : 'list',
			'link_before'			 => '',
			'link_after'			 => '',
			'display_index'			 => isset( Wse_Attachments()->options['frontend_columns']['index'] ) ? (int) Wse_Attachments()->options['frontend_columns']['index'] : 0,
			'display_user'			 => (int) Wse_Attachments()->options['frontend_columns']['author'],
			'display_icon'			 => (int) Wse_Attachments()->options['frontend_columns']['icon'],
			'display_count'			 => (int) Wse_Attachments()->options['frontend_columns']['downloads'],
			'display_size'			 => (int) Wse_Attachments()->options['frontend_columns']['size'],
			'display_date'			 => (int) Wse_Attachments()->options['frontend_columns']['date'],
			'display_caption'		 => (int) Wse_Attachments()->options['frontend_content']['caption'],
			'display_description'	 => (int) Wse_Attachments()->options['frontend_content']['description'],
			'display_empty'			 => 0,
			'display_option_none'	 => __( 'No attachments to download', 'wse-attachments' ),
			'use_desc_for_title'	 => 0,
			'exclude'				 => '',
			'include'				 => '',
			'title'					 => __( 'Attachments', 'wse-attachments' ),
			'title_container'		 => 'div',
			'title_class'			 => 'download-title title',
			'orderby'				 => 'menu_order',
			'order'					 => 'asc',
			'echo'					 => 1
		);

		// we have to force return in shortcodes
		$args['echo'] = 0;

		if ( ! isset( $args['title'] ) ) {
			$args['title'] = '';

			if ( Wse_Attachments()->options['label'] !== '' )
				$args['title'] = Wse_Attachments()->options['label'];
		}

		$args = shortcode_atts( $defaults, $args );

		// reassign post id
		$post_id = (int) (empty( $args['post_id'] ) ? get_the_ID() : $args['post_id']);

		// unset from args
		unset( $args['post_id'] );

		return da_display_wse_attachments( $post_id, $args );
	}

	/**
	 * Handle wse-attachment shortcode.
	 */
	public function wse_attachment_shortcode( $args ) {
		$defaults = array(
			'attachment_id'	 => 0, // deprecated
			'id'			 => 0,
			'title'			 => '',
			'class'			 => ''
		);

		$args = shortcode_atts( $defaults, $args );

		// deprecated attachment_id parameter support
		$args['id'] = ! empty( $args['attachment_id'] ) ? (int) $args['attachment_id'] : (int) $args['id'];

		$atts = array();

		if ( ! empty( $args['title'] ) ) {
			$atts['title'] = $args['title'];
		}

		if ( ! empty( $args['class'] ) ) {
			$atts['class'] = $args['class'];
		}

		return da_wse_attachment_link( (int) $args['id'], false, $atts );
	}

}

new Wse_Attachments_Shortcodes();
