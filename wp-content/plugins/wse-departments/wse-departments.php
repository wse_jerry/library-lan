<?php

/**
 * Plugin Name: Wse Department
 * Plugin URI:  https://wse.com.cn
 * Author:      Jerry Qi
 * Author URI:  https://jerryqi.cn
 * Description: Manage users department and position.
 * Version:     1.0
 * Text Domain: wse-departments
 * Domain Path: /wse-departments/assets/languages/
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Include the User Departments files
 *
 * @since 0.1.0
 */
function _wse_departments() {

	// Get the plugin path
	$plugin_path = plugin_dir_path( __FILE__ ) . 'wse-departments/';

	// Classes
	require_once $plugin_path . 'includes/classes/class-user-taxonomy.php';

	// Functions
	require_once $plugin_path . 'includes/functions/admin.php';
	require_once $plugin_path . 'includes/functions/common.php';
	require_once $plugin_path . 'includes/functions/taxonomies.php';
	require_once $plugin_path . 'includes/functions/hooks.php';
}
add_action( 'plugins_loaded', '_wse_departments' );

/**
 * Return the plugin URL
 *
 * @since 0.1.4
 *
 * @return string
 */
function wse_departments_get_plugin_url() {
	return plugin_dir_url( __FILE__ ) . 'wse-departments/';
}

/**
 * Return the asset version
 *
 * @since 0.1.4
 *
 * @return int
 */
function wse_departments_get_asset_version() {
	return 201806050001;
}
