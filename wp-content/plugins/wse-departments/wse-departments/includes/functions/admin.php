<?php

/**
 * User Departments Admin
 *
 * @package Plugins/Users/Departments/Admin
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Tweak admin styling for a user groups layout
 *
 * @since 0.1.4
 */
function wse_departments_admin_assets() {
	$url = wse_departments_get_plugin_url();	
	$ver = wse_departments_get_asset_version();

	wp_enqueue_style( 'wse_departments', $url. 'assets/css/departments.css', false, $ver, false );
}


/**
 * Add new section to User Profiles
 *
 * @since 0.1.9
 *
 * @param array $sections
 */
function wse_departments_add_profile_section( $sections = array() ) {

	// Copy for modifying
	$new_sections = $sections;

	// Add the "Activity" section
	$new_sections['groups'] = array(
		'id'    => 'groups',
		'slug'  => 'groups',
		'name'  => esc_html__( 'Departments', 'wp-user-activity' ),
		'cap'   => 'edit_profile',
		'icon'  => 'dashicons-groups',
		'order' => 90
	);

	// Filter & return
	return apply_filters( 'wse_departments_add_profile_section', $new_sections, $sections );
}
