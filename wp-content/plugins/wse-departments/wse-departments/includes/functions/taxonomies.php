<?php

/**
 * User Departments Taxonomies
 *
 * @package Plugins/Users/Departments/Taxonomy
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register default user group taxonomies
 *
 * This function is hooked onto WordPress's `init` action and creates two new
 * `WP_User_Taxonomy` objects for user "groups" and "types". It can be unhooked
 * and these taxonomies can be replaced with your own custom ones.
 *
 * @since 0.1.4
 */
function wp_register_default_department_taxonomy() {
	new WP_User_Taxonomy( 'department', 'users/group', array(
		'singular' => __( 'Department',  'wse-departments' ),
		'plural'   => __( 'Wse Departments', 'wse-departments' )
	) );
}

/**
 * Register default user group taxonomies
 *
 * This function is hooked onto WordPress's `init` action and creates two new
 * `WP_User_Taxonomy` objects for user "groups" and "types". It can be unhooked
 * and these taxonomies can be replaced with your own custom ones.
 *
 * @since 0.1.4
 */
function wp_register_default_position_taxonomy() {
	new WP_User_Taxonomy( 'position',  'users/type', array(
		'singular' => __( 'Position',  'wse-departments' ),
		'plural'   => __( 'Wse Positions', 'wse-departments' )
	) );
}
