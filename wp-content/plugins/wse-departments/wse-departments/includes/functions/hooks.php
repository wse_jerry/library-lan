<?php

/**
 * User Departments Hooks
 *
 * @package Plugins/Users/Departments/Hooks
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

// Register the default taxonomies
add_action( 'init', 'wp_register_default_department_taxonomy' );
add_action( 'init', 'wp_register_default_position_taxonomy'  );

// Enqueue assets
add_action( 'admin_head', 'wse_departments_admin_assets' );

// WP User Profiles
add_filter( 'wp_user_profiles_sections', 'wse_departments_add_profile_section' );
