var selArray = [],
    searchArray = [];
$(function() {
    init();

    if (document.getElementById('searchList')) {
        // $.getJSON('../ss/js/search.json', function(json) {
        //     searchInit(json)
        // });
        // searchInit([
        //     { "group": "A", "list": ["A1", "A2"] },
        //     { "group": "B", "list": ["B1", "B2", "B3"] },
        //     { "group": "C", "list": ["C1", "C2"] },
        //     { "group": "D", "list": ["D1", "D2", "D3"] },
        //     { "group": "E", "list": ["E1", "E2"] },
        //     { "group": "F", "list": ["F1", "F2", "F3"] },
        //     { "group": "G", "list": ["G1", "G2"] },
        //     { "group": "H", "list": ["H1", "H2"] },
        //     { "group": "I", "list": ["I1", "I2"] },
        //     { "group": "J", "list": ["J1", "J2"] },
        //     { "group": "K", "list": ["K1", "K2"] },
        //     { "group": "L", "list": ["L1", "L2"] },
        //     { "group": "M", "list": ["M1", "M2", "M3"] },
        //     { "group": "N", "list": ["N1", "N2"] },
        //     { "group": "O", "list": ["O1", "O2"] },
        //     { "group": "P", "list": ["P1", "P2"] },
        //     { "group": "Q", "list": ["Q1", "Q2"] },
        //     { "group": "R", "list": ["R1", "R2"] },
        //     { "group": "S", "list": ["S1", "S2"] },
        //     { "group": "T", "list": ["T1", "T2"] },
        //     { "group": "U", "list": ["U1", "U2"] },
        //     { "group": "V", "list": ["V1", "V2"] },
        //     { "group": "W", "list": ["W1", "W2"] },
        //     { "group": "X", "list": ["X1", "X2"] },
        //     { "group": "Y", "list": ["Y1", "Y2"] },
        //     { "group": "Z", "list": ["Z1", "Z2"] }
        // ]);
    }

    if ($('div').hasClass('setup')) {
        userSetup();
    }



});

function init() {
    $(window).resize(function() {
        adjust();
        popH();
    });
    adjust();
    selectInit();
    search();


    $('.pop .close').on('click', function() {
        if ($('.outer').find('video').length > 0) {
            $('video').get(0).pause();
        }
        $(this).parent().parent().fadeOut(300);
    });

    $('.error .check').on('click', function() {
        popH();
        $('.pop').fadeIn(300);
    });

    $('.index .search_wrap .arrow').on('click', function() {
        var top = $('.h_search').offset().top;
        $('html,body').animate({
            'scrollTop': top
        }, 400);
    });
    $('.gotop').on('click', function() {
        $('html,body').animate({
            'scrollTop': 0
        }, 400);
    });

    $('.zip .arrow').on('click', function() {
        $(this).parent().parent().toggleClass('off');
    });

    var userSubnav = false;
    $('.nav-user').hover(function() {
        $('.user-subnav').addClass('active');
    }, function() {
        $('.user-subnav').removeClass('active');
    });

}

function searchInit(data) {
    for (var i = 0; i < data.length; i++) {
        var group = data[i].group;
        var liGroup = '<li data-group="' + group + '" class="mui-indexed-list-group">' + group + '</li>';
        $('.mui-indexed-list-inner ul').append(liGroup);
        for (var d = 0; d < data[i].list.length; d++) {
            var list = data[i].list[d];
            var li = '<li data-value="' + group + '" data-tags="' + group + '" class="mui-indexed-list-item">' + list + '</li>';
            $('.mui-indexed-list-inner ul').append(li);
        }
        $('.mui-indexed-list-bar').append('<a>' + group + '</a>');
    }
}

function selectInit() {
    $('.sel-con').hover(function(event) {
        $('.sel-list').hide();
        $(this).find('.sel-list').show();
    }, function() {
        $(this).find('.sel-list').delay(100).fadeOut(300);
    });
    // $('.list-box li').on('click', function() {
    //     var multiple = $(this).parent().parent().hasClass('multiple');
    //     var has = $(this).hasClass('selected');
    //     var listSub = $(this).hasClass('list-sub');
    //     var _this = $(this).parents('.list-box');

    //     if (_this.hasClass('sel-list')) {
    //         if (has) {
    //             selArray.remove($(this).text())
    //         }
    //         if (!multiple && !listSub && !$(this).parent().parent().hasClass('list-sub')) {
    //             $(this).toggleClass('selected');
    //         }
    //     } else {
    //         _this.prevAll('em').text($(this).text())
    //     }

    // });
    $('.sel-list li.list-sub span').on('click', function() {
        var act = $(this).hasClass('active');
        if (act) {
            $(this).removeClass('active');
            $(this).next().css('height', 0);
            return;
        }
        $(this).addClass('active');
        $(this).next().css('height', 'auto');
    });
    $('.sel-list li.list-sub li').on('click', function() {
        $(this).toggleClass('selected');
        var len = $('.sel-list li.list-sub li.selected').length;
        var idx = $(this).parent().find('li').length;
        if (len == idx) {
            $(this).parent().parent().addClass('selected');
            $(this).removeClass('selected').siblings().removeClass('selected');
        } else {
            $(this).parent().parent().removeClass('selected');
        }
    });
    $('.multiple li span').on('click', function() {
        var has = $(this).hasClass('selected');
        var hasSq = $(this).hasClass('sq');
        if (hasSq && !has && !$(this).prev().hasClass('selected')) return;
        if (has && !hasSq) {
            $(this).next().removeClass('selected');
        };
        $(this).toggleClass('selected');
    });

    $('.sortbar .sel-list .btn-mod').on('click', function() {
        var $li = $(this).parent().find('li.selected');
        $(this).parent().fadeOut(300);
        $li.each(function(index) {
            var text;
            if ($(this).hasClass('list-sub')) {
                text = $(this).find('span').text();
            } else {
                text = $(this).text();
            }
            selArray.push(text);
            tagAdd();
        });
    });
    $('.pop-share .sel-list .btn-mod,.userinfo .sel-list .btn-mod').on('click', function() {
        var $em = $(this).parent().prevAll('em');
        var $selbox = $(this).parent().prevAll('.selectedbox');
        var multiple = $(this).parent().hasClass('multiple');
        $selbox.text('');
        $(this).parent().fadeOut(300);
        if (multiple) {
            var $liSpan0 = $(this).prev().find('li').find('span:eq(0).selected');
            if (!$liSpan0.length) {
                $em.css('display', 'block');
                $selbox.css('display', 'none');
                return
            }
            $liSpan0.each(function(index) {
                var $liSpan1 = $(this).next().hasClass('selected');
                $em.css('display', 'none');
                if ($liSpan1) {
                    $selbox.css('display', 'block').append('<font class="red">' + $(this).text() + ',</font>');
                } else {
                    $selbox.css('display', 'block').append('<font>' + $(this).text() + ',</font>');
                }

            });
        } else {
            var $li = $(this).prev().find('li.selected');
            if (!$li.length) {
                $em.css('display', 'block');
                $selbox.css('display', 'none');
                return
            }
            $li.each(function(index) {
                $em.css('display', 'none');
                if ($(this).hasClass('list-sub')) {
                    $selbox.css('display', 'block').append($(this).find('span').text() + ',');
                } else {
                    $selbox.css('display', 'block').append($(this).text() + ',');
                }

            });
        }
    });
}

function tagAdd() {
    $('.toolbar .tag .content').html('');
    selArray = arrayRepeat(selArray);
    for (var i = 0; i < selArray.length; i++) {
        var list = '<a>' + selArray[i] + '<i></i></a>';
        $('.toolbar .tag .content').append(list);
    }
    $('.tag a').one('click', function() {
        var name = $(this).text();
        selArray.remove(name);
        selectSel(name)
        $(this).remove();
    });
}

function searchTagAdd() {
    $('#searchList').next('.tag').html('');
    $('#searchList .input-info').html('');
    searchArray = arrayRepeat(searchArray);
    for (var i = 0; i < searchArray.length; i++) {
        var list = '<a>' + searchArray[i] + '<i></i></a>';
        $('#searchList').next('.tag').append(list);
        $('#searchList .input-info').append(searchArray[i] + ',');
    }
    $('#searchList').next('.tag').find('a').one('click', function() {
        var name = $(this).text();
        searchArray.remove(name);
        $(this).remove();
        searchSel();
    });
}

function selectSel(name) {
    $('.sel-list li.selected').each(function() {
        if ($(this).text() == name) {
            $(this).removeClass('selected');
        }
    });
}

function searchSel() {
    $('#searchList .input-info').html('');
    if (searchArray.length == 0) {
        if ($('html').hasClass('eng')) {
            $('#searchList .input-info').html('<em>Please select the person you would like to authonze</em>');
        } else {
            $('#searchList .input-info').html('<em>请查找你要授权的人</em>');
        }
    }
    for (var i = 0; i < searchArray.length; i++) {
        $('#searchList .input-info').append(searchArray[i] + ',');
    }
}

function search() {
    var muiOk = false,
        isBox = false;
    $('#searchList').hover(function(event) {
        var _this = $(this);
        $(this).addClass('active');
        $('.search-list').show();
        if (muiOk) return;
        mui.init();
        mui.ready(function() {
            var list = document.getElementById('searchList');
            window.indexedList = new mui.IndexedList(list);
            $('.mui-placeholder').css({ 'z-index': '-1' });

            $('.mui-indexed-list-item').on('click', function(event) {
                event.stopPropagation();
                var text = $(this).text();
                $('.search-list').hide();
                $('.mui-indexed-list-search-input').val('');
                window.indexedList.search();
                searchArray.push(text);
                searchTagAdd();
            });
            muiOk = true;
        });
    }, function() {
        $(this).removeClass('active');
        setTimeout(function() {
            $('.search-list').hide();
        }, 10)
    });
}

function popNav() { //快速访问导航
    var nav = '<div style="position:fixed;bottom:0;left:0;background-color:rgba(255,255,255,.6);width:100%;font-size:18px;font-weight:bold;padding:10px 0;text-align:center;z-index:999;">';
    nav += '<a href="index.html">首页</a> | ';
    nav += '<a href="search.html">搜索结果</a> | ';
    nav += '<a href="user.html">个人中心</a> | ';
    nav += '<a href="upload.html">上传资料页</a> | ';
    nav += '<a href="details.html">详情页</a> | ';
    nav += '<a href="empower.html">未授权页</a> | ';
    nav += '<a href="login.html">登录页</a> | ';
    nav += '<a href="404.html">404页</a>';
    nav += '</div>';
    $('body').append(nav);
}

function popH() {
    var H = $('html').height(),
        _H = $(window).height(),
        top = $(window).scrollTop();


    setTimeout(function() {
        $('.pop .container').css('top', top + 100)
    }, 500);

    if ($('.h_search').find('div').is('.pop')) return;
    $('.pop').height(H);
}

function adjust() {
    var _W = $(window).width(),
        _H = $(window).height(),
        outerH = $('.outer').height(),
        headerH = $('header').height(),
        footerH = $('footer').height(),
        contentH = $('.content').outerHeight();
    var h = _H - headerH - footerH;

    if ((h - 120) < $('.login-wrap').height()) {
        $('.login-wrap').removeAttr('style');
    } else {
        $('.login-wrap').css({
            'margin-top': (h - $('.login-wrap').height()) / 2
        });
    }


    if ((h - 140) < $('.error .content').height()) {
        $('.error .content').removeAttr('style');
    } else {
        $('.error .content').css({
            'padding-top': (h - $('.error .content').height()) / 2
        });
    }

    if ($('html').hasClass('maxH') || $('body').hasClass('library_login')) {
        if (_H < (headerH + outerH + footerH)) {
            $('.outer').removeAttr('style');
            return;
        };
        $('.outer').height(h);
    }
}

// function fujian() {
//     popH();
//     $('.attach').fadeIn(300);
// }

function shouquan() {
    popH();
    $('.shouquan').fadeIn(300);
}

Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};

function arrayRepeat(arr) {
    var hash = [];
    for (var i = 0; i < arr.length; i++) {
        if (hash.indexOf(arr[i]) == -1) {
            hash.push(arr[i]);
        }
    }
    return hash;
}
//获取URL参数值
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg); // 匹配目标参数
    if (r != null) return unescape(r[2]);
    return null; // 返回参数值
}

function userSetup() {
    $('.setup .btn-mod').on('click', function() {
        var has = $(this).hasClass('ok');
        var $input = $(this).prev('label').find('input');
        if (has) {
            if ($('html').hasClass('eng')) {
                $(this).removeClass('ok').text('EDIT');
            } else {
                $(this).removeClass('ok').text('修改');
            }
            $input.attr("disabled", true);
        } else {
            if ($('html').hasClass('eng')) {
                $(this).addClass('ok').text('OK');
            } else {
                $(this).addClass('ok').text('确定');
            }
            var t = $input.val();
            $input.val("").attr("disabled", false).focus().val(t);
        }
    });
    var clipArea = new PhotoClip('#clipArea', {
        size: [300, 300],
        outputSize: [300, 300],
        file: "#file",
        outputQuality: 1,
        quality: 1,
        ok: "#clipBtn",
        loadStart: function() {
            console.log("照片读取中");
        },
        loadComplete: function() {
            console.log("照片读取完成");
            $('#file').hide();
        },
        done: function(dataURL) {
            $('#headimg').attr('src', dataURL);
            $('.uphead').fadeOut(function() {
                clipArea.clear();
                $('#file').show();
            });
        }
    });
    $('.setup .sethead').on('click', function() {
        $('.uphead').fadeIn();
        popH();
    });
    $('.headclear').on('click', function() {
        clipArea.clear();
        $('#file').show();
    });
}