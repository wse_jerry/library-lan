(function(document, window, $) {
    var cpShow = false,
        $tabbox = $('.list-item').eq(0);


    $(function() {
        // if (getUrlParam('tab') && getUrlParam('tabName')) {
        // 	var name = getUrlParam('tabName'),
        // 		num = getUrlParam('tab');
        // 	$('.tabnav li').eq(num).addClass('active').siblings().removeClass('active');
        // 	listShow(name);
        // }
        //tabInit();
        listSel();
        checkpop();
        adjust();

        $(window).resize(function() {
            adjust();
        });
        $('.pop .close').on('click', function() {
            listCancel();
        });

        // $('.list-item li .toolbox .edit').on('click', function() {
        //     window.location.href = 'upload.html';
        // });
        $('.list-item li .toolbox .del').on('click', function() {
            $(this).parents('li').remove();
        });
    });

    function tabInit() {
        $('.tabnav li').on('click', function() {
            var tabName = $(this).data('tab');
            $(this).addClass('active').siblings().removeClass('active');
            listCancel();
            listShow(tabName);
        });
    }

    function listShow(tabName) {
        $('.list-item').each(function() {
            $(this).removeClass('show').addClass('hide');
            if ($(this).data('tabbox') == tabName) {
                $(this).removeClass('hide').addClass('show');
                $tabbox = $(this);
            }
        });
    }

    function listSel() {
        $('.user .share').on('click', function() {
            $tabbox.find('.selectbox').show();
            $(this).addClass('hide').next().removeClass('hide');
            cpShow = true;
            var y = $('.sortbar').offset().top;
            $('html,body').animate({ 'scrollTop': y }, 500);
        });
        $('.user .check-share').on('click', function() {
            popH();
            $('.pop').fadeIn(300);
        });
        $('.user .cancel-share').on('click', function() {
            listCancel();
            $('html,body').animate({ 'scrollTop': 0 }, 500);
        });
        $('.list-item .selectbox').on('click', function() {
            $(this).parent().toggleClass('selected');
        });
    }

    function listCancel() {
        $tabbox.find('.selectbox').hide();
        cpShow = false;
        $('.btnbox .check-share').addClass('hide').siblings().removeClass('hide');
    }




    function checkpop() {
        $('.user .checkpop .checkAll').on('click', function() {
            $(this).toggleClass('selected');
            if ($(this).hasClass('selected')) {
                $tabbox.find('li').addClass('selected');
            } else {
                $tabbox.find('li').removeClass('selected');
            }

        });
        $(window).scroll(function() {
            var y = $(this).scrollTop();
            var navY = $('.btnbox').offset().top;
            if (y >= navY && cpShow) {
                $('.user .checkpop').fadeIn(300);
            } else {
                $('.user .checkpop').fadeOut(300);
            }
        });
    }

    function adjust() {
        var uploadH = $tabbox.find('li.user_item').eq(0).height() - 61;
        if (uploadH == -61) uploadH = 360;
        $('.list-item .upload').height(uploadH);
    }
})(document, window, jQuery);