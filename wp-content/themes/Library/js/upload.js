(function(document, window, $) {
    var maxNum = 80,
        maxNum120 = 120;

    $('#tagInput').bind('keypress', function(event) {
        var tx = $(this).val();
        if (event.keyCode == "13") {
            $(this).val('');
            $(this).parent().next('.tag').append('<a><em>' + tx + '</em><i></i></a>');
            $('.tag a').one('click', function() {
                $(this).remove();
            });
        }
    });
    // $('.error .close').click(function(){ 
    //     $(this).parent().fadeOut(300);
    // });
    // $('.upbox .close').click(function(){ 
    //     $(this).parent().remove();
    // });
    $('.maxlength').bind('input', function(event) {
        var $thisNum = $(this).next();
        var num;
        if ($(this).hasClass('max120')) {
            num = maxNum120 - $(this).val().length;
        } else {
            num = maxNum - $(this).val().length;
        }
        $thisNum.text(num);
        if (num <= 0) {
            $thisNum.addClass('red');
        } else {
            $thisNum.removeClass('red');
        }
    });
})(document, window, jQuery);