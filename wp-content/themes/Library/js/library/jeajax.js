$(document).ready(function() {
    //获取ajax请求的地址
    var ajaxUrl = wseLibrary.ajaxUrl;

    //初始化上传页面的用户搜索
    if ($('#searchlist')) {
        var data = {
            action: 'searchuser'
        };
        $.post(ajaxUrl, data, function(res) {
            searchInit(JSON.parse(res));
            //在编辑界面渲染授权的人
            if ($('.btn-push').length > 0) {
                if (getUrlParam('editKey') && editDetail.authUser.length > 0) {
                    setTimeout(() => {
                        $('#searchList').trigger('mouseenter');
                        editDetail.authUser.forEach(function(user) {
                            $('.mui-indexed-list-item:contains("' + user + '")').trigger('click');
                        });
                        $('#searchList').trigger('mouseleave');
                    }, 200);
                }
            }
        });
    }
    //切换用户语言
    $('.language a').click(function() {
        if ($(this).hasClass('active')) return;
        var data = {
            action: 'switchlang'
        };
        $.post(ajaxUrl, data, function(res) {
            location.reload();
        });
    });

    //删除用户页文件的ajax请求vsvs
    $('.del').click(function() {
        var item = $(this).parents('.user_item');
        if (item.length > 0) {
            var key = item.attr('key');
            var type = item.parents('.list-item.content').data('tabbox');
            var data = {
                action: 'deleteuserpost',
                args: {
                    type: type,
                    key: key
                }
            }
            $.post(ajaxUrl, data, function(res) {
                if (res == 'OK') {
                    item.remove();
                } else {
                    alert(res);
                }
                console.log(res);
            });
        }
    });

    //在列表页判断用户是否有权限下载该文章的附件
    $('.btn-mod.right').on('click', function() {
        if ($(this).hasClass('disable')) return;
        var key = $(this).parents('li').attr('key');
        var data = {
            action: 'getdownright',
            id: key
        };
        $.post(ajaxUrl, data, function(res) {
            if (res == 'No') {
                shouquan();
            } else {
                $('.attach').html(res);
                jeRebindFunc();
                fujian();
            }
        });
    });

    //封面上传，选中即上传
    $('#post_corver').change(function(e) {
        if (e.target.files.length < 1) return;
        var file = e.target.files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) return;
        var form_data = new FormData();
        form_data.append('files[0]', file);
        // ajax路径
        form_data.append('action', 'uploadfiles');
        //判断如果是在编辑文章那么带上文章的ID
        var editkey = getUrlParam('editkey');
        if (editkey) {
            form_data.append('post_id', editkey);
        }
        $.ajax({
            url: ajaxUrl,
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            beforeSend: function(request, xhr) {
                var upimg = $('<div class="upimg"><div class="load"><span class="corver_perc">0%</span></div><img class="corver_img" src="' + wseLibrary.siteUrl + '/images/upload/img.jpg"></div>');
                $('.corvercontainer').append(upimg);
            },
            xhr: function() {
                //获取XmlHttpRequest对象
                var xhr = $.ajaxSettings.xhr();
                //设置进度条事件
                xhr.upload.onprogress = function(data) {
                    var perc = Math.round((data.loaded / data.total) * 100);
                    $('.corver_perc').text(perc + '%');
                };
                return xhr;
            },
            success: function(response) {
                var result = JSON.parse(response);
                if (result.message[0] == 'OK') {
                    $('.corver_img').attr('src', result.urls[0]);
                    $('#post_corver').attr('corverid', result.ids[0]);
                    $('.upimg .load').hide('slow');
                } else {
                    $('.presult' + current_file_length).remove();
                    $('.error.corver message').text(result.message[0]);
                    $('.error.corver').show();
                }
                console.log(result);
            },
            error: function(error) {
                $('.error.corver message').text("Unknow Error, Please Try Again!");
                $('.error.corver').show();
                console.log(error);
            }
        });
    });

    //附件上传，选中即上传
    $('#post_attachment').change(function(e) {
        if (e.target.files.length < 1) return;
        var file = e.target.files[0];
        var form_data = new FormData();
        form_data.append('files[0]', file);
        // ajax路径
        form_data.append('action', 'uploadfiles');
        //判断如果是在编辑文章那么带上文章的ID
        var editkey = getUrlParam('editkey');
        if (editkey) {
            form_data.append('post_id', editkey);
        }
        var current_file_length = $('.upbox').children().length;
        var addBar = function(id) {
            //新建一个上传进度条
            var new_item = $('<div class="cpresult presult' + id + ' load"><span><b class="pwidth" style="width:0%"></b></span><em class="ptext">0%</em><i class="close"></i></div>');
            $('.upbox').append(new_item);
        };
        var setBar = function(id, perc) {
            $('.presult' + id + ' .ptext').text(perc + '%');
            $('.presult' + id + ' .pwidth').css('width', perc + '%');
        };

        $.ajax({
            url: ajaxUrl,
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            beforeSend: function(request, xhr) {
                addBar(current_file_length);
            },
            xhr: function() {
                //获取XmlHttpRequest对象
                var xhr = $.ajaxSettings.xhr();
                //设置进度条事件
                xhr.upload.onprogress = function(data) {
                    var perc = Math.round((data.loaded / data.total) * 100);
                    console.log(perc);
                    setBar(current_file_length, perc);
                };
                return xhr;
            },
            success: function(response) {
                var result = JSON.parse(response);
                if (result.message[0] == 'OK') {
                    setTimeout(() => {
                        $('.presult' + current_file_length).removeClass('load').addClass('list')
                            .attr('attachedid', result.ids[0])
                            .html('<i class="ico"></i><span>' + result.filename[0] + '</span><i class="close"></i>');
                    }, 200);
                } else {
                    $('.presult' + current_file_length).remove();
                    $('.error.attach message').text(result.message[0]);
                    $('.error.attach').show();
                }
                console.log(result);
            },
            error: function(error) {
                $('.error.attach message').text("Unknow Error, Please Try Again!");
                $('.error.attach').show();
                console.log(error);
            },
            complete: function(e) {
                $('.error .close').off('click').click(function() {
                    $(this).parent().fadeOut(300);
                });
                $('.upbox .close').off('click').click(function() {
                    $(this).parent().remove();
                });
            }
        });
    });
});