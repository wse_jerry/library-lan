//在用户页面计算用户需要访问的URL
function getHref(key, value) {
    var search = location.search.substr(1);
    if (search == "") {
        search = search + '?' + key + '=' + value;
    } else {
        if (search.indexOf(key) > -1) {
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
            //search = search.replace(reg, '$1' + key + '=' + value + '$3');
            search = search.replace(reg, function(match, p1, p2, p3, offset, string) {
                return match.replace(p2, value);
            });
        } else {
            search = search + '&' + key + '=' + value;
        }
        if (search.indexOf('paged') > -1) {
            var regp = new RegExp("(^|&)paged=([^&]*)(&|$)"); // 切换页面之后默认页面切换到第一页
            search = search.replace(regp, function(match, p1, p2, p3, offset, string) {
                return match.replace(p2, 1);
            });
        }
    }
    return location.pathname + '?' + search;
}

//制作自定义的提交表单
function jeGetForm(url, params, form) {
    if (form === undefined) {
        form = $("<form method='get'></form>");
        form.attr({
            "action": url
        });
    }

    var input;
    $.each(params, function(key, value) {
        input = $("<input type='hidden'>");
        input.attr({
            "name": key,
            'value': value
        });
        //input.val(value);
        form.append(input);
    });
    $(document.body).append(form);
    form.submit();
}

function jeRebindFunc() {
    $('.pop .close').off('click').on('click', function() {
        if ($('.outer').find('video').length > 0) {
            $('video').get(0).pause();
        }
        $(this).parent().parent().fadeOut(300);
    });
    //文件下载弹窗的单个文件选中事件
    $('.down_select_box').off('click').on('click', function() {
        $(this).parents('li.down_item_box').toggleClass('selected');
    });

    //文件下载弹窗的文件批量下载时间
    $('.btn-mod.multidown').off('click').on('click', function() {
        $('li.down_item_box.selected').each((idx, item) => {
            window.open($(item).attr('fileurl'), "navTab" + idx);
        });
    })
}