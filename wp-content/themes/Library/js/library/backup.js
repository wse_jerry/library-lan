//附件上传，选中即上传
$('#post_attachment').change(function() {
    var that = this;
    if ($(that).get(0).files.length > 0) {
        var files_data = $(that);
        var files_length = [];
        var files_name = [];
        var fd = new FormData();
        // 遍历所有文件装进FormData
        $.each($(files_data), function(i, obj) {
            $.each(obj.files, function(j, file) {
                fd.append('files[' + j + ']', file);
                files_length[j] = file.size;
                files_name[j] = file.name;
            });
        });
        // ajax路径
        fd.append('action', 'uploadfiles');
        //判断如果是在编辑文章那么带上文章的ID
        var editkey = getUrlParam('editkey');
        if (editkey) {
            fd.append('post_id', editkey);
        }
        var current_file_length = $('.upbox').children().length;
        var current_size = 0;
        var temp_id = 0;
        var addBar = function(id) {
            //新建一个上传进度条
            var new_item = $('<div class="cpresult presult' + id + ' load"><span><b class="pwidth" style="width:0%"></b></span><em class="ptext">0%</em><i class="close"></i></div>');
            $('.upbox').append(new_item);
        };
        var setBar = function(id, perc) {
            $('.presult' + id + ' .ptext').text(perc + '%');
            $('.presult' + id + ' .pwidth').css('width', perc + '%');
        };
        var endBar = function(id, result) {

        };
        var getJePercent = function(tLoaded) {
            var jeLoaded = tLoaded - current_size;
            if (jeLoaded > files_length[temp_id]) {
                current_size += files_length[temp_id];
                setBar(current_file_length + temp_id, 100);
                temp_id++;
                if (temp_id > files_length) return 100;
                addBar(current_file_length + temp_id);
                return getJePercent(tLoaded);
            }
            var perc = Math.round((jeLoaded / files_length[temp_id]) * 100);
            return perc;
        };
        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: fd,
            contentType: false,
            processData: false,
            beforeSend: function(request, xhr) {
                addBar(current_file_length + temp_id);
            },
            success: function(res) {
                var result = JSON.parse(res);
                for (var i = 0; i < result.message.length; i++) {
                    if (result.message[i] == 'OK') {
                        $('.presult' + (current_file_length + i)).removeClass('load').addClass('list').html('<i class="ico"></i><span>' + result.filename[temp_id] + '</span><i class="close"></i>')
                    } else {
                        $('.presult' + (current_file_length + i)).remove();
                        $('.error.attach').show();
                    }
                }
                console.log(res);
            },
            xhr: function() {
                //获取XmlHttpRequest对象
                var xhr = $.ajaxSettings.xhr();
                //设置进度条事件
                xhr.upload.onprogress = function(data) {
                    var jePerc = getJePercent(data.loaded);
                    console.log(jePerc);
                    setBar(current_file_length + temp_id, jePerc);
                };
                return xhr;
            },
            error: function(e) {
                $('.cpresult').remove();
                $('.error.attach').show();
                console.log(e);
            },
            complete: function(e) {
                $('.error .close').off('click').click(function() {
                    $(this).parent().fadeOut(300);
                });
                $('.upbox .close').off('click').click(function() {
                    $(this).parent().remove();
                });
            }
        });
    }
});

//附件（封面）选中即上传
$('input.abc[type=file]').change(function() {
    var that = this;
    return;
    var fd = new FormData();
    var files_data = $(this);
    // Loop through each data and create an array file[] containing our files data.
    $.each($(files_data), function(i, obj) {
        console.log(obj);
        $.each(obj.files, function(j, file) {
            fd.append('files[' + j + ']', file);
        });
    });
    // ajax路径
    fd.append('action', 'uploadfiles');
    //判断如果是在编辑文章那么带上文章的ID
    var editkey = getUrlParam('editkey');
    if (editkey) {
        fd.append('post_id', editkey);
    }
    var temp_id = $('.upbox').children().length;
    $.ajax({
        type: 'POST',
        url: ajaxUrl,
        data: fd,
        contentType: false,
        processData: false,
        beforeSend: function(request, xhr) {
            console.log(that.id);
            //重置进度条
            var new_item = $('<div class="presult' + temp_id + ' load"><span><b class="pwidth" style="width:0%"></b></span><em class="ptext">0%</em><i class="close"></i></div>');
            $('.upbox').append(new_item);
        },
        success: function(res) {
            var result = eval('(' + res + ')');
            for (var i = 0; i < result.message.length; i++) {
                if (result.message[i] == 'OK') {
                    $('.presult' + (temp_id + i)).removeClass('load').addClass('list').html('<i class="ico"></i><span>' + result.filename[temp_id] + '</span><i class="close"></i>')
                } else {
                    $('.presult' + (temp_id + i)).remove();
                    $('.error.attach').show();
                }
            };
            console.log(res);
        },
        xhr: function() {
            //获取XmlHttpRequest对象
            var xhr = $.ajaxSettings.xhr();
            //设置进度条事件
            xhr.upload.onprogress = function(data) {
                var perc = Math.round((data.loaded / data.total) * 100);
                console.log(perc);
                $('.presult' + temp_id + ' .ptext').text(perc + '%');
                $('.presult' + temp_id + ' .pwidth').css('width', perc + '%');
            };
            return xhr;
        },
        error: function(e) {
            $('.presult' + temp_id + '' + i).remove();
            $('.error.attach').show();
            console.log(e);
        },
        complete: function(e) {
            $('.error .close').off('click').click(function() {
                $(this).parent().fadeOut(300);
            });
            $('.upbox .close').off('click').click(function() {
                $(this).parent().remove();
            });
        }
    });
});