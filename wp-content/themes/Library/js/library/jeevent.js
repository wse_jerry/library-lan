$(document).ready(function() {
    //初始化网页界面
    function init() {
        //渲染切换语言的按钮
        var locale = $('.language').attr('locale');
        $('.language .' + locale).addClass('active');
        //获取地址栏参数
        var tab = getUrlParam('tab');
        var sortby = getUrlParam('sortby');
        var limit = getUrlParam('limit');
        var cate = getUrlParam('cate');
        var depart = getUrlParam('depart');
        var ftype = getUrlParam('ftype');
        if (tab) {
            //渲染用户页面的当前tab
            $('.tabnav li[data-tab="' + tab + '"]').addClass('active').siblings().removeClass('active');
        }
        if (sortby) {
            //渲染工具条的当前排序方式
            $('.post_sort li[data-sortby="' + sortby + '"]').addClass('active').siblings().removeClass('active');
            //渲染用户页面的当前排序方式
            var currentSort = $('.post_sort li[data-sortby="' + sortby + '"]')
            currentSort.parents('.list-box').prevAll('em').text(currentSort.text());
        }
        if (limit) {
            //渲染工具条以及用户页面的查询时间限制
            var currentLimit = $('.post_time_limit li[data-limit="' + limit + '"]');
            currentLimit.parents('.list-box').prevAll('em').text(currentLimit.text());
        }
        if (cate) {
            //把首页搜索分类的参数渲染到工具条
            cate.split(',').forEach(c => {
                $('.category[key="' + c + '"]').trigger('click');
            });
        }
        if (depart) {
            //把首页搜索部门的参数渲染到工具条
            depart.split(',').forEach(d => {
                $('.department[key="' + d + '"]').trigger('click');
            });
        }
        if (ftype) {
            //把首页搜索文件类型的参数渲染到工具条
            ftype.split(',').forEach(f => {
                $('.filetype[key="' + f + '"]').trigger('click');
            });
        }
        $('.btn-mod.condition').trigger('click');
    }

    //我的文件页面切换
    $('.tabnav li').on('click', function() {
        var tab = $(this).data('tab');
        var url = getHref('tab', tab);
        location.href = url;
    });

    //在工具条以及用户页切换排序方式
    $('.post_sort li').on('click', function() {
        if ($(this).hasClass('selbox')) return;
        var sortby = $(this).data('sortby');
        var url = getHref('sortby', sortby);
        location.href = url;
    });

    //在工具条以及用户页切换查询时间的限制
    $('.post_time_limit li').on('click', function() {
        var limit = $(this).data('limit');
        var url = getHref('limit', limit);
        location.href = url;
    });

    //主页底部的富搜索事件
    $('#longsearchbtn').click(function() {
        var data = {
            s: $('#searchtext').val()
        };
        var $cateArr = $('.category.active');
        var categories = [];
        $cateArr.each(function() {
            categories.push($(this).attr("key"));
        });
        if (categories.length !== 0) {
            data.cate = categories;
        }
        var $departArr = $('.department.active');
        var departments = [];
        $departArr.each(function() {
            departments.push($(this).attr("key"));
        });
        if (departments.length !== 0) {
            data.depart = departments;
        }
        var $ftypeArr = $('.filetype.active');
        var filetypes = [];
        $ftypeArr.each(function() {
            filetypes.push($(this).attr("key"));
        });
        if (filetypes.length !== 0) {
            data.ftype = filetypes;
        }
        jeGetForm($(this).attr('curl'), data);
    });

    //toolbal的搜索框提交
    $('#searchform').submit(function(e) {
        e.preventDefault();
        var data = {
            s: $('#tbsearchtext').val()
        };
        var $cateArr = $('.category.selected');
        var categories = [];
        $cateArr.each(function() {
            categories.push($(this).attr("key"));
        });
        if (categories.length !== 0) {
            data.cate = categories;
        }
        var $departArr = $('.department.selected');
        var departments = [];
        $departArr.each(function() {
            departments.push($(this).attr("key"));
        });
        if (departments.length !== 0) {
            data.depart = departments;
        }
        var $ftypeArr = $('.filetype.selected');
        var filetypes = [];
        $ftypeArr.each(function() {
            filetypes.push($(this).attr("key"));
        });
        if (filetypes.length !== 0) {
            data.ftype = filetypes;
        }
        jeGetForm($(this).attr('action'), data);
    });

    //上传页面的提交
    $('.btn-push').on('click', function(e) {
        var data = {
            //获取文章标题
            post_titile: $('#post_titile').val(),
            //获取文章摘要
            post_excerpt: $('#post_excerpt').val(),
            //获取文章内容
            post_content: tinymce.activeEditor.getContent()
        };
        //获取文章分类
        var $cateArr = $('.category.selected');
        var categories = [];
        $cateArr.each(function() {
            categories.push($(this).attr("key"));
        });
        if (categories.length !== 0) {
            data.post_category = categories;
        }
        //获取文章所授权下载和查看的部门
        var $departArr = $('.department.selected');
        var departments_down = [];
        var departments_view = [];
        $departArr.each(function() {
            if ($(this).siblings('span.sq').hasClass('selected')) {
                departments_view.push($(this).attr("key"));
            } else {
                departments_down.push($(this).attr("key"));
                departments_view.push($(this).attr("key"));
            }
        });
        if (departments_down.length !== 0) {
            data.post_department_down = departments_down;
        }
        if (departments_view.length !== 0) {
            data.post_department_view = departments_view;
        }
        //获取文章类型
        var $ftypeArr = $('.filetype.selected');
        var filetypes = [];
        $ftypeArr.each(function() {
            filetypes.push($(this).attr("key"));
        });
        if (filetypes.length !== 0) {
            data.post_filetype = filetypes;
        }
        //获取文章所授权的用户
        var $authuserArr = $('.authuser.tag a');
        var authusers = [];
        $authuserArr.each(function() {
            authusers.push($(this).text());
        });
        if (authusers.length !== 0) {
            data.post_authuser = authusers;
        }
        //获取文章标签
        var $ptagArr = $('.ptag.tag a');
        var ptags = [];
        $ptagArr.each(function() {
            ptags.push($(this).text());
        });
        if (ptags.length !== 0) {
            data.post_tag = ptags;
        }
        //获取文章封面图片的id
        var corverid = $('#post_corver').attr('corverid');
        if (corverid) data.post_corver = corverid;
        //获取文章附件的id
        var $pattachArr = $('.cpresult');
        var pattachs = [];
        $pattachArr.each(function() {
            if ($(this).attr('attachedid'))
                pattachs.push($(this).attr('attachedid'));
        });
        if (pattachs.length !== 0) {
            data.post_attachment = pattachs;
        }
        if (getUrlParam('editkey')) {
            data.editkey = getUrlParam('editkey');
        }
        jeGetForm('/upload/', data, $("<form method='post'></form>"));
    });

    //编辑文章时标题被修改的标记
    // $('#post_titile').change(function() {
    //     if (beginTrack)
    //         editField.title = true;
    // });
    //编辑文章时分类被修改的标记
    //编辑文章时分享部门被修改的标记
    //编辑文章时文件类型被修改的标记
    //编辑文章时分享个人被修改的标记
    //编辑文章时摘要被修改的标记
    //编辑文章时标签被修改的标记
    //编辑文章时内容被修改的标记
    // setTimeout(() => {
    //     tinyMCE.activeEditor.onChange.add(function(ed, e) {
    //         if (beginTrack)
    //             editField.content = true;
    //     });
    // }, 500);
    //编辑文章时封面被修改的标记
    //编辑文章时附件被修改的标记

    //文章详情页预览文件
    $('.btn-mod.preview1').on('click', function() {
        popH();
        $('.preview-pop').fadeIn(300, function() {
            if (previewSwiper) {
                previewSwiper.destroy();
            }
            previewSwiper = new Swiper('.swiper-container', {
                on: {
                    init: function() {
                        $('.preview-pop .swiper-slide').height($(window).height() / 1.5);
                    },
                    slideChangeTransitionStart: function() {
                        $('video').get(0).pause();
                    }
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });

    $('.list-box li').on('click', function() {
        var multiple = $(this).parent().parent().hasClass('multiple');
        var has = $(this).hasClass('selected');
        var listSub = $(this).hasClass('list-sub');
        var _this = $(this).parents('.list-box');

        if (_this.hasClass('sel-list')) {
            if (has) {
                selArray.remove($(this).text())
            }
            if (!multiple && !listSub && !$(this).parent().parent().hasClass('list-sub')) {
                $(this).toggleClass('selected');
            }
        } else {
            _this.prevAll('em').text($(this).text())
        }

    });

    $('.list-item li .toolbox .edit').on('click', function() {
        var key = $(this).attr('key');
        window.location.href = '/upload/?editkey=' + key;
    });

    jeRebindFunc();

    init();
});

function fujian() {
    if ($('.attach .container').attr('count') == 0) {
        alert('No attachment for this post.');
        return;
    }
    popH();
    $('.attach').fadeIn(300);
}
//编辑文章时的修改标记
var beginTrack = false;
var editField = {};