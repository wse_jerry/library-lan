//(function(document, window,$) {
    var sArray = [],
        selArray = [],
        selLiNum = {};
    var actLi = 0;
    $(function() {
        swiperInit();
    });

    function swiperInit() {
        var objNum = 0;
        $('.s_popwrap ul>li').each(function(index) {
            var _this = $(this);
            var sBox = {};
            objNum = index;
            sBox.id = objNum;
            sBox.classWrap = 'swiper' + objNum;
            sBox.class = 'slide' + objNum;
            $(this).find('.swiper-container').addClass(sBox.class);
            $(this).find('.swiper-slide').each(function(idx) {
                if (idx >= 1) {
                    _this.addClass(sBox.classWrap);
                    _this.find('.swiper-container').append('<div class="swiper-pagination"></div>');
                    var pagination = '.' + sBox.classWrap + ' .swiper-pagination';
                    var mySwiper = new Swiper('.' + sBox.class, {
                        simulateTouch: false, //禁止鼠标模拟
                        pagination: {
                            el: pagination,
                            clickable:true
                        }
                    });
                    sBox.swiper = mySwiper;
                }
            });
            $('.' + sBox.class + ' li').on('click', function() {
                $(this).toggleClass("active");
            });
            sArray.push(sBox);
        });

        $('.search_sel li').on('click', function() {
            var dataName = $(this).data('pop');
            $('.' + dataName).fadeIn(300);
            actLi = $(this).index();
        });

        
        $('.s_popwrap .btn').on('click', function() {
            var _this = $(this).prev().find('ul>li');
            if (!_this.hasClass('active')) {
                $('.search_sel li').eq(actLi).removeClass('active');
                selLiNum = {};
            } else {
                $('.search_sel li').eq(actLi).addClass('active');
            }
            $(this).prev().find('li.active').each(function(idx){
                selLiNum[idx] = $(this);
            })
            $(this).parents('.s_popwrap').fadeOut(300);
        });


        $('.s_poplist ul>li').hover(function(){
            var idx = $(this).index();
            if (!sArray[idx].swiper) return;
            sArray[idx].swiper.update();
        }).click(function () {
            var idx = $(this).index();
            selAct(sArray[idx].class,$(this));
        });

        $('.s_popwrap .close').on('click', function() {
            var $li = $(this).nextAll('.s_poplist').find('li.active');
            var arr = Object.keys(selLiNum);
            $(this).nextAll('.s_poplist').find('li').removeClass('active');
            if(arr.length == 0){
                $li.removeClass('active');
                return;
            }
            for(i = 0; i < arr.length; i++){
                selLiNum[i].addClass('active');
            }
        });
    }

    function selAct(name,that) {
        var liAct;
        var hasbox = that.find('div').hasClass('sublist');
        if(hasbox){
            $('.' + name).each(function() {
                liAct = $(this).find('li.active');
                selArray = liAct.length;
                if (liAct.length == 0) {
                    that.removeClass('active');
                } else {
                    that.addClass('active');
                }
            });
        }else{
            that.toggleClass('active');
        }
    }
//})(document, window,jQuery);