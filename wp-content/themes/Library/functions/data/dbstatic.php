<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件主要是用来加载数据库中的部门、分类等不会经常更改的资源，后期可以考虑在此增加缓存，以提高网站速度

//获取所有的分类
function library_get_categories() {
    $categories=get_categories(Array(
        'hide_empty'=>0,
        'orderby'=>'term_id',
        'order'=>'DESC'
    ));
    return $categories;
}

//获取所有的部门
function library_get_departments() {
    $departments=get_terms(array(
        'taxonomy'=>'department',
        'hide_empty'=>0,
        'orderby'=>'term_id',
        'order'=>'DESC',
        'parent'=>2
    ));
    return $departments;
}

//为详情页获取分类列举
function library_get_category_relations(){
    //注意这个函数只有在循环内调用才有效，因为他是自动获取的
    $cates=get_the_category();
    $defaultCate=array_values($cates)[0];
    return library_get_taxonomy_relations($defaultCate);
}

//为详情页获取作者部门列举
function library_get_author_department_relations($id){
    $departs=get_the_terms($id,'department');
    $defaultDepart=array_values($departs)[0];
    return library_get_taxonomy_relations($defaultDepart);
}

//为详情页获取作者部门列举
function library_get_subordinary_department_relations(){
    //注意这个函数只有在循环内调用才有效，因为他是自动获取的
    $pid=get_the_ID();
    $departids=get_post_meta($pid,'W_A_view_groups');
    $defaultId=$departids[0][0];
    $depart=get_term($defaultId);
    return library_get_taxonomy_relations($depart);
}

//获取文章类型的列举
function library_list_filetype(){
    $pid=get_the_ID();
    $ftypes=get_post_meta($pid,'je_post_format',true);
    foreach($ftypes as $ft){
        switch($ft){
            case 'img':
                printf('<i class="ico img"></i>%s',__('图片','library'));
                break;
            case 'video':
                printf('<i class="ico video"></i>%s',__('视频','library'));
                break;
            case 'audio':
                printf('<i class="ico audio"></i>%s',__('音频','library'));
                break;
            case 'zip':
                printf('<i class="ico zip"></i>%s',__('压缩包','library'));
                break;
            default:
                printf('<i class="ico office"></i>%s',__('办公文档','library'));
                break;
        }
    }
}

//判断当前用户对当前文章是否有权限查看
function library_has_view_right($id=0) {
    global $post,$user_ID;
    if($id>0)$post=get_post($id);
    if(!$post || !$user_ID)return false;
    if($user_ID==$post->post_author)return true;
    $savedviewusers = get_post_meta($post->ID, 'W_A_view_users', true);
    $savedviewgroups = get_post_meta($post->ID, 'W_A_view_groups', true);
    if(empty($savedviewusers) && empty($savedviewgroups)){
        return true;
    }
    $usercan=false;
    $groupcan=false;
    if (!empty($savedviewusers) && count($savedviewusers) > 0) {
        if(in_array($user_ID,$savedviewusers)){
            $usercan=true;
        }
    }
    if (!empty($savedviewgroups) && count($savedviewgroups) > 0) {
        $depart=wp_get_object_terms($user_ID,'department');
        if(in_array($depart[0]->term_id,$savedviewgroups)){
            $groupcan=true;
        }
    }
    if($usercan || $groupcan){
        return true;
    }
    return false;
}

//判断当前用户对当前文章是否有权限下载
function library_has_down_right($id=0) {
    global $post,$user_ID;
    if($id>0)$post=get_post($id);
    if(!$post || !$user_ID)return false;
    if($user_ID==$post->post_author)return true;
    $saveddownusers = get_post_meta($post->ID, 'W_A_down_users', true);
    $saveddowngroups = get_post_meta($post->ID, 'W_A_down_groups', true);
    if(empty($saveddownusers) && empty($saveddowngroups)){
        return true;
    }
    $usercan=false;
    $groupcan=false;
    if (!empty($saveddownusers) && count($saveddownusers) > 0) {
        if(in_array($user_ID,$saveddownusers)){
            $usercan=true;
        }
    }
    if (!empty($saveddowngroups) && count($saveddowngroups) > 0) {
        $depart=wp_get_object_terms($user_ID,'department');
        if(in_array($depart[0]->term_id,$saveddowngroups)){
            $groupcan=true;
        }
    }
    if($usercan || $groupcan){
        return true;
    }
    return false;
}