<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件主要是用来写多个页面需要用到的公共代码或者函数

//根据所有分类过滤出指定分类的子分类
function library_get_son_categories($categories,$cate) {
    $scategories = array_filter($categories, array(new SonCateFilter($cate),'get'));
    return $scategories;
}

//显示结果页底部的结果导航
function library_pagination() {
    the_posts_pagination( array(
        'screen_reader_text' => ' ',
        'format'             => '?paged=%#%',
        'mid_size' 			 => 2,
        'prev_text' 		 => __( '<li class="pn-prev">上一页</li>', 'library' ),
        'next_text' 		 => __( '<li class="pn-next">下一页</li>', 'library' ),
        'before_page_number' => '<li>',
        'after_page_number'  => '</li>',
    ) );
}

//为需要查询文章的页面生成查询参数，比如用户页，搜索结果页-开始
//生成初始化以及分页参数
function library_init_args($pnum) {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'posts_per_page' => $pnum,
        'paged' => $paged,
        'order' => 'DESC'
    );
    return $args;
}
//在搜索结果页以及用户结果页加入排序的相关过滤参数
function library_sort_args($args) {
    $orderby = isset($_REQUEST['sortby']) ? $_REQUEST['sortby'] : '';
	switch($orderby) {
        case 'bookmark' :
            $args['meta_key']='simplebookmarks_count';
            $args['orderby']='meta_value_num';
            break;
        case 'download' :
            $args['meta_key']='je_downloads';
            $args['orderby']='meta_value_num';
            break;
        case 'visit' :
            $args['meta_key']='je_visits';
            $args['orderby']='meta_value_num';
            break;
        default :
            break;
    }
    return $args;
}
//在搜索结果页以及用户结果页加入时间限制的相关过滤参数
function library_limit_args($args) {
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 'all';
	switch($limit) {
        case 'year' :
            $args['date_query']=array(
                'column'  => 'post_date',
                'after'   => '- 360 days'
            );
            break;
        case 'week' :
            $args['date_query']=array(
                'column'  => 'post_date',
                'after'   => '- 7 days'
            );
            break;
        case 'month' :
            $args['date_query']=array(
                'column'  => 'post_date',
                'after'   => '- 30 days'
            );
            break;
        case 'halfyear' :
            $args['date_query']=array(
                'column'  => 'post_date',
                'after'   => '- 180 days'
            );
            break;
        case 'all' :
        default :
            break;
    }
    return $args;
}
//在搜索结果页添加类别的过滤参数
function library_cate_args($args) {
    if (isset($_GET['cate'])) {
        $cate = explode(',', $_GET['cate']);
        $args['category__and']=$cate;
    }
    return $args;
}
//在搜索结果页添加部门的过滤参数
function library_depart_args($args) {
    if (isset($_GET['depart'])) {
        $depart = explode(',', $_GET['depart']);
        $users = get_objects_in_term($depart, 'department');
        $users = !empty($users) ? $users : PHP_INT_MAX;
        $args['author__in']=$users;
    }
    return $args;
}
//在搜索结果页添加文件类型的过滤参数
function library_ftype_args($args) {
    if (isset($_GET['ftype'])) {
        $ftype = explode(',', $_GET['ftype']);
        $accepted_mimes     = library_get_mine_list($ftype);
        $result = query_posts(array(
            'post_type'         => 'attachment',
            'post_status'       => 'inherit',
            'post_mime_type'    => $accepted_mimes?$accepted_mimes:'0',
        ));
        $pin=wp_list_pluck($result,'post_parent');
        $args['post__in']=$pin?$pin:PHP_INT_MAX;
    }
    return $args;
}
//在用户结果页加入对应tab的相关过滤参数
function library_tab_args($args,$tab,$bookmarkIds,$downloadIds,$viewIds) {
	switch($tab) {
        case 'bookmark' :
            if($bookmarkIds) {
                $args['post__in']=$bookmarkIds;
            } else {
                $args['post__in']=PHP_INT_MAX;
            }
            break;
        case 'download' :
            if($downloadIds) {
                $args['post__in']=$downloadIds;
            } else {
                $args['post__in']=PHP_INT_MAX;
            }
            break;
        case 'visit' :
            if($viewIds) {
                $args['post__in']=$viewIds;
            } else {
                $args['post__in']=PHP_INT_MAX;
            }
            break;
        case 'myfile' :
        default :
            global $user_ID;
            $args['author']=$user_ID;
            break;
    }
    return $args;
}
//对上述函数综合调用获取搜索结果页需要用到的参数结果
function library_get_search_args() {
    $args=library_init_args(9);
    $args=library_sort_args($args);
    $args=library_limit_args($args);
    $args=library_cate_args($args);
    $args=library_depart_args($args);
    $args=library_ftype_args($args);
    if(isset($_GET['s'])){
        $args['s']=$_GET['s'];
    }
    return $args;
}
//对上述函数综合调用获取用户页需要用到的参数结果
function library_get_user_args() {
    global $current_user;
    //获取用户所有的书签的ID集合
	$bookmarkIds=get_user_bookmarks($current_user->ID);
	//获取用户下载过的附件所归属的文章的ID集合
	$downloadIds=get_user_meta($current_user->ID, 'je_download_record',true);
	//获取用户分享的所有文章的ID集合
	$shareIds=get_user_meta($current_user->ID, 'je_share_record',true);
	//获取用户浏览的所有文章的ID集合
    $viewIds=get_user_meta($current_user->ID, 'je_visit_record',true);
    $tab = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : 'myfile';
    $args=library_init_args(8);
    $args=library_tab_args($args,$tab,$bookmarkIds,$downloadIds,$viewIds);
    $args=library_sort_args($args);
    $args=library_limit_args($args);
    return array(
        'args'=>$args,
        'book'=>$bookmarkIds?$bookmarkIds:array(),
        'down'=>$downloadIds?$downloadIds:array(),
        'view'=>$viewIds?$viewIds:array(),
        'share'=>$shareIds?$shareIds:array(),
        'tab'=>$tab
    );
}
//为需要查询文章的页面生成查询参数，比如用户页，搜索结果页-结束

//在浏览文章的时候添加访问记录
function library_the_post_view() {
    global $user_ID;
        $visithistory = get_user_meta($user_ID, 'je_visit_record', true);
        $pid = get_the_ID();
        if ($visithistory) {
            if (!in_array($pid, $visithistory)) {
                array_push($visithistory, $pid);
                update_user_meta($user_ID, 'je_visit_record', $visithistory);
            }
        } else {
            $visithistory = array($pid);
            add_user_meta($user_ID, 'je_visit_record', $visithistory);
        }
}

//获取附件预览的html代码,这是框架使用的代码
function library_attach_preview() {
    $post = get_post();

	if ( empty($post->post_type) || $post->post_type != 'attachment' )
		return $content;

	if ( wp_attachment_is( 'video', $post ) ) {
		$meta = wp_get_attachment_metadata( get_the_ID() );
		$atts = array( 'src' => wp_get_attachment_url() );
		if ( ! empty( $meta['width'] ) && ! empty( $meta['height'] ) ) {
			$atts['width'] = (int) $meta['width'];
			$atts['height'] = (int) $meta['height'];
		}
		if ( has_post_thumbnail() ) {
			$atts['poster'] = wp_get_attachment_url( get_post_thumbnail_id() );
		}
		$p = wp_video_shortcode( $atts );
	} elseif ( wp_attachment_is( 'audio', $post ) ) {
		$p = wp_audio_shortcode( array( 'src' => wp_get_attachment_url() ) );
	} else {
		$p = '<p class="attachment">';
		// show the medium sized image representation of the attachment if available, and link to the raw file
		$p .= wp_get_attachment_link(0, 'medium', false);
		$p .= '</p>';
    }
    return $p;
}

//获取附件预览的html代码,这是自定义的代码
function library_custom_attach_preview() {
    $post = get_post();
    if ( wp_attachment_is( 'video', $post ) ) {
    ?>
    <div class="wp-video">
        <!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
        <video id="source" filetype="video" class="wp-video-shortcode" id="video-22-1" preload="metadata" controls="controls">
		    <source type="video/mp4" src="" />
	    </video>
    </div>
    <?php
    } elseif ( wp_attachment_is( 'audio', $post ) ) {
    ?>
    <!--[if lt IE 9]><script>document.createElement('audio');</script><![endif]-->
    <audio id="source" filetype="audio" class="wp-audio-shortcode" id="audio-9-1" preload="none" style="width: 100%;" controls="controls">
        <source type="audio/mpeg" src="" />
    </audio>
    <?php
    } elseif ( wp_attachment_is( 'image', $post ) ) {
    ?>
    <img id="source" filetype="image" src="">
    <?php
    } elseif ( wp_attachment_is( 'pdf', $post ) ) {
    ?>
    <object id="source" filetype="pdf" data="http://localhost:8888/wp-content/uploads/2018/11/word.docx" type="application/pdf">
        alt : <a  href="http://localhost:8888/wp-content/uploads/2018/11/word.docx">test.pdf</a>
    </object>
    <!-- <embed src="http://localhost:8888/wp-content/uploads/2018/11/pdf.pdf" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html"> -->
    <?php
    } else {
        $p = '<p class="attachment">';
        // show the medium sized image representation of the attachment if available, and link to the raw file
        $p .= wp_get_attachment_link(0, 'medium', false);
        $p .= '</p>';
    }
    return $p;
}

//判断用户浏览的当前的文章的作者是不是用户自己
function library_is_post_author() {
    $post_author_id=get_the_author_meta('ID');
    global $user_ID;
    return $user_ID==$post_author_id;
}

//获取指定文章的所有附件，并按照指定格式输出，主要是搜索结果页点击下载时候的预览
function library_custom_attach_list($postid=0) {
    if($postid==0)$postid=get_the_ID();
    $plist=da_get_wse_attachments($postid,array(
        'orderby'=>'menu_order'
    ));
    ?>
    <div class="container" count="<?php echo count($plist); ?>">
        <span class="close"></span>
        <h2>附件下载</h2>
        <div class="list">
        	<ul>
                <?php
                foreach($plist as $p) {
                    $attachmenturl=da_get_wse_attachment_url($p['ID']);
                    ?>
                    <li class="down_item_box" fileurl="<?php echo $attachmenturl; ?>">
	    		    	<span class="text">
                            <b class="down_select_box"></b>
                            <i></i><?php echo esc_html( $p['title'] ); ?>
                            <em>（<?php echo library_filesize($p['size']); ?>）</em>
                        </span>
		    		</li>
                    <?php
                }
                ?>
	    		<!-- <li class="selected">
		    		<span class="text"><b></b><i></i>0715华尔街商业英语.ppt<em>（92.58m）</em></span>
				</li> -->
		    </ul>
		    <div class="btn-mod multidown">下载</div>
        </div>
    </div>
    <?php
}