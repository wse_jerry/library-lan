<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件用来翻译从数据库加载的文字

//对于分类法的翻译，比如Category、Department
function library_get_taxonomy_name($tax) {
    $locale=get_user_locale();
    if($locale!='zh_CN')return $tax->name;
    if($tax->description=='')return $tax->name;
    return $tax->description;
}

//在文章详情页显示分类法的隶属关系，比如Category、Department
function library_get_taxonomy_relations($tax) {
    $name=library_get_taxonomy_name($tax);
    if($tax->parent==0) return $name;
    $ftax=get_term($tax->parent);
    $fname=library_get_taxonomy_name($ftax);
    return $fname.' &gt; '.$name;
}