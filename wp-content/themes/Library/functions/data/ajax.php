<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}
//这个文件存储所有的ajax请求

//切换显示语言的ajax请求
function library_switchlang()
{
    //die('123');
    $library_language='';
    if(isset($_COOKIE["library_language"])){
        $library_language=($_COOKIE['library_language']=='en_US'?'en_US':'zh_CN');
    }
    else{
        $library_language=get_user_locale();
    }
    $update=($library_language=='en_US'?'zh_CN':'en_US');
    setcookie('library_language', $update, time()+62208000, COOKIEPATH, COOKIE_DOMAIN);
    if(is_user_logged_in()){
        $user_id=get_current_user_id();
        update_user_meta($user_id,'locale',$update);
    }
    print_r($_COOKIE);
    die('OK');
}
add_action('wp_ajax_switchlang', 'library_switchlang');//挂载该Ajax接口
add_action('wp_ajax_nopriv_switchlang', 'library_switchlang');//允许前端使用该Ajax接口

//删除用户页记录的ajax请求
function library_user_delete()
{
    $args = $_POST["args"];
    $type = $args['type'];
    $key = $args['key'];
    global $user_ID;
    if (!$type || !$key || !$user_ID) {
        die('Bad Request');
    }
    switch ($type) {
        //删除指定浏览记录
        case 'visit':
            $visithistory = get_user_meta($user_ID, 'je_visit_record', true);
            if (in_array($key, $visithistory)) {
                array_remove($visithistory, $key);
                update_user_meta($user_ID, 'je_visit_record', $visithistory);
                die('OK');
            } else {
                die('No Such Post');
            }
            break;
        //删除指定下载记录
        case 'download':
            $downloadhistory = get_user_meta($user_ID, 'je_download_record', true);
            if (in_array($key, $downloadhistory)) {
                array_remove($downloadhistory, $key);
                update_user_meta($user_ID, 'je_download_record', $downloadhistory);
                die('OK');
            } else {
                die('No Such Post');
            }
            break;
        //删除指定书签文件
        case 'bookmark':
            $bookmarks = get_user_bookmarks();
            if (in_array($key, $bookmarks)) {
                $bookmarkClass = new Bookmark;
                $bookmarkClass->update(intval($key), 'inactive', 1);
                die('OK');
            } else {
                die('No Such Post');
            }
            break;
        //删除用户的某条上传记录
        case 'myfile':
        default:
            $args = array(
                'author' => $user_ID,
                'include' => $key,
            );
            $posts = get_posts($args);
            if ($posts) {
                wp_delete_post($posts[0]->ID);
                die('OK');
            } else {
                die('No Such Post');
            }
            break;
    }
}
add_action('wp_ajax_deleteuserpost', 'library_user_delete');
add_action('wp_ajax_nopriv_deleteuserpost', 'library_user_delete');

//返回上传页面动态搜索用户的结果
function library_searchuser()
{
    //$args = $_POST["args"];
    $users=get_users(array(
        'orderby'=>'user_email'
    ));
    $groups=array();
    foreach($users as $u){
        $email=$u->user_email;
        if(!array_key_exists($eamil[0],$groups)){
            $groups[$email[0]]=array();
        }
        array_push($groups[$email[0]],$email);
    }
    $result=array();
    foreach($groups as $key => $value){
        $temp=array(
            'group'=>$key,
            'list'=>$value
        );
        array_push($result,$temp);
    }
    die(object_to_json($result));
}
add_action('wp_ajax_searchuser', 'library_searchuser');
add_action('wp_ajax_nopriv_searchuser', 'library_searchuser');

//判断用户在搜索结果页点击下载按钮时是否有权限的ajax请求
function library_getdownright()
{
    $id = $_POST["id"];
    if(is_numeric($id) && library_has_down_right($id)) {
        library_custom_attach_list($id);
        die();
    }
    die('NO');
}
add_action('wp_ajax_getdownright', 'library_getdownright');
add_action('wp_ajax_nopriv_getdownright', 'library_getdownright');

//接受ajax上传的文件
function library_upload_files(){
    $parent_post_id = isset( $_POST['post_id'] ) ? $_POST['post_id'] : 0;  // 获取附件所属文章的ID，在编辑文章然后修改附件时会有此参数
    $max_file_size = 1024 * 5000; // 附件大小KB
    $wp_upload_dir = wp_upload_dir();
    $path = $wp_upload_dir['path'] . '/';
    $count = 0;

    $attachments = get_posts( array(
        'post_type'         => 'attachment',
        'posts_per_page'    => -1,
        'post_parent'       => $parent_post_id,
        'exclude'           => get_post_thumbnail_id() //查询附件应该把封面图排除在外
    ) );

    // 处理保存文件之前的预处理和合法性判断
    if( $_SERVER['REQUEST_METHOD'] == "POST" ){   
        foreach ( $_FILES['files']['name'] as $f => $name ) {
            $extension = pathinfo( $name, PATHINFO_EXTENSION );
            // 产生一个20个随机数的文件名
            $new_filename = library_generate_random_code( 20 )  . '.' . $extension;               
            if ( $_FILES['files']['error'][$f] == 4 ) {
                continue; 
            }
            $filetype=wp_check_filetype( basename( $new_filename ), null );               
            if ( $_FILES['files']['error'][$f] == 0 ) {
                // 判断附件是否超出了大小限制
                if ( $_FILES['files']['size'][$f] > $max_file_size ) {
                    $upload_message[] = "$name is too large!.";
                    continue;                   
                // 判断文件类型是否允许上传
                } elseif( $filetype['ext']=='' ){
                    $upload_message[] = "$name is not a valid format";
                    continue;                     
                } else{ 
                    // //正式开始处理文件
                    if( move_uploaded_file( $_FILES["files"]["tmp_name"][$f], $path.$new_filename ) ) {                          
                        $count++; 
                        $filename = $path.$new_filename;
                        $wp_upload_dir = wp_upload_dir();
                        $attachment = array(
                            'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                            'post_mime_type' => $filetype['type'],
                            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $name ) ),
                            'post_content'   => '',
                            'post_status'    => 'inherit'
                        );
                        // 把附件信息写入数据库
                        $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
                        //require_once( ABSPATH . 'wp-admin/includes/image.php' );                            
                        //$attach_data = wp_generate_attachment_metadata( $attach_id, $filename ); 
                        //wp_update_attachment_metadata( $attach_id, $attach_data );
                        $upload_message[] = "OK";
                        $upload_filename[] = $name;
                        $upload_ids[] = $attach_id;
                        $upload_url[] = wp_get_attachment_url($attach_id);
                    }
                }
            }
        }
    }
    //输出结果
    $result=array(
        'count' => $count,
        'message' => $upload_message,
        'filename' => $upload_filename,
        'ids' => $upload_ids,
        'urls' => $upload_url
    );
    printf(json_encode($result));
    exit();
}
add_action('wp_ajax_uploadfiles', 'library_upload_files');
add_action('wp_ajax_nopriv_uploadfiles', 'library_upload_files');
