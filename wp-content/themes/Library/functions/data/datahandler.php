<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}
//这个文件存储需要处理入库的数据

//附件上传页的发表新的文章的处理函数
function library_upload_new() {
    global $wpdb;
    //标题,文本
    $tpost_titile= $wpdb->escape($_REQUEST['post_titile']);
    //分类，数组
    $tpost_category= explode(',',$wpdb->escape($_REQUEST['post_category']));
    //授权查看，数组
    $tpost_department_view= explode(',',$wpdb->escape($_REQUEST['post_department_view']));
    //授权下载，数组
    $tpost_department_down= explode(',',$wpdb->escape($_REQUEST['post_department_down']));
    //文件类型，数组
    $tpost_filetype= explode(',',$wpdb->escape($_REQUEST['post_filetype']));
    //授权的人，数组
    $tpost_authuser= explode(',',$wpdb->escape($_REQUEST['post_authuser']));
    //简介，文本
    $tpost_excerpt= $wpdb->escape($_REQUEST['post_excerpt']);
    //标签，数组
    $tpost_tag= explode(',',$wpdb->escape($_REQUEST['post_tag']));
    //内容，文本
    $tpost_content= $wpdb->escape($_REQUEST['post_content']);
    //封面，文本
    $tpost_corver= $wpdb->escape($_REQUEST['post_corver']);
    //附件，数组
    $tpost_attachment= explode(',',$wpdb->escape($_REQUEST['post_attachment']));

    $postargs=array(
        'post_title'     => $tpost_titile,
        'post_excerpt'   => $tpost_excerpt,
        'post_type'      => 'post',
        'post_name'      => sanitize_title( $tpost_titile ),
        'comment_status' => 'open',
        'ping_status'    => 'open',
        'post_content'   => $tpost_content,
        'post_status'    => 'publish',
        'post_author'    => $user_ID,
        'menu_order'     => 0,
    );
    //插入文章的标准结构
    $new_page_id = wp_insert_post($postargs);
    //设置文章的分类
    if($tpost_category){
        wp_set_post_categories($new_page_id, $tpost_category);
    }
    //设置文章的标签
    if($tpost_tag){
        wp_set_post_tags($new_page_id, $tpost_tag); 
    }
    //设置文章可以查看和下载的用户
    if($tpost_authuser){
        $library_authuser=array();
        foreach($tpost_authuser as $email){
            $user=get_user_by('email',$email);
            if($user){
                array_push($library_authuser,$user->ID);
            }
        }
        update_post_meta($new_page_id, 'W_A_view_users', $library_authuser);
        update_post_meta($new_page_id, 'W_A_down_users', $library_authuser);
    }
    //设置文章可以查看的部门
    if($tpost_department_view){
        update_post_meta($new_page_id, 'W_A_view_groups', $tpost_department_view);
    }
    //设置文章可以下载的部门
    if($tpost_department_down){
        update_post_meta($new_page_id, 'W_A_down_groups', $tpost_department_down);
    }
    //设置附件类型
    if($tpost_filetype){
        update_post_meta($new_page_id, 'je_post_format', $tpost_filetype);
    }
    //设置自定义的文章摘要，新版Wordpress具备post_excerpt属性
    // if($tpost_excerpt){
    //     update_post_meta($new_page_id, 'je_post_excerpt', $tpost_excerpt);
    // }
    //设置文章的封面图
    if($tpost_corver){
        set_post_thumbnail($new_page_id,$tpost_corver);
    }
    //设置文章的附件
    if($tpost_attachment){
        $attach_meta=array();
        $current_user_id = get_current_user_id();
        foreach($tpost_attachment as $id){
            $attach_meta[$id]=array(
                'file_id'		 => $id,
                'file_date'		 => current_time( 'mysql' ),
                'file_exclude'	 => '',
                'file_user_id'	 => $current_user_id
            );
            update_post_meta( $id, '_da_downloads', 0 );
            wp_update_post(array(
                'ID'             => $id,
                'post_parent'     => $new_page_id,
            ));
        }
        update_post_meta($new_page_id,'_da_attachments',$attach_meta);
    }
    wp_redirect(get_permalink($new_page_id));
}



//附件上传页编辑文章的内容处理函数
function library_upload_edit() {
    global $wpdb;
    //标题,文本
    $tpost_titile= $wpdb->escape($_REQUEST['post_titile']);
    //分类，数组
    $tpost_category= explode(',',$wpdb->escape($_REQUEST['post_category']));
    //授权查看，数组
    $tpost_department_view= explode(',',$wpdb->escape($_REQUEST['post_department_view']));
    //授权下载，数组
    $tpost_department_down= explode(',',$wpdb->escape($_REQUEST['post_department_down']));
    //文件类型，数组
    $tpost_filetype= explode(',',$wpdb->escape($_REQUEST['post_filetype']));
    //授权的人，数组
    $tpost_authuser= explode(',',$wpdb->escape($_REQUEST['post_authuser']));
    //简介，文本
    $tpost_excerpt= $wpdb->escape($_REQUEST['post_excerpt']);
    //标签，数组
    $tpost_tag= explode(',',$wpdb->escape($_REQUEST['post_tag']));
    //内容，文本
    $tpost_content= $wpdb->escape($_REQUEST['post_content']);
    //封面，文本
    $tpost_corver= $wpdb->escape($_REQUEST['post_corver']);
    //附件，数组
    $tpost_attachment= explode(',',$wpdb->escape($_REQUEST['post_attachment']));

    //插入文章的标准结构
    $new_page_id = (int)$_REQUEST['editkey'];
    wp_update_post(array(
        'ID'             => $new_page_id,
        'post_title'     => $tpost_titile,
        'post_excerpt'   => $tpost_excerpt,
        'post_content'   => $tpost_content,
    ));
    //设置文章的分类
    if($tpost_category){
        wp_set_post_categories($new_page_id, $tpost_category);
    }
    //设置文章的标签
    if($tpost_tag){
        wp_set_post_tags($new_page_id, $tpost_tag); 
    }
    //设置文章可以查看和下载的用户
    if($tpost_authuser){
        $library_authuser=array();
        foreach($tpost_authuser as $email){
            $user=get_user_by('email',$email);
            if($user){
                array_push($library_authuser,$user->ID);
            }
        }
        update_post_meta($new_page_id, 'W_A_view_users', $library_authuser);
        update_post_meta($new_page_id, 'W_A_down_users', $library_authuser);
    }
    //设置文章可以查看的部门
    if($tpost_department_view){
        update_post_meta($new_page_id, 'W_A_view_groups', $tpost_department_view);
    }
    //设置文章可以下载的部门
    if($tpost_department_down){
        update_post_meta($new_page_id, 'W_A_down_groups', $tpost_department_down);
    }
    //设置附件类型
    if($tpost_filetype){
        update_post_meta($new_page_id, 'je_post_format', $tpost_filetype);
    }
    //设置自定义的文章摘要，新版Wordpress具备post_excerpt属性
    // if($tpost_excerpt){
    //     update_post_meta($new_page_id, 'je_post_excerpt', $tpost_excerpt);
    // }
    //设置文章的封面图
    if($tpost_corver){
        set_post_thumbnail($new_page_id,$tpost_corver);
    }
    //设置文章的附件
    if($tpost_attachment){
        $attach_meta=array();
        $current_user_id = get_current_user_id();
        foreach($tpost_attachment as $id){
            $attach_meta[$id]=array(
                'file_id'		 => $id,
                'file_date'		 => current_time( 'mysql' ),
                'file_exclude'	 => '',
                'file_user_id'	 => $current_user_id
            );
            update_post_meta( $id, '_da_downloads', 0 );
            wp_update_post(array(
                'ID'             => $id,
                'post_parent'     => $new_page_id,
            ));
        }
        update_post_meta($new_page_id,'_da_attachments',$attach_meta);
    }
    wp_redirect(get_permalink($new_page_id));
}

//对上传的附件进行预处理
function library_handle_attachments($files, $new_page_id, $isthumb = false)
{
    if ($isthumb) {
        $_FILES = array("postcover" => $files);
        $newupload = wselibrary_insert_attachment('postcover', $new_page_id);
        if (is_wp_error($newupload)) {
            die(print_r($newupload));
        } else {
            // The image was uploaded successfully!
            update_post_meta($new_page_id, '_thumbnail_id', $newupload);
        }
    } else {
        $dafiles = array();
        foreach ($files['name'] as $key => $value) {
            if ($files['name'][$key]) {
                $file = array(
                    'name' => $files['name'][$key],
                    'type' => $files['type'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'error' => $files['error'][$key],
                    'size' => $files['size'][$key],
                );
                $_FILES = array("files" => $file);
                $newupload = wselibrary_insert_attachment('files', $new_page_id);
                $dafiles[$newupload] = array(
                    'file_id' => $newupload,
                    'file_date' => current_time('mysql'),
                    'file_exclude' => false,
                    'file_user_id' => $user_ID,
                );
                //update_post_meta($newupload, '_da_posts', array( $new_page_id ) );
                update_post_meta($newupload, '_da_downloads', 0);
            }
        }
        update_post_meta($new_page_id, '_da_attachments', $dafiles);
    }
}

//把附件信息入库
function wselibrary_insert_attachment($file_handler, $post_id, $setthumb = 'false')
{
    global $wpdb;
    // check to make sure its a successful upload
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) {
        __return_false();
    }
    require_once ABSPATH . "wp-admin" . '/includes/image.php';
    require_once ABSPATH . "wp-admin" . '/includes/file.php';
    require_once ABSPATH . "wp-admin" . '/includes/media.php';
    $attach_id = media_handle_upload($file_handler, $post_id);
    if ($setthumb) {
        $image_url = wp_get_attachment_image_src($attach_id, 'full');
        $wpdb->insert(
            $wpdb->prefix . 'postmeta',
            array(
                'post_id' => $post_id,
                'meta_key' => 'wpcf-vi-img',
                'meta_value' => $image_url[0],
            )
        );
    }
    return $attach_id;
}
