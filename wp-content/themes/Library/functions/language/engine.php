<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}

require get_template_directory() . '/language/en.php';
require get_template_directory() . '/language/cn.php';

function _j($key)
{
    global $wselibrary_settings;
    global $wselibrary_language;
    if ($wselibrary_language == $wselibrary_settings['second_language']) {
        global $wselibrary_chinese;
        echo $wselibrary_chinese[$key];
    } else {
        global $wselibrary_english;
        echo $wselibrary_english[$key];
    }
}

function _q($en, $cn)
{
    global $wselibrary_settings;
    global $wselibrary_language;
    if ($wselibrary_language == $wselibrary_settings['second_language']) {
        return $cn;
    } else {
        return $en;
    }
}