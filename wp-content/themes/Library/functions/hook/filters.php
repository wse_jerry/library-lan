<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}

if (!function_exists('wselibrary_login_protection')) {
    function wselibrary_login_protection()
    {
        if ($_GET['keyword'] != 'jerryqi') header('Location: /index.php/login');
    }
    add_action('login_enqueue_scripts','wselibrary_login_protection');
}

if(!function_exists('wselibrary_add_custom_mime_types')){
    function wselibrary_add_custom_mime_types($mimes){
        return array_merge($mimes,array (
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ));
    }
    add_filter('upload_mimes','wselibrary_add_custom_mime_types');
}

if(!function_exists('wselibrary_get_attachedparents')){
    function wselibrary_get_attachedparents(){
        if (isset($_GET['ftype'])){
            global $attachedparents;
            $ftypes = explode(',', $_GET['ftype']);
            $mimetype = array();
            $showtag = array();
            foreach ($ftypes as $ftype) {
                switch ($ftype) {
                    case '1':
                        if (!$showtag[0]) {
                            $showtag[0] = true;
                            array_push($mimetype, 'image/jpeg');
                        }
                        break;
                    case '2':
                        if (!$showtag[1]) {
                            $showtag[1] = true;
                            array_push($mimetype, 'video/mp4');
                        }
                        break;
                    case '3':
                        if (!$showtag[2]) {
                            $showtag[2] = true;
                            array_push($mimetype, 'application/zip');
                        }
                        break;
                    case '4':
                        if (!$showtag[3]) {
                            $showtag[3] = true;
                            array_push($mimetype, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        }
                        break;
                    case '5':
                    default:
                        if (!$showtag[4]) {
                            $showtag[4] = true;
                            array_push($mimetype, 'audio/mpeg');
                        }
                        break;
                }
            }
            //die(print_r($mimetype));
            global $usepre;
            $usepre=false;
            $attachments = get_posts(array(
                'post_type' => 'attachment',
                'posts_per_page' => -1,
                'post_mime_type' => $mimetype,
            ));
            $usepre=true;
            //die(print_r($attachments));
            $attachedparents = array();
            foreach ($attachments as $attach) {
                if (!in_array($attach->post_parent, $attachedparents)) {
                    array_push($attachedparents, $attach->post_parent);
                }
            }
            //die(print_r($attachedparents));
        }
    }
}

if (!function_exists('wselibrary_pre_get_posts')) {
    function wselibrary_pre_get_posts($query)
    {
        global $usepre;
        if ($usepre) {
            if (isset($_GET['cate'])) {
                $cate = explode(',', $_GET['cate']);
                $query->set('cat', $cate);
            }
            if (isset($_GET['depart'])) {
                $depart = explode(',', $_GET['depart']);
                $users = get_objects_in_term($depart, 'department');
                $users = !empty($users) ? $users : PHP_INT_MAX;
                $query->set('author__in', $users);
            }
            if (isset($_GET['ftype'])) {   
                global $attachedparents; 
                //print_r($attachedparents);   
                //die('12345');   
                $query->set('post__in', $attachedparents);
            }
        }
        // if (is_page('user')) {
        //     $query->set('posts_per_page', 8);
        // }
        // else
        // {
        //     $query->set('posts_per_page', 9);
        // }
    }
    add_action('pre_get_posts', 'wselibrary_pre_get_posts');
}

if (!function_exists('wselibrary_login_failed')) {
    function wselibrary_login_failed()
    {
        $login_page = home_url('/index.php/login/');
        wp_redirect($login_page . '?login=failed');
        exit;
    }
    //add_action('wp_login_failed', 'wselibrary_login_failed');
}

if (!function_exists('wselibrary_verify_user_pass')) {
    function wselibrary_verify_user_pass($user, $username, $password)
    {
        $login_page = home_url('/index.php/login/');
        if ($username == "" || $password == "") {
            wp_redirect($login_page . "?login=empty");
            exit;
        }
    }
    //add_filter('authenticate', 'wselibrary_verify_user_pass', 1, 3);
}

if (!function_exists('wselibrary_logout_redirect')) {
    function wselibrary_logout_redirect()
    {
        $login_page = home_url('index.php/login/');
        wp_redirect($login_page . "?login=false");
        exit;
    }
    //add_action('wp_logout', 'wselibrary_logout_redirect');
}

if (!function_exists('wselibrary_filter_post_thumbnail')) {
    function wselibrary_filter_post_thumbnail($html, $post_id, $post_thumbnail_id, $size, $attr)
    {
        if ('' == $html) {
            return '<img src="' . get_template_directory_uri() . '/images/listimg01.jpg" />';
        } else {
            $id = get_post_thumbnail_id();
            $src = wp_get_attachment_image_src($id, $size);
            return '<img src="' . $src[0] . '" />';
        }
    }
    add_filter('post_thumbnail_html', 'wselibrary_filter_post_thumbnail', 99, 5);
}

if(!function_exists('wselibrary_content_authorization')){
    function wselibrary_content_authorization($content){
        global $post,$user_ID;
        $savedviewusers = get_post_meta($post->ID, 'W_A_view_users', true);
        $savedviewgroups = get_post_meta($post->ID, 'W_A_view_groups', true);
        $error_message = '<p>
        You have not permission to view this content,
        click <a postid=' . $post->ID .
            ' onclick="shouquan()" >here to get authorization</a>.
        </p>';
        $usercan=false;
        $groupcan=false;
        if (!empty($savedviewusers) && count($savedviewusers) > 0) {
            if(in_array($user_ID,$savedviewusers)){
                $usercan=true;
            }
        }
        if (!empty($savedviewgroups) && count($savedviewgroups) > 0) {
            $depart=wp_get_object_terms($user_ID,'department');
            if(in_array($depart[0]->term_id,$savedviewgroups)){
                $groupcan=true;
            }
        }
        if((empty($savedviewusers) && empty($savedviewgroups)) || $usercan || $groupcan){
            return $content;
        }
        return $error_message;
    }
    add_filter('the_content','wselibrary_content_authorization');
}

if(!function_exists('wselibrary_attachment_authorization')){
    function wselibrary_attachment_authorization($args){
        global $post,$user_ID;
        $saveddownusers = get_post_meta($post->ID, 'W_A_down_users', true);
        $saveddowngroups = get_post_meta($post->ID, 'W_A_down_groups', true);
        $usercan=false;
        $groupcan=false;
        if (!empty($saveddownusers) && count($saveddownusers) > 0) {
            if(in_array($user_ID,$saveddownusers)){
                $usercan=true;
            }
        }
        if (!empty($saveddowngroups) && count($saveddowngroups) > 0) {
            $depart=wp_get_object_terms($user_ID,'department');
            if(in_array($depart[0]->term_id,$saveddowngroups)){
                $groupcan=true;
            }
        }
        if((empty($saveddownusers) && empty($saveddowngroups)) || $usercan || $groupcan){
            return true;
        }
        return false;
    }
    add_filter('librarycandownload','wselibrary_attachment_authorization');
}