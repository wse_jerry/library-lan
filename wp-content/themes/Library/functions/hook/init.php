<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件主要是用来初始化主题运行时需要用到的钩子

//对非管理员隐藏系统的管理工具条
if (!current_user_can('manage_options')) {
    add_filter('show_admin_bar', '__return_false');
}

//添加特色图片的支持
add_theme_support('post-thumbnails', array(
    'post',
    'page',
    'custom-post-type-name',
    ));

//添加filter来支持默认的特色图片
function library_filter_post_thumbnail($html, $post_id, $post_thumbnail_id, $size, $attr) {
    if ('' == $html) {
        return '<img src="' . get_template_directory_uri() . '/images/listimg01.jpg" />';
    } else {
        $id = get_post_thumbnail_id();
        $src = wp_get_attachment_image_src($id, $size);
        return '<img src="' . $src[0] . '" />';
    }
}
add_filter('post_thumbnail_html', 'library_filter_post_thumbnail', 99, 5);

//初始化主题事件
function library_init(){
    //加载语言包
    load_theme_textdomain('library', get_template_directory() . '/languages'); 
} 
add_action ('init', 'library_init');

//让主题支持多语言
$user_locale = get_user_locale();
function library_change_userlang_by_userlocale($locale){
    if(isset($_COOKIE["library_language"])){
        $library_language=$_COOKIE['library_language'];
        return ($library_language=="zh_CN"?"zh_CN":"en_US");
    }
    global $user_locale;
    return $user_locale ? $user_locale : $locale;
}
add_filter('locale','library_change_userlang_by_userlocale');

//文章内容是否有权限阅读的过滤器
function library_content_authorization($content){
    global $post;
    if(library_has_view_right()) {
        return $content;
    }
    return '<p>
    You have not permission to view this content,
    click <a postid='.$post->ID.
        ' href="javascript:shouquan();" >here to get authorization</a>.
    </p>';
}
add_filter('the_content','library_content_authorization');

//文章附件是否有权限下载的过滤器
function library_attachment_authorization($args){
    return library_has_down_right($args['postid']);
}
add_filter('librarycandownload','library_attachment_authorization');

//获取文章摘要的过滤，判断是否有自定义的摘要，有则使用自定义摘要，没有则使用系统默认摘要,新版Wordpress具备post_excerpt属性
// function library_get_excerpt($excerpt){
//     $pid=get_the_ID();
//     $jeExcerpt=get_post_meta($pid,'je_post_excerpt',true);
//     if($jeExcerpt)return $jeExcerpt;
//     return $excerpt;
// }
// add_filter('get_the_excerpt','library_get_excerpt');