<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件主要是用来自定义登陆界面的，我们知道Wordpress自带了一套登陆界面以及逻辑，我们想要使用的他的逻辑和我们自己的界面
//在这个我的实现方法是使用钩子把自己的界面添加到登陆页表单里买呢去，同时用Javascript隐藏掉他自带的表单，如此即达到了全定制自定义登陆界面的功能
//事实上，这里有一种更原生的方法，当然是只使用钩子添加字段，但是这个涉及到现有效果和原有效果冲突的尴尬，所以上述方法虽然粗暴，但是简单
//同时，Wordpress的默认登陆地址是/wp-login.php，于是我使用了一个WPS Hide Login的插件是的登录地址变成了/login

function library_loginscripts() { 
    wp_dequeue_style( 'login' );
    wp_dequeue_script('utils');
	wp_dequeue_script('user-profile');
    ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/normalize.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/selectric.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/style.css" />
    <link type="favicon" rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/favicon.ico" />
	<script src="<?php bloginfo('template_url');?>/js/vendor/jquery-2.2.3.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/vendor/modernizr-3.3.1.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/vendor/jquery.selectric.js"></script>
<?php }
add_action( 'login_enqueue_scripts', 'library_loginscripts' );

//添加注册表单的信息验证，包括验证邮箱是否是华尔街的邮箱以及部门是否是数据库中存在的部门
function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) { 
    //邮箱的合法性验证
    if(!library_endsWith($user_email,'@wallstreetenglish.com')) {
        $errors->add( 'wse_mail', __( '只有华尔街英语员工可以注册.' ) );
    }
    //验证部门是否合法
    if ( empty( $_POST['register_department'] ) || ! empty( $_POST['register_department'] ) && trim( $_POST['register_department'] ) == '' ) {
        $errors->add( 'department_error', __( '您提交的部门非法.' ) );
    }
    else {
        $departmentid=$_POST['register_department'];
        $departments=library_get_departments();
        $has=false;
        foreach($departments as $depart) {
            if($depart->term_id==$departmentid){
                $has=true;
                break;
            }
        }
        if($has==false) {
            $errors->add( 'department_error', __( '您提交的部门非法.' ) );
        }
    }
    return $errors;
}
add_filter( 'registration_errors', 'myplugin_registration_errors', 10, 3 );

//注册表单验证通过之后将部门信息写入数据库
function myplugin_user_register( $user_id ) {
    if ( ! empty( $_POST['register_department'] ) ) {
        wp_set_object_terms($user_id,(int)$_POST['register_department'],'department');
    }
}
add_action( 'user_register', 'myplugin_user_register' );

function library_validwsemail($errors, $user){
    $errors->add( 'wse_mail', __( '只有华尔街英语员工可以注册.' ) );
}
//add_action('validate_password_reset','library_validwsemail');

function library_login_header() { ?>
    <header>
        <div class="content">
            <div class="logo"><a href="/"></a></div>
            <div class="right nav-right">
                <ul>
                    <li class="language" locale="<?php echo get_user_locale(); ?>">
                        <a class="zh_CN" href="javascript:void(0);">CHN</a>
                        <a class="en_US" href="javascript:void(0);">ENG</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="outer login">
		<div class="content">
            <?php library_login_getform();?>
		</div>
    </div>
    <?php library_view('copyright');?>
    <script src="<?php bloginfo('template_url');?>/js/main.js"></script>
    <script>
        $(function(){
            $('#register_department').selectric({
              maxHeight: 200,
              optionsItemBuilder:function(itemData) {
              	return '<b class="teamico"></b>' + itemData.text;
              },
              labelBuilder:function(itemData) {
              	return '<b class="teamico"></b>' + itemData.text;
              }
            });
            //Render Login Page
            if ($('body').hasClass('library_login')) {
                $('html').addClass('no-js');
                $('html').addClass('maxH');
                $('body').removeClass('login');
                $('#login').remove();
                $('.clear').remove();
            }
            //渲染切换语言的按钮
            var locale = $('.language').attr('locale');
            $('.language .' + locale).addClass('active');
            //切换用户语言
            $('.language a').click(function() {
                if ($(this).hasClass('active')) return;
                var data = {
                    action: 'switchlang'
                };
                $.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function() {
                   location.reload();
                });
            });
        })
    </script>
<?php }
add_action( 'login_header', 'library_login_header');

function library_login_bodyclass() {
    return array('library_login');
}
add_filter('login_body_class', 'library_login_bodyclass');

function library_login_getform(){
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';
    switch ($action) {
        case 'postpass' :
        //case 'logout' :
        case 'lostpassword':
        case 'retrievepassword' :
            $lostpassword_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
            ?>
            <!-- 找回密码 -->
			<div class="login-wrap">
				<h1>找回密码</h1>
                <form name="lostpasswordform" id="lostpasswordform" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" method="post">
                    <?php
                    if ( isset( $_GET['error'] ) ) {
                        if ( 'invalidkey' == $_GET['error'] ) {
                            ?>
                            <div class="hint"><i></i>重置口令无效，请重新申请重置链接</div>
                            <?php
                        } elseif ( 'expiredkey' == $_GET['error'] ) {
                            ?>
                            <div class="hint"><i></i>重置口令过期，请重新申请重置链接</div>
                            <?php
                        }
                    }
                    ?>
                    <input type="text" placeholder="邮箱" name="user_login" id="user_login" class="input" value="<?php echo esc_attr($user_login); ?>" size="20">
                    <input type="hidden" name="redirect_to" value="<?php echo esc_attr( $lostpassword_redirect ); ?>" />
                    <button class="btn-mod">发送邮件</button>
					<div class="login-links">
						<span>找回密码的邮件将会被寄给您。</span>
					</div>
				</form>
			</div>
			<!-- end 找回密码 -->
            <?php
            break;
        case 'resetpass' :
        case 'rp' :
            $rp_cookie = 'wp-resetpass-' . COOKIEHASH;
            if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
                list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
                $user = check_password_reset_key( $rp_key, $rp_login );
                if ( isset( $_POST['pass1'] ) && ! hash_equals( $rp_key, $_POST['rp_key'] ) ) {
                    $user = false;
                }
            } else {
                $user = false;
            }
            $validerror = new WP_Error();
            if ( isset($_POST['pass1']) && $_POST['pass1'] != $_POST['pass2']) {
                $validerror->add( 'library_password_reset_mismatch', __( '您两次填写的密码不一致.','library' ) );
            }
            if ( ( ! $validerror->get_error_code() ) && isset( $_POST['pass1'] ) && !empty( $_POST['pass1'] ) ) {
                ?>
                <!-- 找回密码2 -->
			    <div class="login-wrap">
				    <h1>重置成功</h1>
				    <p class="copy">您的密码已经重置成功，您现在可以使用新的密码登陆。</p>
				    <a href="<?php echo esc_url( wp_login_url() );?>" class="btn-mod">立即前往</a>
			    </div>
			    <!-- end 找回密码2 -->
                <?php
                break;
            }
            ?>
            <!-- 设置新密码 -->
			<div class="login-wrap">
				<h1>设置新密码</h1>
                <form name="resetpassform" id="resetpassform" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=resetpass', 'login_post' ) ); ?>" method="post" autocomplete="off">
                    <?php
                        if ( 'library_password_reset_null' == $validerror->get_error_code() ) {
                            ?>
                            <div class="hint"><i></i>邮箱不能为空，请重新输入</div>
                            <?php
                        }
                        if ( 'library_password_reset_mismatch' == $validerror->get_error_code() ) {
                            ?>
                            <div class="hint"><i></i>两次输入不匹配，请重新输入</div>
                            <?php
                        }
                    ?>
                    <input type="hidden" id="user_login" value="<?php echo esc_attr( $rp_login ); ?>" autocomplete="off" />
                    <input type="password" placeholder="登录密码" data-reveal="1" data-pw="<?php echo esc_attr( wp_generate_password( 16 ) ); ?>" name="pass1" id="pass1" class="input password-input" size="24" value="" autocomplete="off" aria-describedby="pass-strength-result" />
                    <input type="password" placeholder="确认密码" name="pass2" id="pass2" />
                    <input type="checkbox" checked name="pw_weak" class="pw-checkbox" style="display:none;" />
                    <input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>" />
					<button class="btn-mod">确认</button>
				</form>
			</div>
			<!-- end 设置新密码 -->
            <?php
            break;
        case 'register' :
            ?>
            <!-- 注册 -->
			<div class="login-wrap mb20">
				<h1>注册</h1>
				<form name="registerform" id="registerform" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" method="post" novalidate="novalidate">
					<input type="text" name="user_login" id="user_login" placeholder="用户名">
					<input type="email" name="user_email" id="user_email" placeholder="邮箱">
					<label class="sel-con">
                        <select id="register_department" name="register_department">
                            <?php
                            $departments=library_get_departments();
                            foreach($departments as $depart) {
                                echo sprintf('<option class="department" value="%d">%s</option>',$depart->term_id,library_get_taxonomy_name($depart));
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn-mod">立即注册</button>
					<div class="login-links">
						<span>注册确认信将会被寄给您。</span>
						<span class="right">有账号 <a href="<?php echo esc_url( wp_login_url() );?>" class="black">立即登录</a></span>
					</div>
				</form>
			</div>
            <!-- end 注册 -->
            <?php
            break;
        case 'confirmaction' :
        case 'login':
        default :
            if	( isset($_GET['checkemail']) && 'confirm' == $_GET['checkemail'] ) {
                ?>
                <!-- 找回密码2 -->
			    <div class="login-wrap">
				    <h1>找回密码</h1>
				    <p class="copy">重置密码的邮件已经发往您的邮箱，需前往邮箱认证。</p>
				    <a href="https://mail.google.com/mail/u/0/#inbox" class="btn-mod">立即前往</a>
			    </div>
			<!-- end 找回密码2 -->
                <?php
                break;
            }
            elseif	( isset($_GET['checkemail']) && 'newpass' == $_GET['checkemail'] ) {
                ?>
                
                <?php
                break;
            }
            elseif	( isset($_GET['checkemail']) && 'registered' == $_GET['checkemail'] ) {
                ?>
                <!-- 注册成功 -->
			    <div class="login-wrap">
				    <h1>注册</h1>
				    <p class="copy">注册确认信已经发往您的邮箱，需前往邮箱认证。</p>
				    <a href="https://mail.google.com/mail/u/0/#inbox" class="btn-mod">立即前往</a>
			    </div>
			    <!-- end 注册成功 -->
                <?php
                break;
            }
            ?>
            <!-- 登录 -->
			<div class="login-wrap">
				<h1><?php _e('登录','library'); ?></h1>
				<form name="loginform" id="loginform1" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">
					<input type="text" name="log" id="user_login1" placeholder="用户名／邮箱">
                    <input type="password" name="pwd" id="user_pass1" placeholder="密码">
                    <input type="hidden" name="redirect_to" value="/" />
					<button class="btn-mod"><?php _e('登录','library'); ?></button>
					<div class="login-links">
                        <a href="<?php echo esc_url( wp_lostpassword_url() );?>">忘记密码</a>
                        <?php
                        if ( get_option( 'users_can_register' ) ) :
                            ?>
                            <span class="right">没有账号 <a href="<?php echo esc_url( wp_registration_url() ); ?>" class="black">点击注册</a></span>
                            <?php
                        endif;
                        ?>
					</div>
				</form>
			</div>
			<!-- end 登录 -->
            <?php
            break;
    }
}