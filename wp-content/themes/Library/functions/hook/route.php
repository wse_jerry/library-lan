<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件主要是用来对自定义页面使用自定义路由，假设有一个名字为abc的页面需要展示，他们的区别如下：
//如果使用自定义页面，那么需要在根目录添加一个page-abc.php的文件，他的访问URL是/index.php/abc
//如果使用自定义路由，那么我们只需要将指定URL让他访问到制定的文件即可，在这里我使用/abc

function library_query_vars($vars){
  $vars[] = 'lpage';
  return $vars;
}
add_filter('query_vars', 'library_query_vars');

function library_functionality_urls() {
    //测试路由
    add_rewrite_rule(
        '^lpage/(\w+)?',
        'index.php?lpage=$matches[1]',
        'top'
    );
    //upload页面的路由
    add_rewrite_rule(
        '^upload$',
        'index.php?lpage=upload',
        'top'
    );
    //用户设置页面
    add_rewrite_rule(
        '^user/setting$',
        'index.php?lpage=setting',
        'top'
    );
    //用户文件页面
    add_rewrite_rule(
        '^user$',
        'index.php?lpage=user',
        'top'
    );
    //法律申明页面
    add_rewrite_rule(
        '^privacy$',
        'index.php?lpage=privacy',
        'top'
    );
    //授权申明页面
    add_rewrite_rule(
        '^empower$',
        'index.php?lpage=empower',
        'top'
    );
    //测试参数页面
    add_rewrite_rule(
        '^test$',
        'index.php?lpage=test',
        'top'
    );
}
add_action('init', 'library_functionality_urls');