<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件是用来渲染局部页面文件的

function library_view($name) {
    $filename=get_template_directory() . '/functions/part/'.$name.'.php';
    if(file_exists($filename)) {
        require $filename;
    }
    else {
        echo 'No such view!';
    }
}