<?php
if(library_is_post_author()) {
?>
<div class="pop pop-share">
	<div class="container">
		<span class="close"></span>
		<h2>分享文件</h2>
		<form>
			<ul>
				<li class="select">
					<font>授权部门</font>
					<label class="sel-con light-ico">
						<em class="gray">授权部门</em>
						<span class="selectedbox"></span>
						<i class="arrow"></i>
						<div class="sel-list">
							<h3><?php _e('市场部','library'); ?><i></i></h3>
							<ul>
								<?php
								$departments=library_get_departments();
								foreach($departments as $depart) {
									echo sprintf('<li class="department" key="%d"><i></i>%s</li>',$depart->term_id,library_get_taxonomy_name($depart));
								}
								?>
							</ul>
							<div class="btn-mod condition"><?php _e('确定','library'); ?></div>
						</div>
					</label>
				</li>
				<li>
					<font>授权个人</font>
					<label id="searchList">
						<div class="input-info"><em>请查找你要授权的人</em></div>
						<div class="mui-indexed-list-search">
							<input type="search" class="mui-indexed-list-search-input" placeholder="请查找您要授权的人">
							<i class="s-ico"></i>
						</div>
						<div class="search-list">
							<div class="mui-indexed-list-bar"></div>
							<div class="mui-indexed-list-inner">
								<ul></ul>
							</div>
						</div>
					</label>
				</li>
			</ul>
			<button class="btn-mod red">提交</button>
		</form>
	</div>
</div>
<?php
}
?>