<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
?>
<div class="pop shouquan">
            <div class="container">
                <span class="close"></span>
                <h2><?php _e('授权申请','library'); ?></h2>
                <form>
                    <ul>
                        <li><font><?php _e('用&nbsp;&nbsp;户&nbsp;&nbsp;名','library'); ?></font>
                            <input type="text">
                        </li>
                        <li><font><?php _e('个人邮箱','library'); ?></font>
                            <input type="email">
                        </li>
                        <li class="select team-select">
                            <font><?php _e('所属部门','library'); ?></font>
                            <label class="sel-con">
                                <select>
                                    <?php
									$departments=library_get_departments();
									foreach($departments as $depart) {
										echo sprintf('<option class="department" key="%d">%s</option>',$depart->term_id,library_get_taxonomy_name($depart));
									}
									?>
                                </select>
                            </label>
                        </li>
                        <li class="select qx-select">
                            <font>选择权限</font>
                            <label class="sel-con">
                                <select>
                                    <option value="申请查看">申请查看</option>
                                    <option value="申请查看与下载">申请查看与下载</option>
                                </select>
                            </label>
                        </li>
                        <li><font>申请理由</font>
                            <textarea placeholder="申请理由"></textarea>
                        </li>
                    </ul>
                    <button class="btn-mod red">提交</button>
                </form>
            </div>
        </div>