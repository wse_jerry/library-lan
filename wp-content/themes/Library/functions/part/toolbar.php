<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件是工具条的内容，他作为部分内容供页面调用
?>
<div class="toolbar">
			<div class="search">
				<div class="content w1200">
					<form role="search" method="get" id="searchform" action="<?php echo home_url('/');?>">
						<input type="text" id="tbsearchtext" name="s" value="<?php echo get_search_query(); ?>">
						<button type="submit"><?php _e('搜索','library'); ?></button>
					</form>
				</div>
			</div>
			<div class="sortbar">
				<div class="content w1200">
					<div class="sort-item">
						<em><?php _e('筛选：','library'); ?></em>
						<ul>
						    <li class="selbox sel-con">
								<em><?php _e('全部类别','library'); ?></em>
								<i class="arrow-ico"></i>
								<div class="list-box sel-list maxw">
									<div class="item">
										<h3><?php _e('热门','library'); ?></h3>
										<ul>
											<li class="list-sub">
												<span><i></i>品牌相关</span>
												<ul>
													<li><i></i>品牌相关1</li>
													<li><i></i>品牌相关2</li>
													<li><i></i>品牌相关3</li>
													<li><i></i>品牌相关4</li>
												</ul>
											</li>
										</ul>
									</div>
									<div class="item">
										<h3><?php _e('更多','library'); ?></h3>
										<ul>
											<?php
											$categories=library_get_categories();
											foreach($categories as $fcate)
											{
												if($fcate->parent!=0)continue;
                                    			$scategories = library_get_son_categories($categories, $fcate);
                                    			if(count($scategories)==0)
                                    			{
                                        			printf('<li class="category" key="%d"><i></i>%s</li>',$fcate->term_id,library_get_taxonomy_name($fcate));
                                        			continue;
                                    			}
												$nums=0;
											?>
											<li class="list-sub">
												<span><i></i><?php echo library_get_taxonomy_name($fcate); ?></span>
												<ul>
											<?php
											foreach($scategories as $scate)
											{
												printf('<li class="category" key="%d"><i></i>%s</li>',$scate->term_id,library_get_taxonomy_name($scate));
											}
											?>
												</ul>
											</li>
											<?php
											}
											?>
										</ul>
									</div>
                                    <div class="btn-mod condition"><?php _e('确定','library'); ?></div>
								</div>
							</li>
						    <li class="selbox sel-con">
								<em><?php _e('全部部门','library'); ?></em>
								<i class="arrow-ico"></i>
								<div class="list-box sel-list">
									<h3><?php _e('市场部','library'); ?></h3>
									<ul>
										<?php
										$departments=library_get_departments();
										foreach($departments as $depart) {
											echo sprintf('<li class="department" key="%d"><i></i>%s</li>',$depart->term_id,library_get_taxonomy_name($depart));
										}
										?>
	                                </ul>
                                    <div class="btn-mod condition"><?php _e('确定','library'); ?></div>
								</div>
							</li>
						    <li class="selbox sel-con">
								<em><?php _e('全部格式','library'); ?></em>
								<i class="arrow-ico"></i>
								<div class="list-box sel-list">
									<ul>
										<li class="filetype" key="img"><i></i><?php _e('图片','library'); ?></li>
                            			<li class="filetype" key="video"><i></i><?php _e('视频','library'); ?></li>
                            			<li class="filetype" key="zip"><i></i><?php _e('压缩包','library'); ?></li>
                            			<li class="filetype" key="office"><i></i><?php _e('办公文档','library'); ?></li>
                            			<li class="filetype" key="audio"><i></i><?php _e('音频','library'); ?></li>
									</ul>
                                    <div class="btn-mod condition"><?php _e('确定','library'); ?></div>
								</div>
							</li>
						</ul>
					</div>
					<div class="sort-item right">
						<em><?php _e('排序：','library'); ?></em>
						<ul>
						    <li class="selbox">
								<em><?php _e('综合','library'); ?></em>
								<i class="arrow-ico"></i>
								<div class="list-box">
									<ul class="post_sort">
										<li data-sortby="recent"><?php _e('综合','library'); ?></li>
									    <li data-sortby="bookmark"><?php _e('收藏量','library'); ?></li>
									    <li data-sortby="download"><?php _e('下载量','library'); ?></li>
									</ul>
								</div>
							</li>
						    <li class="selbox">
								<em><?php _e('全部时间','library'); ?></em>
								<i class="arrow-ico"></i>
								<div class="list-box">
									<ul class="post_time_limit">
										<li data-limit="all"><?php _e('全部时间','library'); ?></li>
									    <li data-limit="year"><?php _e('今年','library'); ?></li>
									    <li data-limit="week"><?php _e('最近一周','library'); ?></li>
									    <li data-limit="month"><?php _e('最近一月','library'); ?></li>
									    <li data-limit="halfyear"><?php _e('最近半年','library'); ?></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="tag">
				<div class="content w1200"></div>
			</div>
		</div>