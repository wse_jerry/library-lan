<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//用来获取子分类的过滤器
class SonCateFilter {
    private $termid;

    function __construct($fcate) {
        $this->termid = $fcate->term_id;
    }

    function get($cate){
        return $cate->parent==$this->termid;
    }
}