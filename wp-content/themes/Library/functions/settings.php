<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, but you cannot access this page directly.');
}

global $wselibrary_settings;
$wselibrary_settings = array(
    'default_language' => 'en_US',
    'second_language' => 'cn_ZH',
);
