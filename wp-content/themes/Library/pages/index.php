<?php
//真正访问的主题首页是由这个文件进行渲染的，主题根目录下的主页文件用来做路由判断了
    get_header();
?>
    <div class="outer index">

        <!-- 搜索 -->
        <div class="search_wrap">
            <div class="bg">
                <img src="<?php bloginfo('template_url');?>/images/index/bg_1.jpg">
                <span class="arrow"><?php _e('查看更多搜索','library'); ?></span>
            </div>
            <div class="content">
                <div class="copy"><?php _e('资源为你的成长提供支持','library'); ?></div>
                <div class="search">
                    <form method="get" action="<?php echo home_url('/'); ?>">
                        <input type="text" id="searchtext" name="s" placeholder="<?php _e('请输入您要的资源关键词','library'); ?>">
                        <button type="submit" class="btn-mod"><?php _e('搜索','library'); ?></button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end 搜索 -->

        <!-- 高级搜索 -->
        <div class="h_search">
            <div class="bg"><img src="<?php bloginfo('template_url');?>/images/index/bg_2.jpg"></div>
            <div class="content">
                <ul class="search_sel">
                    <li data-pop="category"><i class="i_1"></i><?php _e('按类别筛选','library'); ?></li>
                    <li data-pop="team"><i class="i_2"></i><?php _e('按部门筛选','library'); ?></li>
                    <li data-pop="format"><i class="i_3"></i><?php _e('按格式筛选','library'); ?></li>
                </ul>
                <a href="javascript:void(0);" id="longsearchbtn" curl="<?php echo home_url('/'); ?>" class="btn-mod red"><?php _e('立即搜索','library'); ?></a>
            </div>

            <!-- 类别搜索浮层 -->
            <div class="pop s_popwrap category">
                <div class="content w1200">
                    <div class="close"></div>
                    <div class="title"><i></i><span><?php _e('按类别搜索','library'); ?></span></div>
                        <div class="s_poplist">
                            <ul>
                                <?php
                                $categories=library_get_categories();
                                foreach($categories as $fcate)
                                {
                                    if($fcate->parent!=0)continue;
                                    $scategories = library_get_son_categories($categories, $fcate);
                                    if(count($scategories)==0)
                                    {
                                        printf('<li class="category" key="%d"><span>%s</span></li>',$fcate->term_id,library_get_taxonomy_name($fcate));
                                        continue;
                                    }
                                    $nums=0;
                                ?>
                                <li>
                                    <span><?php echo library_get_taxonomy_name($fcate); ?></span>
                                    <i class="arrow"></i>
                                    <div class="sublist">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                <?php
                                    foreach($scategories as $scate)
                                    {
                                        if($nums % 8==0)
                                        {
                                        ?>
                                        <div class="swiper-slide">
                                            <ol>
                                        <?php
                                        }
                                        printf('<li class="category" key="%d">%s</li>',$scate->term_id,library_get_taxonomy_name($scate));
                                        $nums++;
                                        if($nums % 8==0)
                                        {
                                        ?>
                                            </ol>
                                        </div>
                                        <?php
                                        }
                                    }
                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <a href="javascript:void(0)" class="btn"></a>
                </div>
            </div>
            <!-- end 类别搜索浮层 -->

            <!-- 部门搜索浮层 -->
            <div class="pop s_popwrap team">
                <div class="content">
                    <div class="close"></div>
                    <div class="title"><i></i><span><?php _e('按部门搜索','library'); ?></span></div>
                    <div class="s_poplist">
                        <ul>
                            <?php
                            $departments=library_get_departments();
                            foreach($departments as $depart) {
                                echo sprintf('<li class="department" key="%d">%s</li>',$depart->term_id,library_get_taxonomy_name($depart));
                            }
                            ?>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" class="btn"></a>
                </div>
            </div>
            <!-- end 部门搜索浮层 -->

            <!-- 格式搜索浮层 -->
            <div class="pop s_popwrap format">
                <div class="content">
                    <div class="close"></div>
                    <div class="title"><i></i><span><?php _e('按格式搜索','library'); ?></span></div>
                    <div class="s_poplist">
                        <ul>
                            <li class="filetype" key="img"><i class="img-ico"></i><?php _e('图片','library'); ?></li>
                            <li class="filetype" key="video"><i class="video-ico"></i><?php _e('视频','library'); ?></li>
                            <li class="filetype" key="zip"><i class="zip-ico"></i><?php _e('压缩包','library'); ?></li>
                            <li class="filetype" key="office"><i class="office-ico"></i><?php _e('办公文档','library'); ?></li>
                            <li class="filetype" key="audio"><i class="audio-ico"></i><?php _e('音频','library'); ?></li>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" class="btn"></a>
                </div>
            </div>
            <!-- end 格式搜索浮层 -->

            <!-- 悬浮工具 -->
            <?php library_view('flowtool');?>
            <!-- end 悬浮工具 -->

        </div>
        <!-- end 高级搜索 -->
    </div>
<?php
    add_action('library_footer_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/index.js"></script>
        <?php
    });
    get_footer();