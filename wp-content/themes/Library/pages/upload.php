<?php
    global $wpdb,$user_ID;
    if (isset($_REQUEST['editkey'])) {
        $editkey = (int)$_REQUEST['editkey'];
        $args = array(
            'author' => $user_ID,
            'include' => $editkey,
        );
        $posts = get_posts($args);
        if($posts){
            $editPost=$posts[0];
        }else
        {
            status_header( 404 );
            get_template_part( 404 ); 
            exit();
        }
    }
    if($_POST) {
        //da_attach_file()
        if(isset($editPost)) {
            library_upload_edit();
        } else {
            library_upload_new();
        }
    }
    add_action('library_header_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/vendor/mui.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/vendor/mui.indexedlist.js"></script>
        <?php
    });
    get_header();
?>
    <div class="outer upload">
        <div class="content w1200">

            <!-- 基本信息 -->
            <div class="item userinfo">
                <div class="title"><h2><?php _e('基本信息','library'); ?></h2></div>
                <div class="container userinfo-form">
                    <form>
                        <ul>
                            <li class="col-1">
                                <label class="light-ico">
                                    <input type="text" id="post_titile" <?php if(isset($editPost)) printf('value="%s"',$editPost->post_title); ?> placeholder="<?php _e('输入文件标题','library'); ?>" class="maxlength" maxlength="80">
                                    <em class="hint-text">80</em>
                                </label>
                            </li>
                            <li class="col-3">
                                <label class="sel-con">
                                    <em class="gray"><?php _e('类别','library'); ?></em>
                                    <span class="selectedbox"></span>
                                    <i class="arrow"></i>
                                    <div class="list-box sel-list">
                                        <ul>
                                            <?php
                                            $categories=library_get_categories();
                                            if (isset($editPost)){
                                                $pcates=get_the_category();
                                            }
											foreach($categories as $fcate)
											{
												if($fcate->parent!=0)continue;
                                    			$scategories = library_get_son_categories($categories, $fcate);
                                    			if(count($scategories)==0)
                                    			{
                                                    if(in_array($fcate,$pcates)){
                                                        printf('<li class="category selected" key="%d"><i></i>%s</li>',$fcate->term_id,library_get_taxonomy_name($fcate));
                                                    }
                                                    else{
                                                        printf('<li class="category" key="%d"><i></i>%s</li>',$fcate->term_id,library_get_taxonomy_name($fcate));
                                                    }
                                        			continue;
                                    			}
												$nums=0;
											?>
											<li class="list-sub">
												<span><i></i><?php echo library_get_taxonomy_name($fcate); ?></span>
												<ul>
											<?php
											foreach($scategories as $scate)
											{
                                                if(in_array($scate,$pcates)){
                                                    printf('<li class="category selected" key="%d"><i></i>%s</li>',$scate->term_id,library_get_taxonomy_name($scate));
                                                }
                                                else{
                                                    printf('<li class="category" key="%d"><i></i>%s</li>',$scate->term_id,library_get_taxonomy_name($scate));
                                                }
											}
											?>
												</ul>
											</li>
											<?php
											}
											?>
                                        </ul>
                                        <div class="btn-mod jeselect"><?php _e('确定','library'); ?></div>
                                    </div>
                                </label>
                                <label class="sel-con">
                                    <em class="gray"><?php _e('部门','library'); ?></em>
                                    <span class="selectedbox"></span>
                                    <i class="arrow"></i>
                                    <div class="list-box sel-list multiple">
                                        <ul>
                                            <?php
                                            $departments=library_get_departments();
                                            if (isset($editPost)){
                                                $pdeparts_view=get_post_meta(get_the_ID(),'W_A_view_groups',true);
                                                $pdeparts_down=get_post_meta(get_the_ID(),'W_A_down_groups',true);
                                            }
						    				foreach($departments as $depart) {
                                                printf('<li>');
                                                if(in_array($depart->term_id,$pdeparts_down)||in_array($depart->term_id,$pdeparts_view)){
                                                    printf('<span class="department selected" key="%d"><i></i>%s</span>',$depart->term_id,library_get_taxonomy_name($depart));
                                                }
                                                else{
                                                    printf('<span class="department" key="%d"><i></i>%s</span>',$depart->term_id,library_get_taxonomy_name($depart));
                                                }
                                                if(in_array($depart->term_id,$pdeparts_view)&&!in_array($depart->term_id,$pdeparts_down)){
                                                    printf('<span class="sq selected"><i></i>%s</span>',__('仅查看','library'));
                                                }
                                                else{
                                                    printf('<span class="sq"><i></i>%s</span>',__('仅查看','library'));
                                                }
                                                printf('</li>');
								    		}
									    	?>
                                        </ul>
                                        <div class="btn-mod jeselect"><?php _e('确定','library'); ?></div>
                                    </div>
                                </label>
                                <label class="sel-con light-ico">
                                    <em class="gray"><?php _e('格式','library'); ?></em>
                                    <span class="selectedbox"></span>
                                    <i class="arrow"></i>
                                    <div class="list-box sel-list">
                                        <ul>
                                            <?php
                                            if (isset($editPost)){
                                                $pftype=get_post_meta(get_the_ID(),'je_post_format',true);
                                            }
                                            ?>
                                            <li class="filetype<?php if(in_array('img',$pftype)) echo ' selected'; ?>" key="img"><i></i><?php _e('图片','library'); ?></li>
                                			<li class="filetype<?php if(in_array('video',$pftype)) echo ' selected'; ?>" key="video"><i></i><?php _e('视频','library'); ?></li>
                                			<li class="filetype<?php if(in_array('zip',$pftype)) echo ' selected'; ?>" key="zip"><i></i><?php _e('压缩包','library'); ?></li>
                                			<li class="filetype<?php if(in_array('office',$pftype)) echo ' selected'; ?>" key="office"><i></i><?php _e('办公文档','library'); ?></li>
                                			<li class="filetype<?php if(in_array('audio',$pftype)) echo ' selected'; ?>" key="audio"><i></i><?php _e('音频','library'); ?></li>
                                        </ul>
                                        <div class="btn-mod jeselect"><?php _e('确定','library'); ?></div>
                                    </div>
                                </label>
                            </li>
                            <li class="col-1">
                                <label id="searchList">
                                    <div class="input-info"><em><?php _e('请查找你要授权的人','library'); ?></em></div>
                                    <div class="mui-indexed-list-search">
                                        <input type="search" class="mui-indexed-list-search-input" placeholder="<?php _e('请查找你要授权的人','library'); ?>" value="">
                                        <i class="s-ico"></i>
                                    </div>
                                    <div class="search-list">
                                        <div class="mui-indexed-list-bar"></div>
                                        <div class="mui-indexed-list-inner">
                                            <div class="mui-indexed-list-empty-alert">没有数据</div>
                                            <ul></ul>
                                        </div>
                                    </div>
                                </label>
                                <div class="tag authuser"></div>
                            </li>
                            <li class="col-1">
                                <label class="">
                                    <input type="text" id="post_excerpt" <?php if(isset($editPost)) printf('value="%s"',$editPost->post_excerpt); ?> placeholder="<?php _e('文件简介','library'); ?>" class="maxlength max120" maxlength="120">
                                    <em class="hint-text">120</em>
                                </label>
                            </li>
                            <li class="col-1">
                                <label class="">
                                    <input type="text" placeholder="<?php _e('输入文件标签','library'); ?>" id="tagInput" class="maxlength max120" maxlength="120">
                                    <em class="hint-text">120</em>
                                </label>
                                <div class="tag ptag"></div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <!-- end 基本信息 -->

            <!-- 详细内容 -->
            <div class="item">
                <div class="title"><h2><?php _e('详细内容','library'); ?></h2></div>
                <div class="container light-ico">
                    <?php 
                    if (isset($editPost)) {
                        $content = $editPost->post_content;
                    } else {
                        $content = '<p>Place your description here.</p>';
                    }
                    wp_editor(
                        $content,
                        'wselibrarycontent',
                        array(
                            'wpautop' => true,
                            'media_buttons' => false
                        )
                    ); 
                    ?>
                </div>
            </div>
            <!-- end 详细内容 -->

            <!-- 上传文件封面 -->
            <div class="item">
                <div class="title"><h2><?php _e('上传文件封面','library'); ?></h2></div>
                <div class="container corvercontainer">
                    <div class="file file-img">
                        <input id="post_corver" type="file" accept="image/*">
                        <div class="file-text">
                            <i></i>
                            <em><?php _e('上传封面','library'); ?></em>
                        </div>
                    </div>
                    <?php
                    if (isset($editPost)) {
                    ?>
                    <div class="upimg"><img class="corver_img" src="<?php echo get_the_post_thumbnail_url($editPost->ID); ?>"></div>
                    <?php
                    }
                    ?>
                </div>
                <div class="error corver" style="display:none;">
                    <span class="close"></span>
                    <div class="container">
                        <i></i>
                        <p class="message"><?php _e('封面上传仅支持JPG格式','library'); ?></p>
                    </div>
                </div>
            </div>
            <!-- end 上传文件封面 -->

            <!-- 上传附件 -->
            <div class="item">
                <div class="title"><h2><?php _e('上传附件','library'); ?></h2></div>
                <div class="container">
                    <div class="file">
                        <input type="file" id="post_attachment" accept="*/*">
                        <div class="file-text">
                            <i></i>
                            <em><?php _e('上传附件','library'); ?></em>
                            <p><?php _e('也可以拖拽文件到该区域，完成上传<span>支持1M以内文件上传</span>','library'); ?></p>
                        </div>
                    </div>
                    <div class="upbox">
                        <?php
                        if (isset($editPost)){
                            $pattachs=get_post_meta(get_the_ID(),'_da_attachments',true);
                            $index=0;
                            foreach($pattachs as $id=>$obj){
                                $attach=get_post($id);
                                $ext=wp_check_filetype($attach->guid)['ext'];
                                $attach_name=($attach->post_title).'.'.$ext;
                                printf('<div class="%s" attachedid="%d"><i class="ico"></i><span>%s</span><i class="close"></i></div>',('cpresult '.'presult'.$index.' list'),$id,$attach_name);
                                $index++;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="error attach" style="display:none'">
                    <span class="close"></span>
                    <div class="container">
                        <i></i>
                        <p class="message"><?php _e('附件上传仅支持图片/视频/<br>音频/压缩包/办公文档','library'); ?></p>
                    </div>
                </div>
            </div>
            <!-- end 上传附件 -->
            <div class="btn-mod btn-push"><?php _e('立即发布','library'); ?></div>
        </div>
        <!-- 悬浮工具 -->
        <div class="poptool">
            <div class="tab gotop">
                <i class="tab-ico"></i>
            </div>
        </div>
        <!-- end 悬浮工具 -->
    </div>
    <?php
    if (isset($editPost)){
        $ptags=wp_get_post_tags(get_the_ID());
        $otags=array();
        foreach($ptags as $tag){
            array_push($otags,$tag->name);
        }
        $puserids=get_post_meta(get_the_ID(),'W_A_view_users',true);
        $pusers=get_users(array(
            'include'=>$puserids
        ));
        $ousers=array();
        foreach($pusers as $user){
            array_push($ousers,$user->user_email);
        }
        ?>
        <script>
            var editDetail={
                authUser:<?php echo json_encode($ousers); ?>,
                postTag:<?php echo json_encode($otags); ?>
            };
        </script>
        <?php
    }   
    ?>
<?php
    add_action('library_footer_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/upload.js"></script>
        <script>
            $(function(){
                $('.wp-editor-wrap').addClass('light-ico');
                var editkey = getUrlParam('editkey');
                if (editkey) {
                    window.onbeforeunload = function() {
                        return 'Your edit will not save, are you sure to leave?';
                    };
                    //在编辑界面渲染分类、授权部门、文件类型
                    $('.jeselect').trigger('click');
                    //在编辑界面渲染标签
                    if(editDetail.postTag.length>0){
                        editDetail.postTag.forEach(function(tag){
                            $('.ptag').append('<a><em>' + tag + '</em><i></i></a>');
                            $('.tag a').one('click', function() {
                                $(this).remove();
                            });
                        });
                    }
                }
            });
        </script>
        <?php
    });
    get_footer();