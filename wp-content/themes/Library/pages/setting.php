<?php
    add_action('library_header_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/vendor/hammer.min.js"></script>
	    <script src="<?php bloginfo('template_url');?>/js/vendor/iscroll-zoom-min.js"></script>
	    <script src="<?php bloginfo('template_url');?>/js/vendor/lrz.all.bundle.js"></script>
	    <script src="<?php bloginfo('template_url');?>/js/vendor/PhotoClip.js"></script>
        <?php
    });
    get_header();
?>
    <div class="outer user">
		<div class="setup content w1200">
			<div class="sethead">
				<div class="mask"><?php _e('修改头像','library'); ?></div>
				<img style="width:160px;height:160px;" src="<?php echo get_avatar_url($current_user->ID); ?>">
			</div>
			<div class="name"><?php echo $current_user->display_name ?></div>
			<div class="setinfo">
				<form>
					<div class="item">
						<em><?php _e('用&nbsp;&nbsp;户&nbsp;&nbsp;名','library'); ?></em>
						<label>
							<input type="text" value="<?php echo $current_user->display_name ?>" disabled>
						</label>
						<span class="btn-mod"><?php _e('修改','library'); ?></span>
					</div class="item">
					<div class="item">
						<?php
						$department=wp_get_object_terms($current_user->ID,'department');
						?>
						<em><?php _e('所属部门','library'); ?></em>
						<label>
							<input type="text" value="<?php echo library_get_taxonomy_name($department[0])?>" disabled>
						</label>
						<span class="btn-mod"><?php _e('修改','library'); ?></span>
					</div class="item">
					<div class="item">
						<em><?php _e('登录密码','library'); ?></em>
						<label>
							<input type="password" value="" disabled>
							<p class="hint"><?php _e('密码要求至少包含字母，符号或数字中的两项且长度超过6位，建议您经常修改密码，以保证帐号更加安全.','library'); ?></p>
						</label>
						<span class="btn-mod"><?php _e('修改','library'); ?></span>
					</div class="item">
				</form>
			</div>
		</div>
		
		<div class="pop uphead">
			<div class="container">
				<span class="close"></span>
				<h2><?php _e('头像设置','library'); ?></h2>
				<div class="sethead">
					<input type="file" id="file" title="上传头像"/>
					<div id="clipArea"></div>
				</div>
				<div class="btn-mod headclear"><?php _e('重置','library'); ?></div>
				<div class="btn-mod red" id="clipBtn"><?php _e('确定','library'); ?></div>
			</div>
		</div>

        <!-- 悬浮工具 -->
		<?php library_view('flowtool');?>
        <!-- end 悬浮工具 -->
    </div>
<?php
    get_footer();