<?php
    add_action('library_header_style',function(){
        ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/selectric.css" />
        <?php
    });
    add_action('library_header_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/vendor/jquery.selectric.js"></script>
        <?php
    });
    get_header();
?>
    <div class="outer error">
        <div class="content">
            <img src="<?php bloginfo('template_url');?>/images/error_bg2.png" width="623" height="383">
            <p class="copy"><?php _e('抱歉，您未获取查看此文件的资格','library'); ?></p>
            <div class="btn-con">
                <a href="/" class="btn-mod"><?php _e('返回首页','library'); ?></a>
                <a href="javascript:void(0)" class="btn-mod red check"><?php _e('申请查看','library'); ?></a>
            </div>
        </div>

        <!-- 授权申请浮层 -->
        <?php library_view('authorizeflow');?>
        <!-- end 授权申请浮层 -->

    </div>
<?php
    add_action('library_footer_script',function(){
        ?>
        <script>
            $(function(){
                $('.qx-select select').selectric({
                    maxHeight: 200
                });
                $('.team-select select').selectric({
                    maxHeight: 200,
                    optionsItemBuilder:function(itemData) {
                        return '<b class="teamico"></b>' + itemData.text;
                    },
                    labelBuilder:function(itemData) {
                        return '<b class="teamico"></b>' + itemData.text;
                    }
                });
            });
        </script>
        <?php
    });
    get_footer();