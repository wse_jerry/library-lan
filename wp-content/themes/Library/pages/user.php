<?php
	add_action('library_header_script',function(){
		?>
		<script src="<?php bloginfo('template_url');?>/js/vendor/mui.js"></script>
	    <script src="<?php bloginfo('template_url');?>/js/vendor/mui.indexedlist.js"></script>
		<?php
	});
    get_header();
?>
    <div class="outer user">
		<div class="checkpop">
			<div class="content w1200">
				<div class="checkAll"><i></i><?php _e('全选','library'); ?></div>
				<div class="btn-mod white right check-share"><?php _e('立即分享','library'); ?></div>
				<div class="btn-mod white right cancel-share"><?php _e('取消分享','library'); ?></div>
			</div>
		</div>
		<?php
		$result=library_get_user_args();
		$bookmarkIds=$result['book'];
		$downloadIds=$result['down'];
		$shareIds=$result['share'];
		$viewIds=$result['view'];
		$args=$result['args'];
		$tab=$result['tab'];
		query_posts($args);
		?>
		<!-- 用户信息 -->
		<div class="user-wrap content w1200">
			<div class="databar">
				<ul>
					<li>
						<em><?php _e('上传量','library'); ?></em>
						<span><?php echo count_user_posts($current_user->ID); ?></span>
					</li>
					<li>
						<em><?php _e('下载量','library'); ?></em>
						<span><?php echo count($downloadIds);?></span>
					</li>
				</ul>
				<ul class="right">
					<li>
						<em><?php _e('收藏量','library'); ?></em>
						<span><?php echo count($bookmarkIds); ?></span>
					</li>
					<li>
						<em><?php _e('分享量','library'); ?></em>
						<span><?php echo count($shareIds);?></span>
					</li>
				</ul>
			</div>
			<div class="userbox">
				<div class="userset"><i></i><a href="/user/setting"><?php _e('个人设置','library'); ?></a></div>
				<div class="userhead"><img style="width:105px;height:105px;" src="<?php echo get_avatar_url($current_user->ID); ?>"></div>
				<div class="userinfo">
					<p class="name"><?php echo $current_user->display_name; ?></p>
					<p class="email"><?php echo $current_user->user_email; ?></p>
				</div>
				<div class="btnbox">
					<div class="btn-mod share"><?php _e('文件分享','library'); ?></div>
					<div class="btn-mod check-share hide"><?php _e('立即分享','library'); ?></div>
				</div>
				<div class="tabnav">
					<ul>
						<li class="active" data-tab="myfile"><?php _e('我的文件','library'); ?></li>
						<li data-tab="bookmark"><?php _e('书签文件','library'); ?></li>
						<li data-tab="download"><?php _e('下载记录','library'); ?></li>
						<li data-tab="visit"><?php _e('浏览记录','library'); ?></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end 用户信息 -->

		<!-- 排序方式 -->
		<div class="sortbar content w1200">
			<ul class="post_sort">
				<li data-sortby="recent" class="active"><?php _e('按综合排序','library'); ?></li>
				<li data-sortby="bookmark"><?php _e('按收藏量排序','library'); ?></li>
				<li data-sortby="download"><?php _e('按下载量排序','library'); ?></li>
				<li class="selbox">
					<em><?php _e('全部时间','library'); ?></em>
					<i></i>
					<div class="list-box">
						<ul class="post_time_limit">
							<li data-limit="all"><?php _e('全部时间','library'); ?></li>
							<li data-limit="year"><?php _e('今年','library'); ?></li>
							<li data-limit="week"><?php _e('最近一周','library'); ?></li>
							<li data-limit="month"><?php _e('最近一月','library'); ?></li>
							<li data-limit="halfyear"><?php _e('最近半年','library'); ?></li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
		<!-- end 排序方式 -->


		<!-- 列表 -->
		<div class="list-item content w1200 show" data-tabbox="<?php echo $tab; ?>">
			<div class="list">
				<ul>
					<li class="upload">
						<a href="/upload">
							<div class="file">
								<div class="file-text">
									<i></i>
									<em><?php _e('上传资料','library'); ?></em>
								</div>
							</div>
						</a>
					</li>
					<?php
					if (have_posts()):
						//$count=0;
						while (have_posts()): the_post();//$count++;
						?>
						<li class="user_item" key="<?php the_ID(); ?>">
							<div class="selectbox"><i></i></div>
							<div class="img">
								<div class="ico-box">
									<i class="doc">dov</i>
									<i class="video">video</i>
									<i class="zip">zip</i>
								</div>
								<a href="<?php the_permalink()?>"><?php the_post_thumbnail();?></a>
							</div>
							<div class="text">
								<p><?php the_title();?></p>
								<em><?php echo library_timeago(''); ?></em>
							</div>
							<div class="toolbox">
								<?php if($tab=='myfile') printf('<span class="edit" key="%d">%s</span>',get_the_ID(),__('编辑','library')); ?>
								<?php if($tab=='myfile'||$tab=='bookmark'||$tab=='visit') printf('<span class="del">%s</span>',__('删除','library')); ?>
							</div>
						</li>
						<?php
						endwhile;
					else:
						//echo '<h1>Nothing Found</h1>';
					endif;
					?>
				</ul>
			</div>
			<!-- 翻页 -->
			<div class="page">
				<div class="p-wrap">
					<ul>
						<?php
						library_pagination();
						//echo '<li class="pn-prev">'.$orderby.'</li>';				
						?>
					</ul>
				</div>
			</div>
			<!-- end 翻页 -->
		</div>
		<!-- end 列表 -->


		<!-- 分享文件浮层 -->
		<div class="pop pop-share">
			<div class="container">
				<span class="close"></span>
				<h2>分享文件</h2>
				<form>
					<ul>
						<li class="select">
							<font>授权部门</font>
							<label class="sel-con light-ico">
								<em class="gray">授权部门</em>
								<span class="selectedbox"></span>
								<i class="arrow"></i>
								<div class="sel-list">
									<h3>市场部<i></i></h3>
									<ul>
										<li><i></i>线下推广</li>
										<li><i></i>电话营销</li>
										<li><i></i>线上推广</li>
										<li><i></i>数字传播</li>
										<li><i></i>品牌传播</li>
										<li><i></i>社交媒体</li>
										<li><i></i>新市场业务</li>
									</ul>
									<div class="btn-mod">确定</div>
								</div>
							</label>
						</li>
						<li>
							<font>授权个人</font>
							<label id="searchList">
								<div class="input-info"><em>请查找你要授权的人</em></div>
								<div class="mui-indexed-list-search">
									<input type="search" class="mui-indexed-list-search-input" placeholder="请查找您要授权的人">
									<i class="s-ico"></i>
								</div>
								<div class="search-list">
									<div class="mui-indexed-list-bar"></div>
									<div class="mui-indexed-list-inner">
										<ul></ul>
									</div>
								</div>
							</label>
						</li>
					</ul>
					<button class="btn-mod red">提交</button>
				</form>
			</div>
		</div>
		<!-- end 分享文件浮层 -->


		<!-- 悬浮工具 -->
		<div class="poptool">
			<div class="tab gotop">
				<i class="tab-ico"></i>
			</div>
		</div>
		<!-- end 悬浮工具 -->

    </div>
<?php
    add_action('library_footer_script',function(){
    ?>
	    <script src="<?php bloginfo('template_url');?>/js/member.js"></script>
    <?php
    });
    get_footer();