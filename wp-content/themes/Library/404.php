<?php
    get_header();
?>
    <div class="outer error">
		<div class="content">
			<img src="<?php bloginfo('template_url')?>/images/error_bg.png" width="816" height="396">
			<p class="copy"><?php _e('抱歉，您要找的页面暂时找不到了.','library'); ?></p>
		</div>
    </div>
<?php
    get_footer();