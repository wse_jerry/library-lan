<?php
    add_action('library_header_style',function(){
        ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/selectric.css" />
        <?php
    });
    add_action('library_header_script',function(){
        ?>
        <script src="<?php bloginfo('template_url');?>/js/vendor/jquery.selectric.js"></script>
        <?php
    });
    get_header();
?>
    <div class="outer searchend">
		<!-- 工具条 -->
		<?php library_view('toolbar');?>
		<!-- end 工具条 -->
		

		<div class="list-item content w1200">
			<ul>
			<?php 
			if (have_posts()):
				while (have_posts()): the_post();
			?>
				<li key=<?php the_ID(); ?>>
					<div class="img">
			            <div class="ico-box">
			                <i class="doc"></i>
			            </div>
			            <a href="<?php the_permalink()?>"><?php the_post_thumbnail();?></a>
			        </div>
			        <div class="text">
			            <p><?php the_title();?></p>
			            <em><?php echo library_timeago(''); ?></em>
			        </div>
			        <div class="toolbox">
			            <a href="<?php the_permalink()?>" class="btn-mod"><?php _e('查看详情','library'); ?></a>
			            <a href="javascript:void(0)" class="btn-mod right"><?php _e('下载','library'); ?></a>
			        </div>
				</li>
			<?php 
				endwhile;
			else:
				echo '<h1>Nothing Found</h1>';
			endif;
			?>				
			</ul>
		</div>

		<!-- 翻页 -->
		<div class="page">
			<div class="p-wrap">
				<ul>
				<?php
				library_pagination();
				?>
				</ul>
			</div>
		</div>
		<!-- end 翻页 --> 

		<!-- 授权申请浮层 -->
        <?php library_view('authorizeflow');?>
        <!-- end 授权申请浮层 -->

		<!-- 附件下载浮层 -->
		<div class="pop attach"></div>
        <!-- end 附件下载浮层 -->

        <!-- 悬浮工具 -->
        <?php library_view('flowtool');?>
        <!-- end 悬浮工具 -->

	</div>
<?php
    add_action('library_footer_script',function(){
        ?>
        <script>
            $(function(){
                $('.team-select select').selectric({
                    maxHeight: 200,
                    optionsItemBuilder:function(itemData) {
                        return '<b class="teamico"></b>' + itemData.text;
                    },
                    labelBuilder:function(itemData) {
                        return '<b class="teamico"></b>' + itemData.text;
                    }
				});
				
				$('.qx-select select').selectric({
				maxHeight: 200,
				optionsItemBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				},
				labelBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				}
			});
            });
        </script>
        <?php
    });
    get_footer();