    <?php library_view('copyright');?>
    <script src="<?php bloginfo('template_url');?>/js/main.js"></script>
    <?php do_action('library_footer_script');?>
    <script src="<?php bloginfo('template_url');?>/js/library/jetools.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/library/jeajax.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/library/jeevent.js"></script>
    <?php wp_footer(); ?>
</body>

</html>