<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//主题根目录下的公共方法的入口，所有的自定义的公共方法文件都必须在这里引入
require get_template_directory() . '/functions/hook/init.php';
require get_template_directory() . '/functions/hook/route.php';
require get_template_directory() . '/functions/hook/login.php';

require get_template_directory() . '/functions/data/dataloader.php';
require get_template_directory() . '/functions/data/dbstatic.php';
require get_template_directory() . '/functions/data/datahandler.php';
require get_template_directory() . '/functions/data/ajax.php';
require get_template_directory() . '/functions/data/translate.php';

require get_template_directory() . '/functions/class/SonCateFilter.php';

require get_template_directory() . '/functions/utility/ploader.php';
require get_template_directory() . '/functions/utility/common.php';
