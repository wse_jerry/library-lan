<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}

require get_template_directory() . '/function/filters.php';
require get_template_directory() . '/functions/utility/ajax.php';
require get_template_directory() . '/functions/utility/datareader.php';
require get_template_directory() . '/functions/utility/handle.php';
require get_template_directory() . '/functions/utility/segment.php';
require get_template_directory() . '/function/settings.php';

require get_template_directory() . '/language/engine.php';

if (!function_exists('wselibrary_setup')) {
    function wselibrary_setup()
    {
        load_theme_textdomain('wselibrary');
    }
    add_action('after_setup_theme', 'wselibrary_setup');

    if (!current_user_can('manage_options')) {
        add_filter('show_admin_bar', '__return_false');
    }
    
}

if (!function_exists('init_wse_library')) {
    function init_wse_library()
    {
        global $wselibrary_language, $usepre;
        $wselibrary_language = wselibrary_getUserLanguage();
        $usepre=true;
        $login_page = home_url('/index.php/login/');
        $page_viewed = basename($_SERVER['REQUEST_URI']);
        if ($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
            wp_redirect($login_page);
            exit;
        }
        wselibrary_get_attachedparents();
    }
    add_action('init', 'init_wse_library', 0);
}

add_theme_support('post-thumbnails', array(
    'post',
    'page',
    'custom-post-type-name',
    ));