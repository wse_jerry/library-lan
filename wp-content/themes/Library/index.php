<?php
$lpage = get_query_var("lpage");
switch($lpage)
{
    case 'upload' :
        require get_template_directory() . '/pages/upload.php';
        break;
    case 'user' :
        require get_template_directory() . '/pages/user.php';
        break;
    case 'setting' :
        require get_template_directory() . '/pages/setting.php';
        break;
    case 'privacy' :
        require get_template_directory() . '/pages/privacy.php';
        break;
    case 'empower' :
        require get_template_directory() . '/pages/empower.php';
        break;
    case 'test' :
        require get_template_directory() . '/pages/test.php';
        break;
    default :
        require get_template_directory() . '/pages/index.php';
        break;
}