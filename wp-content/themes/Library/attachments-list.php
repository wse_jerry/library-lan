<?php
/**
 * The template for displaying attachments as list.
 *
 * Override this template by copying it to yourtheme/attachments-list.php
 *
 * @author Digital Factory
 * @package Wse Attachments/Templates
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

if ( empty( $args ) || ! is_array( $args ) )
	exit;

// extract variable storing all the parameters and attachments
extract( $args );
//print_r($attachments);
// you can start editing here 
?>
<?php if ( $count > 0 ):?>
<div class="attach">
	<div class="title"><i></i><?php _e('附件','library'); printf('( <em>%d</em> )',$count); ?></div>
	<div class="list">
		<ul>
			<?php foreach ( $attachments as $attachment ) : ?>
			<li>
				<span class="text"><i></i><?php echo esc_html( $attachment['title'] ); ?><em>（<?php echo library_filesize($attachment['size']); ?>）</em></span>
				<span class="right">
					<?php
					$attachmenturl=da_get_wse_attachment_url($attachment['ID']);
					if(library_has_view_right()){
						if(wp_attachment_is('audio',$attachment['ID'])||
						wp_attachment_is('video',$attachment['ID'])||
						wp_attachment_is('image',$attachment['ID'])||
						wp_attachment_is('pdf',$attachment['ID'])) {
							printf('<a class="btn-mod preview" url="%s" fileurl="%s">%s</a>',get_attachment_link($attachment['ID']),$attachmenturl,__('预览','library'));
						}
						else {
							printf('<a class="btn-mod preview disable">%s</a>',__('预览','library'));
						}
					}
					else{
						printf('<a class="btn-mod preview disable">%s</a>',__('预览','library'));
					}
					if(library_has_down_right()){
						printf('<a class="btn-mod dwownload" href="%s">%s</a>',$attachmenturl,__('下载','library'));
					}
					else{
						printf('<a class="btn-mod dwownload disable">%s</a>',__('下载','library'));
					}
					?>
				</span>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<?php endif; ?>