<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    die('Sorry, you cannot access this page directly.');
}
//这个文件是所有显示页面的公共头文件
    global $current_user;
    if(!is_user_logged_in()){
        wp_redirect(wp_login_url());
    }
?>
<!DOCTYPE html>
<html lang="zh" class="no-js<?php if(get_user_locale()!='zh_CN')echo ' eng'; ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>首页</title>
    <!-- build css/js -->
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/normalize.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/swiper-4.3.3.min.css" type="text/css" media="screen" />
    <?php do_action('library_header_style');?>
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/style.css" type="text/css" media="screen" />
    <link type="favicon" rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
    <script src="<?php bloginfo('template_url');?>/js/vendor/jquery-2.2.3.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/vendor/modernizr-3.3.1.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/vendor/swiper-4.3.3.min.js"></script>
    <script>
        var wseLibrary={
            siteUrl:"<?php echo bloginfo('template_url'); ?>",
            ajaxUrl:"<?php echo admin_url('admin-ajax.php'); ?>"
        };
    </script>
    <?php 
    do_action('library_header_script');
    wp_head();
    ?>
    <!-- endbuild -->
</head>
<body>
    <header>
        <div class="content">
            <div class="logo"><a href="/"></a></div>
            <div class="right nav-right">
                <ul>
                    <li class="language" locale="<?php echo get_user_locale(); ?>">
                        <a class="zh_CN" href="javascript:void(0);">CHN</a>
                        <a class="en_US" href="javascript:void(0);">ENG</a>
                    </li>
                    <?php
                        if(is_user_logged_in())
                        {
                    ?>
                    <li class="nav-user">
                        <a href="/user">
                            <span class="name"><?php echo $current_user->display_name ?></span>
                            <span class="head"><img style="width:45px;height:45px;" src="<?php echo get_avatar_url($current_user->ID); ?>"></span>
                        </a>
                        <div class="user-subnav">
                            <ul>
                                <li class="border"><a href="/upload"><?php _e('上传文件','library'); ?></a></li>
                                <li><a href="/user"><?php _e('我的文件','library'); ?></a></li>
                                <li><a href="/user/?tab=bookmark"><?php _e('书签文件','library'); ?></a></li>
                                <li><a href="/user/?tab=download"><?php _e('下载记录','library'); ?></a></li>
                                <li class="border"><a href="/user/?tab=visit"><?php _e('浏览记录','library'); ?></a></li>
                                <li class="border"><a href="/user/setting"><?php _e('个人设置','library'); ?></a></li>
                                <li class="signout"><a href="<?php echo wp_logout_url( '/login' ); ?> "><i></i><?php _e('退出','library'); ?></a></li>
                            </ul>
                        </div>
                    </li>
                    <?php        
                        }
                    ?>
                </ul>
            </div>
        </div>
    </header>