<?php
    add_action('library_header_style',function(){
        ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/selectric.css" />
        <?php
    });
    add_action('library_header_script',function(){
        ?>
        <script src="<?php bloginfo('template_url')?>/js/vendor/jquery.selectric.js"></script>
	    <script src="<?php bloginfo('template_url')?>/js/vendor/mui.js"></script>
	    <script src="<?php bloginfo('template_url')?>/js/vendor/mui.indexedlist.js"></script>
        <?php
    });
    get_header();
?>
    <div class="outer details">

		<!-- 工具条 -->
		<?php library_view('toolbar');?>
		<!-- end 工具条 -->

		<div class="d-container">
			<?php while(have_posts()):the_post();library_the_post_view();?>
			<div class="content w1200">
				<div class="info">
					<div class="info-text">
						<h1><?php the_title(); ?></h1>
						<p class="desc"><?php echo get_the_excerpt(); ?></p>
						<p class="intro">
							<span><?php _e('作者：','library'); the_author();?></span>
							<span><?php _e('隶属部门：','library'); ?><?php echo library_get_author_department_relations(get_the_author_meta('ID')); ?></span>
							<span><?php _e('上传日期：','library'); the_date();?></span>
						</p>
						<div class="btn">
							<?php 
							the_bookmarks_button(get_the_ID());
							// if(library_is_post_author()) echo '<a class="ico share"></a>';
							if(library_has_down_right()) echo '<a onclick="fujian()" class="ico download"></a>';
							?>
						</div>
					</div>
					<div class="attributes">
						<span><?php _e('种类：','library'); ?><?php echo library_get_category_relations(); ?></span>
						<span><?php _e('授权部门：','library'); ?><?php echo library_get_subordinary_department_relations(); ?></span>
						<span><?php _e('格式：','library'); ?><?php library_list_filetype(); ?></span>
					</div>
					<div class="attributes tagbox">
						<em><?php _e('标签：','library'); ?></em>
						<?php
						the_tags('<span>','</span><span>','</span>');
						?>
					</div>
				</div>
				<div class="d-content">
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>

		<!-- 预览浮层 -->
		<div class="pop preview-pop">
		
		</div>
		<!-- end 预览浮层 -->


		<!-- 授权申请浮层 -->
		<?php library_view('authorizeflow');?>
		<!-- end 授权申请浮层 -->


		<!-- 附件下载浮层 -->
		<div class="pop attach"><?php library_custom_attach_list();?></div>
        <!-- end 附件下载浮层 -->


		
		<!-- 分享文件浮层 -->
		<?php library_view('postshare');?>
		<!-- end 分享文件浮层 -->

		<!-- 悬浮工具 -->
		<div class="poptool">
			<div class="tab gotop">
				<i class="tab-ico"></i>
			</div>
		</div>
		<!-- end 悬浮工具 -->
	
	</div>
<?php
add_action('library_footer_script',function(){
    ?>
    <script>
		$(function () {
			var previewSwiper;
			$('.team-select select').selectric({
				maxHeight: 200,
				optionsItemBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				},
				labelBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				}
			});

			$('.qx-select select').selectric({
				maxHeight: 200,
				optionsItemBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				},
				labelBuilder: function (itemData) {
					return '<b class="teamico"></b>' + itemData.text;
				}
			});

			$('.info .btn .share').on('click',function(){
				popH();
				$('.pop-share').fadeIn(300);
			});

			$('.info .btn .collect').on('click',function(){
				$(this).addClass('active');
			});

			$('.btn-mod.preview').on('click',function(){
				var url=$(this).attr('url');
				if(url==undefined||url==null||url=="")return;
				var fileurl=$(this).attr('fileurl');
				$.get(url,function(data){
					var html=$(data);
					$('.preview-pop').html(html);
					var fileObj = $('#source');//.attr('src',fileurl)
					switch(fileObj.attr('filetype')) {
						case 'video':
							fileObj.find('source').attr('src',fileurl)
							break;
						case 'audio':
							fileObj.find('source').attr('src',fileurl)
							break;
						case 'image':
							fileObj.attr('src',fileurl)
							break;
						case 'pdf':
							//fileObj.attr('data',fileurl);
							break;
						case 'word':
							break;
						default:
							break;
					}
					$('.preview-pop').fadeIn(300,function(){
						popH();
						$('.preview-pop').fadeIn(300,function(){
							if(previewSwiper){
								previewSwiper.destroy();
							}
							previewSwiper = new Swiper('.swiper-container', {
								on:{
									init:function(){
										$('.preview-pop .swiper-slide').height($(window).height()/1.5);
										var box=$('.swiper-slide');
										fileObj.attr('width',box.width());
										fileObj.attr('height',box.height());
									},
									slideChangeTransitionStart:function(){
										$('video').get(0).pause();
									}
								},
								navigation: {
									nextEl: '.swiper-button-next',
									prevEl: '.swiper-button-prev',
								},
							});
						});
					});
					jeRebindFunc();
				});
			});
			$('.btn-mod.preview').on('click',function(){
				
			});
		});
	</script>
    <?php
});
    get_footer();