# Library SOP

### 初始化代码环境
1. 使用码云初始化git环境
>mkdir Library && cd Library    
git init   
git remote add origin git@gitee.com:jerryqi/Library.git    
git pull origin master    
curl -O https://wordpress.org/latest.tar.gz   
tar -xvzf latest.tar.gz   
mv wordpress/* .  
rmdir wordpress && rm latest.tar.gz

2. 将Wordpress的源代码添加到gitignore中
> /wp-admin     
/wp-content/languages/*     
/wp-content/plugins/*       
/wp-content/themes/*    
!/wp-content/themes/JerryTheme  
/wp-content/index.php   
/wp-includes    
/index.php  
/license.txt    
/readme.html    
/wp-activate.php    
/wp-blog-header.php     
/wp-comments-post.php   
/wp-config-sample.php   
/wp-cron.php    
/wp-links-opml.php  
/wp-load.php    
/wp-login.php   
/wp-mail.php    
/wp-settings.php    
/wp-signup.php  
/wp-trackback.php   
/xmlrpc.php     
/wp-config.php  
/.htaccess
3. 把第一次的修改提交到gitee
> git add .     
git commit -m "initial code"    
git push origin master

### 创建主题
1. 在themes目录下创建Library文件夹
2. 创建两个必要文件index.php和style.css
3. 拷贝前端文件（js，css，img等）到网站目录并只做预览图screenshot
4. 安装Wordpress并激活当前主题

### 制作主题

##### 制作首页
1. 制作header.php和footer.php
2. 完成index.php的静态页面的效果
- 当前只是作出效果，保证主题首页能够正确添加，动态内容后续再添加
- 修改静态文件js，css等为主题地址的相对路径
> bloginfo('template_url'); 

##### 自定义登陆页
1. 安装WPS Hide Login插件来自定义登陆页地址
2. 在自定义登陆页面的时候，采用了一种灵活的方式，因为前端页面的css使用的class和Wordpress是不用的，而且包裹的层也是不一样的，那么想要再原来Wordpress的表单基础上进行界面改造似乎是有点繁琐的，那么这里采用的方法是把前端文件的表单放到login_header的钩子里面，也就是会放到Wordpress表单的前面，同时加两句js把自带的表单给删除或者隐藏，以此方法来实现自定义登陆，注册等表单的完全自定义。逻辑代码放在了functions/hook/login.php，js代码放在了js/render.js。

##### 制作用户页面
1. 在主题根目录下新建author.php
2. 在拷贝前端页面私有部分代码
3. 在菜单栏使用get_the_author_link()来获取用户页面的地址

##### 制作自定义路由
1. 添加route.php，使用add_rewrite_rule添加路由
2. 添加路由规则之后必须再Settings->Permalink Settings不用进行任何更改点击保存
3. 可以参考官方文档https://codex.wordpress.org/Rewrite_API/add_rewrite_rule

##### 制作搜索结果页
1. 使用自定义的搜索参数
2. 使用library_timeago获取制定格式的发文时间
3. 开启特色图片的功能add_theme_support，位于hook/init.php
3. 在没有特色图片时使用默认的特色图片

##### 主题需要用到的插件
1. WP-

### 搭建Azure环境
1. 使用两台搭载CentOS的Linux服务器（测试环境使用A1系列）
2. 创建过程
Resource Group==》Virtual Network==》Subnet(2)==》Virtual Machine(2)
3. 在Network Security Group开放相应端口

### Wordpress使用总结

1. 关于get_categories函数，用这个函数获取所有文章分类时，他默认只读取所有有文章的分类，想要获取空的分类需要指定hide_empty属性为0
> 查看官方文档可以知道，他接受的参数如下(https://developer.wordpress.org/reference/functions/get_categories/)： 
'hide_empty'       => 0,    
'name'             => 'category_parent',    
'orderby'          => 'name',   
'selected'         => $category->parent,    
'hierarchical'     => true,     
'show_option_none' => __('None')    